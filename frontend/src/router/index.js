import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue'
import store from '../store'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    meta: { 
      requiresAuth: true
    },
  },
  {
   path: '/signin',
   name: 'SignIn',
   component: () => import('../views/SignIn.vue'),
  },
  {
   path: '/plans',
   name: 'Plans',
   component: () => import('../views/loadworks/Plans.vue'),
  },
  {
   path: '/contingent',
   name: 'Contingent',
   component: () => import('../views/loadworks/Contingent.vue'),
  },
  {
   path: '/versions',
   name: 'Versions',
   component: () => import('../views/loadworks/LoadVersions.vue'),
  },
  {
   path: '/gload/:version/:own',
   name: 'GLoad',
   component: () => import('../views/loadworks/GLoad.vue'),
  },
  {
   path: '/oload',
   name: 'OLoad',
   component: () => import('../views/loadworks/Oload.vue'),
  },
  {
   path: '/teachers',
   name: 'TeachersLoad',
   component: () => import('../views/teachers/TeachersLoad.vue'),
  },
  {
   path: '/tload',
   name: 'TLoad',
   component: () => import('../views/teachers/TLoad.vue'),
  },
  {
   path: '/columns',
   name: 'Columns',
   component: () => import('../views/loadworks/Columns.vue'),
  },
  {
   path: '/settings',
   name: 'settings',
   component: () => import('../views/Settings.vue'),
  },
  {
   path: '/dirs',
   name: 'dirs',
   component: () => import('../views/dirs/Dirs.vue'),
  },
  {
   path: '/dirs/faculties',
   name: 'faculties',
   component: () => import('../views/dirs/Faculties.vue'),
  },
  {
   path: '/dirs/directions',
   name: 'directions',
   component: () => import('../views/dirs/Directions.vue'),
  },
  {
   path: '/dirs/teachers',
   name: 'TeachersDir',
   component: () => import('../views/dirs/Teachers.vue'),
  },
  {
   path: '/dirs/departments',
   name: 'Departments',
   component: () => import('../views/dirs/Departments.vue'),
  },
  {
   path: '/dirs/objecttypes',
   name: 'ObjectTypes',
   component: () => import('../views/dirs/ObjectTypes.vue'),
  },
  {
   path: '/dirs/typesofwork',
   name: 'TypesOfWork',
   component: () => import('../views/dirs/TypesOfWork.vue'),
  },
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to, from, next) => {

  if(to.matched.some(record => record.meta.requiresAuth)) {
   if (store.getters['IS_AUTHENTICATED']) {
       next()
       return
   }
   next({path: '/signin', query: {next: to.path}})
  } 
  else {
    next() 
  }
}) 

export default router

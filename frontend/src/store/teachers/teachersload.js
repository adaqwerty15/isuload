import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_stat = 'teachers/stat'
const resource_uri_get_version = 'teachers/version'
const resource_uri_get_teachers_load = 'teachers/load'
const resource_uri_get_dep_teachers = 'teachers/dep'
const resource_uri_set_all_stroke = 'teachers/allstroke'
const resource_uri_set_calc_stroke = 'teachers/calcpart'
const resource_uri_set_load_stroke = 'teachers/loadpart'
const resource_uri_set_load_pract = 'teachers/practpart'

export default {
  state: () => ({
    deps_stat: [],
    teachers_load: [],
    dep_teachers: [],
    teachers_version: {}
  }),
  mutations: {
    SET_STAT_TO_STATE: (state, objects) => {
      state.deps_stat = objects
    },
    SET_TEACHERS_LOAD_TO_STATE: (state, objects) => {
      state.teachers_load = objects
    },
    SET_DEP_TEACHERS_TO_STATE: (state, objects) => {
      state.dep_teachers = objects
    },
    SET_ONE_STROKE_TO_STATE: (state, objects) => {
      let ind = state.teachers_load.findIndex(e => e.key == objects.key);
      if (ind >= 0) {  
        state.teachers_load.splice(ind, 1, objects);
      }
    },
    SET_TEACHERS_VERSION_TO_STATE: (state, objects) => {
      state.teachers_version = objects
    },
  },
  actions: {
    async GET_DEPS_STAT_FROM_API({dispatch, commit}, {faculty, year}) {
      dispatch('CLEAR_DEPS_STAT')
      return axios.get(resource_uri_get_stat,
                      {params: {faculty: faculty, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_STAT_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_DEPS_STAT({commit}) {
      commit('SET_STAT_TO_STATE', [])
    },
    async GET_TEACHERS_LOAD_FROM_API({commit}, {department, year}) {
      return axios.get(resource_uri_get_teachers_load,
                      {params: {department: department, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_TEACHERS_LOAD_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_DEP_TEACHERS_FROM_API({commit}, {department, year}) {
      return axios.get(resource_uri_get_dep_teachers,
                      {params: {department: department, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_DEP_TEACHERS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async SET_TEACHER_TO_ALL_STROKE({commit}, {uuid, group_id, group_type,
                                               department, teacher_id, load_version, key}) {
      return axios.put(resource_uri_set_all_stroke,
                      {},
                      {params: {uuid: uuid, group_id: group_id, 
                                group_type: group_type, department: department, 
                                teacher_id: teacher_id, load_version: load_version,
                                key: key},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_DEP_TEACHERS_TO_STATE', response.data.teachers)
                     commit('SET_ONE_STROKE_TO_STATE', response.data.stroke)
                  })
                  .catch(errorHandler.errorHandler)
    },  
    async SET_TEACHER_TO_LOAD_STROKE({commit}, {uuid, group_id, group_type,
                                               department, teacher_id, load_version, key, hours_type}) {
      return axios.put(resource_uri_set_load_stroke,
                      {},
                      {params: {uuid: uuid, group_id: group_id, 
                                group_type: group_type, department: department, 
                                teacher_id: teacher_id, load_version: load_version,
                                key: key, hours_type: hours_type},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_DEP_TEACHERS_TO_STATE', response.data.teachers)
                     commit('SET_ONE_STROKE_TO_STATE', response.data.stroke)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async SET_TEACHER_TO_CALC_STROKE({commit}, {uuid, group_id, group_type,
                                               department, teacher_id, load_version, key, calc_id, hours_type}) {
      return axios.put(resource_uri_set_calc_stroke,
                      {},
                      {params: {uuid: uuid, group_id: group_id, 
                                group_type: group_type, department: department, 
                                teacher_id: teacher_id, load_version: load_version,
                                key: key, calc_id: calc_id, hours_type: hours_type},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_DEP_TEACHERS_TO_STATE', response.data.teachers)
                     commit('SET_ONE_STROKE_TO_STATE', response.data.stroke)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_TEACHERS_VERSION_FROM_API({commit}, {faculty, year}) {
      return axios.get(resource_uri_get_version,
                      {params: {faculty: faculty, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_TEACHERS_VERSION_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async SET_TEACHER_TO_PRACT_STROKE({commit}, {uuid, group_id, group_type,
                                            department, teacher_id, load_version, key, bc, vc}) {
      return axios.put(resource_uri_set_load_pract,
                      {},
                      {params: {uuid: uuid, group_id: group_id, 
                                group_type: group_type, department: department, 
                                teacher_id: teacher_id, load_version: load_version,
                                key: key, bc: bc, vc: vc},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                    console.log(response.data)
                     commit('SET_DEP_TEACHERS_TO_STATE', response.data.teachers)
                     commit('SET_ONE_STROKE_TO_STATE', response.data.stroke)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_TEACHER_TO_PRACT_STROKE({commit}, {uuid, group_id, group_type,
                                            department, teacher_id, load_version, key}) {
      return axios.delete(resource_uri_set_load_pract,
                        {params: {uuid: uuid, group_id: group_id, 
                                  group_type: group_type, department: department, 
                                  teacher_id: teacher_id, load_version: load_version,
                                  key: key},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                    console.log(response.data)
                     commit('SET_DEP_TEACHERS_TO_STATE', response.data.teachers)
                     commit('SET_ONE_STROKE_TO_STATE', response.data.stroke)
                  })
                  .catch(errorHandler.errorHandler)
    },        

  },
  getters: {
    DEPS_STAT(state) {
      return state.deps_stat
    },
    TEACHERS_LOAD(state) {
      return state.teachers_load
    },
    DEP_TEACHERS(state) {
      return state.dep_teachers
    },
    TEACHERS_VERSION(state) {
      return state.teachers_version
    }
  }  
}    
import store from './index'

const errorHandler = (error) => {
  store.commit('changeFlag', 'Произошла ошибка на сервере. Попробуйте повторить действие или обновить страницу.')
  return Promise.reject(error)
}

const errorHandlerWithMsg = (error, msg) => {
  store.commit('changeFlag', msg)
  return Promise.reject(error)
}


export default {
  errorHandler: errorHandler,
  errorHandlerWithMsg: errorHandlerWithMsg
}
import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_tree = 'contingent/profilestree'
const resource_uri_get_groups_contingent = 'contingent/groups'
const resource_uri_add_group_profile = 'contingent/groups'
const resource_uri_edit_group_name = 'contingent/groups/name'
const resource_uri_edit_subgroup_name = 'contingent/subgroups/name'
const resource_uri_edit_group_contingent = 'contingent/groups'
const resource_uri_edit_subgroup_contingent = 'contingent/subgroups'
const resource_uri_add_subgroups_block = 'contingent/subgroups'
const resource_uri_add_choice_group = 'contingent/groupschoice'
const resource_uri_disable_subgroups_block = 'contingent/subgroups/deactivation'
const resource_uri_delete_groups_choice = 'contingent/groupschoice'
const resource_uri_edit_groupchoice_contingent = 'contingent/groupschoice'
const resource_uri_generate_groups = "contingent/generateauto"


export default {
  state: () => ({
    profilesTree: [],
    groupsContingent: null
  }),
  mutations: {
    SET_PROFILES_TREE_TO_STATE: (state, objects) => {
      state.profilesTree = objects
    },
    SET_GROUPS_CONTINGENT_TO_STATE: (state, objects) => {
      state.groupsContingent = objects
    },
    ADD_GROUP_TO_STATE: (state, object) => {
      state.groupsContingent[object.group.course].groups.push(object.group);
      state.groupsContingent[object.group.course].common = object.common;
    },
    EDIT_GROUP_NAME_IN_STATE: (state, object) => {
      let grs = state.groupsContingent[object.course].groups;
      let ind = grs.findIndex(e => e.id == object.id);
      if (ind >= 0) {
        grs[ind].name = object.name;
      }
    },
    EDIT_SUBGROUP_NAME_IN_STATE: (state, object) => {
      let grs = state.groupsContingent[object.group.course].groups;
      let ind = grs.findIndex(e => e.id == object.group.id);
      if (ind >= 0) {
        let subs = grs[ind].blocks[object.subgroup.block_id];
        for (let sub of subs) {
          if (sub.id == object.subgroup.id) {
            sub.name = object.subgroup.name;
          }
        }
      }
    },
    EDIT_GROUP_CONTINGENT_IN_STATE: (state, object) => {
      let grs = state.groupsContingent[object.group.course].groups;
      state.groupsContingent[object.group.course].common = object.common;
      let ind = grs.findIndex(e => e.id == object.group.id);
      if (ind >= 0) {
        grs[ind].budzhet_count = object.group.budzhet_count;
        grs[ind].budzhet_foreign = object.group.budzhet_foreign;
        grs[ind].vnebudzhet_count = object.group.vnebudzhet_count;
        grs[ind].vnebudzhet_foreign = object.group.vnebudzhet_foreign;
      }
    },
    EDIT_SUBGROUP_CONTINGENT_IN_STATE: (state, object) => {
      let grs = state.groupsContingent[object.group.course].groups;
      let ind = grs.findIndex(e => e.id == object.group.id);
      if (ind >= 0) {
        let subs = grs[ind].blocks[object.subgroup.block_id];
        for (let sub of subs) {
          if (sub.id == object.subgroup.id) {
            sub.budzhet_count = object.subgroup.budzhet_count;
            sub.budzhet_foreign = object.subgroup.budzhet_foreign;
            sub.vnebudzhet_count = object.subgroup.vnebudzhet_count;
            sub.vnebudzhet_foreign = object.subgroup.vnebudzhet_foreign;
          }
        }
      }
    },
    ADD_SUBGROUPS_TO_STATE: (state, objects) => {
      let grs = state.groupsContingent[objects.group.course].groups;
      let ind = grs.findIndex(e => e.id == objects.group.id);
      if (ind >= 0) {
        let key = Object.keys(objects.subgroups)[0]
        console.log(objects.subgroups[key])
        grs[ind].blocks[key] = objects.subgroups[key]
      }
    },
    DELETE_SUBGROUP_IN_STATE: (state, object) => {
      let grs = state.groupsContingent[object.course].groups;
      state.groupsContingent[object.course].common = object.common;
      let ind = grs.findIndex(e => e.id == object.group_id);
      if (ind >= 0) {
        delete grs[ind].blocks[object.block_id];
      }
    },
    DELETE_GROUP_FROM_STATE: (state, object) => {
      let grs = state.groupsContingent[object.course].groups;
      let ind = grs.findIndex(e => e.id == object.group_id);
      if (ind >= 0) {
        grs.splice(ind, 1)
      }
      state.groupsContingent[object.course].common = object.common;
    },
    ADD_CHOICE_GROUP_TO_STATE: (state, object) => {
      let grs = state.groupsContingent[object.group.course].blocks;
      grs[object.block].push(object.group);
    },
    DELETE_CHOICE_GROUP_FROM_STATE: (state, object) => {
      let grs = state.groupsContingent[object.course].blocks[object.block];
      grs.splice(object.idInList, 1)
    },
    EDIT_GROUPCHOICE_CONTINGENT_IN_STATE: (state, object) => {
      let grs = state.groupsContingent[object.course].blocks[object.block];
      grs[object.idInList].GroupChoice.budzhet_count = object.group.budzhet_count;
      grs[object.idInList].GroupChoice.budzhet_foreign = object.group.budzhet_foreign;
      grs[object.idInList].GroupChoice.vnebudzhet_count = object.group.vnebudzhet_count;
      grs[object.idInList].GroupChoice.vnebudzhet_foreign = object.group.vnebudzhet_foreign;
    }
  },
  actions: {
    async GET_PROFILES_TREE_FROM_API({dispatch, commit}, {faculty}) {
      dispatch('CLEAR_PROFILES_TREE')
      return axios.get(resource_uri_get_tree,
                      {params: {faculty: faculty},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_PROFILES_TREE_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_PROFILES_TREE({commit}) {
      commit('SET_PROFILES_TREE_TO_STATE', [])
    },
    async GET_GROUPS_CONTINGENT_FROM_API({dispatch, commit}, {year, profile_id}) {
      dispatch('CLEAR_GROUPS_CONTINGENT')
      return axios.get(resource_uri_get_groups_contingent,
                      {params: {year: year, profile_id: profile_id},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_GROUPS_CONTINGENT_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_GROUPS_CONTINGENT({commit}) {
      commit('SET_GROUPS_CONTINGENT_TO_STATE', [])
    },
    async ADD_PROFILE_GROUP_TO_API({commit}, {group_name, course, profile_id,
                                              year, bc, vc, bf, vf}) {
      return axios.post(resource_uri_add_group_profile,  
              {},   
              {
               params: {year: year, profile_id: profile_id, 
                        name: group_name, course: course, bc: bc,
                        vc: vc, bf: bf, vf: vf}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
        .then((response) => {
          commit('ADD_GROUP_TO_STATE', response.data)
        })
        .catch(errorHandler.errorHandler)
    },
    async EDIT_GROUP_NAME_IN_API({commit}, {group_name, group_id}) {
      return axios.put(resource_uri_edit_group_name,
                        {},
                        {params: {new_name: group_name, group_id: group_id},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    commit('EDIT_GROUP_NAME_IN_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_SUBGROUP_NAME_IN_API({commit}, {group_name, group_id}) {
      return axios.put(resource_uri_edit_subgroup_name,
                        {},
                        {params: {new_name: group_name, subgroup_id: group_id},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    commit('EDIT_SUBGROUP_NAME_IN_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_GROUP_CONTINGENT_IN_API({commit}, {group_id, bc, vc, bf, vf}) {
      return axios.put(resource_uri_edit_group_contingent,
                        {},
                        {params: {group_id: group_id, bc: bc, 
                                  vc: vc, bf: bf, vf: vf},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    commit('EDIT_GROUP_CONTINGENT_IN_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_SUBGROUP_CONTINGENT_IN_API({commit}, {group_id, bc, vc, bf, vf}) {
      return axios.put(resource_uri_edit_subgroup_contingent,
                        {},
                        {params: {subgroup_id: group_id, bc: bc, 
                                  vc: vc, bf: bf, vf: vf},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    commit('EDIT_SUBGROUP_CONTINGENT_IN_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_SUBGROUPS_BLOCK_TO_API({commit}, {group_id, subgroups}) {
      return axios.post(resource_uri_add_subgroups_block,  
              subgroups,   
              {
               params: {group_id: group_id}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
        .then((response) => {
          commit('ADD_SUBGROUPS_TO_STATE', response.data)
        })
        .catch(errorHandler.errorHandler)
    },
    async DISABLE_SUBGROUP_BLOCK_IN_API({commit}, {block_id, course, group_id}) {
      return axios.put(resource_uri_disable_subgroups_block,
                        {},
                        {params: {block_id: block_id},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then(() => {
                    commit('DELETE_SUBGROUP_IN_STATE', {"block_id": block_id, 
                                                        "course": course,
                                                        "group_id": group_id})
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_GROUP_FROM_API({commit}, {group_id}) {
      return axios.delete(resource_uri_get_groups_contingent,
                      {params: {group_id: group_id},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('DELETE_GROUP_FROM_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_CHOICE_GROUP_TO_API({commit}, {uuid, block}) {
      return axios.post(resource_uri_add_choice_group,  
              {}, 
              {
               params: {uuid: uuid, block: block}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
        .then((response) => {
          commit('ADD_CHOICE_GROUP_TO_STATE', response.data)
        })
        .catch(errorHandler.errorHandler)
    },
    async DELETE_CHOICE_GROUP_IN_API({commit}, {block, id, course, idInList}) {
      return axios.delete(resource_uri_delete_groups_choice,
                          {params: {group_id: id},
                           headers: {
                              'Access-Control-Allow-Origin': '*', 
                              'Content-Type': 'application/json', 
                              'Authorization': `Bearer ${localStorage.getItem('token')}`
                           }})
                  .then(() => {
                     commit('DELETE_CHOICE_GROUP_FROM_STATE', {"block": block, 
                                                               "course": course,
                                                               "idInList": idInList})
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_GROUP_CHOICE_CONTINGENT_IN_API({commit}, {group_id, bc, vc, bf, vf,
                                                         block, course, idInList}) {
      return axios.put(resource_uri_edit_groupchoice_contingent,
                        {},
                        {params: {group_id: group_id, bc: bc, 
                                  vc: vc, bf: bf, vf: vf},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    commit('EDIT_GROUPCHOICE_CONTINGENT_IN_STATE', {"block": block, 
                                                                    "course": course,
                                                                    "idInList": idInList,
                                                                    "group": response.data})
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GENERATE_GROUPS_IN_API(_, {faculty, year}) {
      return axios.post(resource_uri_generate_groups,  
              {},   
              {
               params: {year: year, faculty: faculty}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
        .then(() => {})
        .catch(errorHandler.errorHandler)
    },              
  },
  getters: {
    PROFILES_TREE(state) {
      return state.profilesTree
    },
    GROUPS_CONTINGENT(state) {
      return state.groupsContingent
    }
  }
}
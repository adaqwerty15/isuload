import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_faculties = 'dirs/faculties'
const resource_uri_compare_faculties = 'dirs/faculties/compare1c'


export default {
  state: () => ({
    faculties: []
  }),
  mutations: {
    SET_FACULTIES_TO_STATE: (state, objects) => {
      state.faculties = objects
    },
  },
  actions: {
    async GET_FACULTIES_FROM_API({dispatch, commit}, {ids:ids}) {
      dispatch('CLEAR_FACULTIES')
      return axios.get(resource_uri_get_faculties,
                      {params: {ids: ids},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_FACULTIES_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_FACULTIES({commit}) {
      commit('SET_FACULTIES_TO_STATE', [])
    },
    async ADD_FACULTY_TO_API({commit}, {name, group_template}) {
       return axios.post(resource_uri_get_faculties,
                        {},
                        {params: {name: name, group_template: group_template},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_FACULTIES_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_FACULTY_IN_API(_, {name, group_template, faculty_id}) {
       return axios.put(resource_uri_get_faculties,
                        {},
                        {params: {name: name, group_template: group_template,
                                  faculty_id: faculty_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then(() => {})
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_FACULTY_FROM_API({commit}, {faculty_id}) {
       return axios.delete(resource_uri_get_faculties,
                        {params: {faculty_id: faculty_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_FACULTIES_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async COMPARE_FACULTIES_IN_API({commit}) {
       return axios.post(resource_uri_compare_faculties,
                        {},
                        {params: {},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_FACULTIES_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    FACULTIES(state) {
      return state.faculties
    }
  }
}
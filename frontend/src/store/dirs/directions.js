import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_profiles_tree = 'dirs/profiles/tree'
const resource_uri_get_qualifications = 'dirs/qualifications'
const resource_uri_compare_directions = 'dirs/directions/compare1c'
const resource_uri_directions = 'dirs/directions'
const resource_uri_profiles = 'dirs/profiles'


export default {
  state: () => ({
    profiles_tree_dir: [],
    quals: []
  }),
  mutations: {
    SET_PROFILES_TREE_DIR_TO_STATE: (state, objects) => {
      state.profiles_tree_dir = objects
    },
    SET_QUALS_TO_STATE: (state, objects) => {
      state.quals = objects
    },
    ADD_DIRECTION_TO_STATE: (state, objects) => {
      let ind = state.profiles_tree_dir.findIndex(e => e.key == objects.data.faculty_id.toString());
      if (ind >= 0) {
        state.profiles_tree_dir[ind].children.push(objects);
      }
    },
    EDIT_DIRECTION_IN_STATE: (state, objects) => {
      let ind = state.profiles_tree_dir.findIndex(e => e.key == objects.data.faculty_id.toString());
      if (ind >= 0) {
        let indd = state.profiles_tree_dir[ind].children.findIndex(e => e.data.id == objects.data.id);
        if (indd >= 0)
          state.profiles_tree_dir[ind].children[indd].data = objects.data;
      }
    },
    DELETE_DIRECTION_FROM_STATE: (state, objects) => {
      let ind = state.profiles_tree_dir.findIndex(e => e.key == objects.data.faculty_id.toString());
      if (ind >= 0) {
        let indd = state.profiles_tree_dir[ind].children.findIndex(e => e.data.id == objects.data.id);
        if (indd >= 0)
          state.profiles_tree_dir[ind].children.splice(indd, 1);
      }
    },
    ADD_PROFILE_TO_STATE: (state, objects) => {
      let ind = state.profiles_tree_dir.findIndex(e => e.key == objects.data.faculty_id.toString());
      if (ind >= 0) {
        let indd = state.profiles_tree_dir[ind].children.findIndex(e => e.data.id == objects.data.direction_id);
        if (indd >= 0)
          state.profiles_tree_dir[ind].children[indd].children.push(objects);
      }
    },
    EDIT_PROFILE_IN_STATE: (state, objects) => {
      let ind = state.profiles_tree_dir.findIndex(e => e.key == objects.data.faculty_id.toString());
      if (ind >= 0) {
        let indd = state.profiles_tree_dir[ind].children.findIndex(e => e.data.id == objects.data.direction_id);
        if (indd >= 0) {
          let indp = state.profiles_tree_dir[ind].children[indd].children.findIndex(e => e.data.id == objects.data.id);
          if (indp >= 0)
            state.profiles_tree_dir[ind].children[indd].children[indp].data = objects.data;
        }
      }
    },
    DELETE_PROFILE_FROM_STATE: (state, objects) => {
      let ind = state.profiles_tree_dir.findIndex(e => e.key == objects.data.faculty_id.toString());
      if (ind >= 0) {
        let indd = state.profiles_tree_dir[ind].children.findIndex(e => e.data.id == objects.data.direction_id);
        if (indd >= 0) {
          let indp = state.profiles_tree_dir[ind].children[indd].children.findIndex(e => e.data.id == objects.data.id);
          if (indp >= 0)
            state.profiles_tree_dir[ind].children[indd].children.splice(indp, 1);
        }
      }
    },

  },
  actions: {
    async GET_PROFILES_TREE_DIR_FROM_API({dispatch, commit}) {
      dispatch('CLEAR_PROFILES_TREE_DIR')
      return axios.get(resource_uri_get_profiles_tree,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_PROFILES_TREE_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_PROFILES_TREE_DIR({commit}) {
      commit('SET_PROFILES_TREE_DIR_TO_STATE', [])
    },
    async GET_QUALS_FROM_API({commit}) {
      return axios.get(resource_uri_get_qualifications,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_QUALS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async COMPARE_DIRECTIONS_IN_API({commit}) {
       return axios.post(resource_uri_compare_directions,
                        {},
                        {params: {},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_PROFILES_TREE_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_DIRECTION_TO_API({commit}, {name, shifr, faculty_id, qual_id, group_template}) {
       return axios.post(resource_uri_directions,
                        {},
                        {params: {name: name, group_template: group_template, shifr: shifr,
                                  faculty_id: faculty_id, qual_id: qual_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('ADD_DIRECTION_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_DIRECTION_IN_API({commit}, {name, shifr, faculty_id, qual_id, group_template, did}) {
       return axios.put(resource_uri_directions,
                        {},
                        {params: {name: name, group_template: group_template, shifr: shifr,
                                  faculty_id: faculty_id, qual_id: qual_id, d_id: did},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('EDIT_DIRECTION_IN_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_DIRECTION_FROM_API({commit}, {faculty_id, did}) {
       return axios.delete(resource_uri_directions,
                        {params: {faculty_id: faculty_id, d_id: did},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('DELETE_DIRECTION_FROM_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_PROFILE_TO_API({commit}, {name, faculty_id, d_id}) {
       return axios.post(resource_uri_profiles,
                        {},
                        {params: {name: name, faculty_id: faculty_id, d_id: d_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('ADD_PROFILE_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_PROFILE_IN_API({commit}, {name, faculty_id, d_id, p_id}) {
       return axios.put(resource_uri_profiles,
                        {},
                        {params: {name: name, d_id: d_id, p_id: p_id,
                                  faculty_id: faculty_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('EDIT_PROFILE_IN_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_PROFILE_FROM_API({commit}, {faculty_id, d_id, p_id}) {
       return axios.delete(resource_uri_profiles,
                        {params: {faculty_id: faculty_id, d_id: d_id, p_id: p_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('DELETE_PROFILE_FROM_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    PROFILES_TREE_DIR(state) {
      return state.profiles_tree_dir
    },
    QUALS(state) {
      return state.quals
    }
  }
}
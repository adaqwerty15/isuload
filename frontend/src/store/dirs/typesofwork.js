import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_types_of_work = 'dirs/typesofwork'


export default {
  state: () => ({
    types_of_work_dir: []
  }),
  mutations: {
    SET_TYPES_OF_WORK_DIR_TO_STATE: (state, objects) => {
      state.types_of_work_dir = objects
    },
  },
  actions: {
    async GET_TYPES_OF_WORK_DIR_FROM_API({dispatch, commit}) {
      dispatch('CLEAR_TYPES_OF_WORK_DIR')
      return axios.get(resource_uri_types_of_work,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_TYPES_OF_WORK_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_TYPES_OF_WORK_DIR_({commit}) {
      commit('SET_TYPES_OF_WORK_DIR_TO_STATE', [])
    },
    async ADD_TYPE_OF_WORK_DIR_TO_API({commit}, {name, code, active_calc, type_w}) {
       return axios.post(resource_uri_types_of_work,
                        {},
                        {params: {name: name, code: code, active_calc: active_calc, type_w: type_w},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TYPES_OF_WORK_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_TYPE_OF_WORK_DIR_IN_API({commit}, {name, code, active_calc, type_w, tw_id}) {
       return axios.put(resource_uri_types_of_work,
                        {},
                        {params: {name: name, code: code, active_calc: active_calc, 
                                  type_w: type_w, tw_id: tw_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TYPES_OF_WORK_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_TYPE_OF_WORK_DIR_FROM_API({commit}, {tw_id}) {
       return axios.delete(resource_uri_types_of_work,
                        {params: {tw_id: tw_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TYPES_OF_WORK_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    TYPES_OF_WORK_DIR(state) {
      return state.types_of_work_dir
    }
  }
}
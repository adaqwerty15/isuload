import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_departments = 'dirs/deps'


export default {
  state: () => ({
    dir_departments: []
  }),
  mutations: {
    SET_DIR_DEPARTMENTS_TO_STATE: (state, objects) => {
      state.dir_departments = objects
    },
  },
  actions: {
    async GET_DIR_DEPARTMENTS_FROM_API({dispatch, commit}) {
      dispatch('CLEAR_DIR_DEPARTMENTS')
      return axios.get(resource_uri_departments,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_DIR_DEPARTMENTS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_DIR_DEPARTMENTS({commit}) {
      commit('SET_DIR_DEPARTMENTS_TO_STATE', [])
    },
    async ADD_DIR_DEPARTMENT_TO_API({commit}, {name, number, faculty_id}) {
       return axios.post(resource_uri_departments,
                        {},
                        {params: {name: name, number: number, faculty_id: faculty_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_DIR_DEPARTMENTS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_DIR_DEPARTMENT_IN_API({commit}, {name, number, faculty_id, dep_id}) {
       return axios.put(resource_uri_departments,
                        {},
                        {params: {name: name, number: number, faculty_id: faculty_id, dep_id: dep_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_DIR_DEPARTMENTS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_DIR_DEPARTMENT_FROM_API({commit}, {dep_id}) {
       return axios.delete(resource_uri_departments,
                        {params: {dep_id: dep_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_DIR_DEPARTMENTS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    DIR_DEPARTMENTS(state) {
      return state.dir_departments
    }
  }
}
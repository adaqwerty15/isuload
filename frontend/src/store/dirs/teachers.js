import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_teachers_dir = 'dirs/teachers'
// const resource_uri_get_qualifications = 'dirs/qualifications'
const resource_uri_compare_teachers = 'dirs/teachers/compare1c'


export default {
  state: () => ({
    teachers_dir: [],
  }),
  mutations: {
    SET_TEACHERS_DIR_TO_STATE: (state, objects) => {
      state.teachers_dir = objects
    },
  },
  actions: {
    async GET_TEACHERS_DIR_FROM_API({dispatch, commit}, {faculty}) {
      dispatch('CLEAR_TEACHERS_DIR')
      return axios.get(resource_uri_get_teachers_dir,
                      {params: {faculty: faculty},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_TEACHERS_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_TEACHERS_DIR({commit}) {
      commit('SET_TEACHERS_DIR_TO_STATE', [])
    },
    async ADD_TEACHER_TO_API({commit}, {name, surname, fathername,
                                        position, degree, academic_rank,
                                        rate, state, dep, faculty}) {
       return axios.post(resource_uri_get_teachers_dir,
                        {},
                        {params: {name: name, surname: surname, fathername: fathername,
                                  position: position, degree: degree, academic_rank: academic_rank,
                                  rate: rate, state: state, dep: dep, faculty: faculty},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TEACHERS_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_TEACHER_IN_API({commit}, {name, surname, fathername,
                                        position, degree, academic_rank,
                                        rate, state, dep, t_id, faculty}) {
       return axios.put(resource_uri_get_teachers_dir,
                        {},
                        {params: {name: name, surname: surname, fathername: fathername,
                                  position: position, degree: degree, academic_rank: academic_rank,
                                  rate: rate, state: state, dep: dep, t_id: t_id, faculty: faculty},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TEACHERS_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_TEACHER_FROM_API({commit}, {t_id, faculty}) {
       return axios.delete(resource_uri_get_teachers_dir,
                        {params: {t_id: t_id, faculty: faculty},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TEACHERS_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async COMPARE_TEACHERS_IN_API({commit}, {faculty}) {
       return axios.post(resource_uri_compare_teachers,
                        {},
                        {params: {faculty: faculty},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_TEACHERS_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    TEACHERS_DIR(state) {
      return state.teachers_dir
    },
  }
}
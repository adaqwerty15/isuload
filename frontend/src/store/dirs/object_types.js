import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_object_types = 'dirs/objecttypes'


export default {
  state: () => ({
    object_types_dir: []
  }),
  mutations: {
    SET_DIR_OBJECT_TYPES_DIR_TO_STATE: (state, objects) => {
      state.object_types_dir = objects
    },
  },
  actions: {
    async GET_OBJECT_TYPES_DIR_FROM_API({dispatch, commit}) {
      dispatch('CLEAR_OBJECT_TYPES_DIR')
      return axios.get(resource_uri_object_types,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_DIR_OBJECT_TYPES_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_OBJECT_TYPES_DIR({commit}) {
      commit('SET_DIR_OBJECT_TYPES_DIR_TO_STATE', [])
    },
    async ADD_OBJECT_TYPES_DIR_TO_API({commit}, {name, add_inf}) {
       return axios.post(resource_uri_object_types,
                        {},
                        {params: {name: name, add_inf: add_inf},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_DIR_OBJECT_TYPES_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_OBJECT_TYPES_DIR_IN_API({commit}, {name, add_inf, ot_id}) {
       return axios.put(resource_uri_object_types,
                        {},
                        {params: {name: name, add_inf: add_inf, ot_id: ot_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_DIR_OBJECT_TYPES_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_OBJECT_TYPES_DIR_FROM_API({commit}, {ot_id}) {
       return axios.delete(resource_uri_object_types,
                        {params: {ot_id: ot_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_DIR_OBJECT_TYPES_DIR_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    OBJECT_TYPES_DIR(state) {
      return state.object_types_dir
    }
  }
}
import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_plans = 'plans'
const resource_uri_add_plans = 'plans/uploadfile'
const resource_uri_set_cont = 'plans/contingent'


export default {
  state: () => ({
    plans: [],
    res_upload: []
  }),
  mutations: {
    SET_PLANS_TO_STATE: (state, objects) => {
      state.plans = objects
    },
    ADD_RES_UPLOAD_TO_STATE: (state, objects) => {
      state.res_upload = objects
    }, 
    DELETE_PLAN_FROM_STATE: (state, object) => {
      state.plans = state.plans.filter(
        rep => rep.id != object);
    },
  },
  actions: {
    async GET_PLANS_FROM_API({dispatch, commit}, {year: year, faculty: faculty}) {
      dispatch('CLEAR_PLANS')
      return axios.get(resource_uri_get_plans,
                      {params: {year: year, faculty: faculty},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_PLANS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_PLANS({commit}) {
      commit('SET_PLANS_TO_STATE', [])
    },
    async ADD_PLANS_TO_API({commit}, {year: year, faculty: faculty, files: files}) {
      let formData = new FormData();
      formData.append('faculty', faculty);
      formData.append('year', year);
      for (let file of files) {
          formData.append('files', file, file.name);
      }
      
      return axios.post(resource_uri_add_plans, 
              formData,
              {
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'multipart/form-data', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
        .then((response) => {
          commit('ADD_RES_UPLOAD_TO_STATE', response.data)
        })
        .catch(errorHandler.errorHandler)
    },
    async DELETE_PLAN_FROM_API({commit}, {plan_id}) {
      return axios.delete(resource_uri_get_plans, 
                          {params: {plan_id: plan_id},
                          headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                          }})
        .then((response) => {
          commit('DELETE_PLAN_FROM_STATE', response.data)
        })
        .catch(errorHandler.errorHandler)
    },
    async SET_PLAN_CONTINGENT_IN_API(_, {plan_id, bc, vc, bg, vg}) {
      return axios.put(resource_uri_set_cont,
                        {},
                        {params: {plan_id: plan_id, bc: bc, vc: vc, bg: bg, vg: vg},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then(() => {
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    PLANS(state) {
      return state.plans
    },
    RES_UPLOAD(state) {
      return state.res_upload
    }
  }
}
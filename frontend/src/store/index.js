import { createStore } from 'vuex'

import auth from './base/auth'
import common from './base/common'
import faculties from './dirs/faculties'
import departments from './dirs/departments'
import objecttypes from './dirs/object_types'
import typesofwork from './dirs/typesofwork'
import directions from './dirs/directions'
import teachersdir from './dirs/teachers'
import plans from './plans/plans'
import contingent from './contingent/contingent'
import versions from './load/versions'
import loadtable from './load/loadtable'
import teachersload from './teachers/teachersload'
import columns from './load/columns'

export default createStore({
  state: {
    flag: false,
    msg: ''
  },
  mutations: {
    changeFlag (state, msg) {
      state.flag = !state.flag;
      if (msg)
        state.msg = msg
    }
  },
  actions: {
  },
  modules: {
    auth,
    faculties,
    plans,
    common,
    contingent,
    versions,
    loadtable,
    teachersload,
    columns,
    directions,
    teachersdir,
    departments,
    objecttypes,
    typesofwork
  }
})

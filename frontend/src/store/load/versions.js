import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_versions = 'load/versions'
const resource_uri_get_version = 'load/version'
const resource_uri_set_current = 'versions/current'
const resource_uri_approve_version = 'versions/approve'
const resource_uri_calc_version = 'versions/calc'
const resource_uri_calc_version_vn = 'versions/calcvn'
const resource_uri_versions = 'versions'

export default {
  state: () => ({
    versions: [],
    version: null
  }),
  mutations: {
    SET_VERSIONS_TO_STATE: (state, objects) => {
      state.versions = objects
    },
    SET_VERSION_TO_STATE: (state, objects) => {
      state.version = objects
    }
  },
  actions: {
    async GET_LOAD_VERSIONS_FROM_API({dispatch, commit}, {faculty, year}) {
      dispatch('CLEAR_VERSIONS')
      return axios.get(resource_uri_get_versions,
                      {params: {faculty: faculty, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_VERSIONS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_LOAD_VERSION_FROM_API({commit}, {version}) {
      return axios.get(resource_uri_get_version,
                      {params: {version: version},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_VERSION_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_VERSIONS({commit}) {
      commit('SET_VERSIONS_TO_STATE', [])
    },
    async SET_VERSION_STATE_TO_API({commit}, {version, own, other, comment}) {
      return axios.post(resource_uri_approve_version,  
              comment,   
              {
               params: {version: version, own: own,
                        other: other}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
             .then((response) => {
               commit('SET_VERSIONS_TO_STATE', response.data)
              })
             .catch(errorHandler.errorHandler)
    },
    async CREATE_VERSION({commit}, {faculty_id, year, name}) {
      return axios.post(resource_uri_calc_version,  
              {},   
              {
               params: {up_ver: -1, new_ver: false, faculty_id: faculty_id, 
                        year: year, name: name}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
             .then((response) => {
               commit('SET_VERSIONS_TO_STATE', response.data)
              })
             .catch(errorHandler.errorHandler)
    },
    async UPDATE_VERSION_IN_API({commit}, {faculty_id, year, name, new_ver, up_ver}) {
      return axios.post(resource_uri_calc_version,  
              {},   
              {
               params: {up_ver: up_ver, new_ver: new_ver, faculty_id: faculty_id, 
                        year: year, name: name}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
             .then((response) => {
               commit('SET_VERSIONS_TO_STATE', response.data)
              })
             .catch(errorHandler.errorHandler)
    },
    async UPDATE_VERSION_VN_IN_API(_, {version}) {
      return axios.post(resource_uri_calc_version_vn,  
              {},   
              {
               params: {version}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
             .then(() => {})
             .catch(errorHandler.errorHandler)
    },
    async DELETE_VERSION_FROM_API({commit}, {version}) {
      return axios.delete(resource_uri_versions, 
                        {
                         params: {version: version}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                     commit('SET_VERSIONS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async SET_CURRENT_TO_API({commit}, {version}) {
      return axios.put(resource_uri_set_current,
                        {},
                        {params: {version: version},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    commit('SET_VERSIONS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    LOAD_VERSIONS(state) {
      return state.versions
    },
    LOAD_VERSION(state) {
      return state.version
    }
  }  
}    
import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_columns = 'columns'
const resource_uri_copy_columns = 'columns/copy'
const resource_uri_get_column_formuls = 'columns/formuls'
const resource_uri_columns_visibility = 'columns/visibility'
const resource_uri_columns_existence = 'columns/existence'
const resource_uri_get_formuls_variables = 'columns/variables'
const resource_uri_get_formuls_constrs = 'columns/constrs'

export default {
  state: () => ({
    columns: [],
    formuls: [],
    formuls_variables: {},
    formuls_constrs: {}
  }),
  mutations: {
    SET_COLUMNS_TO_STATE: (state, objects) => {
      state.columns = objects
    },
    SET_COLUMN_FORMULS_TO_STATE: (state, objects) => {
      state.formuls = objects
    },
    SET_FORMULS_VARIABLES_TO_STATE: (state, objects) => {
      state.formuls_variables = objects
    },
    SET_FORMULS_CONSTRS_TO_STATE: (state, objects) => {
      state.formuls_constrs = objects
    }
  },
  actions: {
    async GET_COLUMNS_FROM_API({dispatch, commit}, {faculty, year}) {
      dispatch('CLEAR_COLUMNS')
      return axios.get(resource_uri_get_columns,
                      {params: {faculty: faculty, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_COLUMNS({commit}) {
      commit('SET_COLUMNS_TO_STATE', [])
    },
    async SET_COLUMNS_VISIBILITY_TO_API({commit}, {column_id}) {
      return axios.put(resource_uri_columns_visibility,
                      {},
                      {params: {column_id: column_id},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_NEW_COLUMN_TO_API({commit}, {faculty, year, name, alias, parent_id}) {
      return axios.post(resource_uri_get_columns,
                        {},
                        {params: {faculty: faculty, year: year,
                                  name: name, alias: alias,
                                  parent_id: parent_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_COLUMN_IN_API({commit}, {column_id, name, alias}) {
      return axios.put(resource_uri_get_columns,
                        {},
                        {params: {column_id: column_id,
                                  name: name, alias: alias},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_COLUMN_EXISTENCE_IN_API({commit}, {column_id}) {
      return axios.put(resource_uri_columns_existence,
                        {},
                        {params: {column_id: column_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_COLUMN_FORMULS_FROM_API({dispatch, commit}, {column_id}) {
      dispatch('CLEAR_COLUMN_FORMULS')
      return axios.get(resource_uri_get_column_formuls,
                      {params: {column_id: column_id},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_COLUMN_FORMULS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_COLUMN_FORMULS({commit}) {
      commit('SET_COLUMN_FORMULS_TO_STATE', [])
    },
    async GET_FORMULS_VARIABLES_FROM_API({commit}) {
      return axios.get(resource_uri_get_formuls_variables,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_FORMULS_VARIABLES_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_FORMULS_CONSTRS_FROM_API({commit}) {
      return axios.get(resource_uri_get_formuls_constrs,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_FORMULS_CONSTRS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_FORMULA_TO_API({commit}, {column_id, data}) {
      return axios.post(resource_uri_get_column_formuls,
                        data,
                        {params: {column_id: column_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_COLUMN_FORMULS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async EDIT_FORMULA_IN_API({commit}, {column_id, formul_id, data}) {
      return axios.put(resource_uri_get_column_formuls,
                        data,
                        {params: {column_id: column_id, formul_id: formul_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_COLUMN_FORMULS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_FORMULA_FROM_API({commit}, {column_id, formul_id}) {
      return axios.delete(resource_uri_get_column_formuls,
                        {params: {column_id: column_id, formul_id: formul_id},
                        headers: {
                          'Access-Control-Allow-Origin': '*', 
                          'Content-Type': 'application/json', 
                          'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }})
                  .then((response) => {
                     commit('SET_COLUMN_FORMULS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async COPY_COLUMNS_FROM_API({commit}, {faculty, year}) {
      return axios.post(resource_uri_copy_columns, {},
                      {params: {faculty: faculty, year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
  },
  getters: {
    COLUMNS(state) {
      return state.columns
    },
    COLUMN_FORMULS(state) {
      return state.formuls
    },
    FORMULS_VARIABLES(state) {
      return state.formuls_variables
    },
    FORMULS_CONSTRS(state) {
      return state.formuls_constrs
    }
  }
}    

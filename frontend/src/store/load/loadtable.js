import axios from 'axios'
import errorHandler from '../_utils'

const resource_uri_get_columns = 'load/table/columns'
const resource_uri_get_load = 'load/table/load'
const resource_uri_get_faculty_deps = 'dirs/departments'
const resource_uri_set_activity = 'load/table/loadstroke/active'
const resource_uri_set_department = 'load/table/loadstroke/department'
const resource_uri_get_subgroups_stroke_info = 'load/table/loadstroke/subgroupsinfo'
const resource_uri_get_flows = 'load/table/flows'
const resource_uri_get_stroke_flows = 'load/table/stroke/flows'
const resource_uri_appoint_subgroups = 'load/table/loadstroke/appointsubgroups'
const resource_uri_join_subgroups = 'load/table/loadstroke/joinsubgroups'
const resource_uri_set_subgroup_dep = 'load/table/loadstroke/subgroupdep'
const resource_uri_add_flow = 'load/table/flow'
const resource_uri_add_flow_stroke = 'load/table/stroke/flow'
const resource_uri_add_own_stroke = 'load/table/ownstroke'
const resource_uri_get_stroke_object_types = 'dirs/ownobjecttypes'
const resource_uri_get_columns_for_change = "dirs/columnsforchange"
const resource_uri_set_column_value = "load/table/columnvalue"


export default {
  state: () => ({
    columns: [],
    ownTableLoad: {},
    faculty_deps: [],
    all_faculty_deps: [],
    subgroups_stroke_info: {},
    flows: [],
    stroke_flows: [],
    stroke_object_types: [],
    columns_for_change: []
  }),
  mutations: {
    SET_COLUMNS_TO_STATE: (state, objects) => {
      state.columns = objects
    },
    SET_OWN_TABLE_LOAD_TO_STATE: (state, objects) => {
      state.ownTableLoad = objects
    },
    SET_ACTIVITY_IN_STATE: (_, objects) => {
      let ind = objects.profile.loadstrokes_budzhet.findIndex(e => e.uuid == objects.uuid); 
      if (ind >= 0) {     
         objects.profile.loadstrokes_budzhet[ind].show = objects.value;
      }
      ind = objects.profile.loadstrokes_vnebudzhet.findIndex(e => e.uuid == objects.uuid); 
      if (ind >= 0) {     
         objects.profile.loadstrokes_vnebudzhet[ind].show = objects.value;
      }
    },
    SET_FACULTY_DEPS_TO_STATE: (state, objects) => {
      state.faculty_deps = objects
    },
    SET_ALL_FACULTY_DEPS_TO_STATE: (state, objects) => {
      state.all_faculty_deps = objects
    },
    SET_DEPARTMENT_IN_STATE: (_, objects) => {
      let ind = objects.profile.loadstrokes_budzhet.findIndex(e => e.uuid == objects.uuid); 
      if (ind >= 0) {     
         objects.profile.loadstrokes_budzhet[ind].department = objects.department;
      }
      ind = objects.profile.loadstrokes_vnebudzhet.findIndex(e => e.uuid == objects.uuid); 
      if (ind >= 0) {     
         objects.profile.loadstrokes_vnebudzhet[ind].department = objects.department;
      }
    },
    SET_SUBGROUPS_STROKE_INFO_TO_STATE: (state, objects) => {
      state.subgroups_stroke_info = objects
    },
    SET_SUBGROUPS_STROKE_TO_STATE: (state, objects) => {
      if (objects.res.sub != "") {
        if (Array.isArray(objects.res.sub)) {
          state.subgroups_stroke_info[objects.res.sub[0].parent_id].strokesubs = objects.res.sub
        }
        else {
          state.subgroups_stroke_info[objects.res.sub.parent_id].strokesubs.push(objects.res.sub)
        }
        
        let ind = objects.profile.loadstrokes_budzhet.findIndex(e => e.uuid == objects.uuid); 
        if (ind >= 0) {  
           let count = 0   
           for (let i=ind; i < objects.profile.loadstrokes_budzhet.length; i++) {
            if (objects.profile.loadstrokes_budzhet[i].uuid == objects.uuid) 
              count++;
            else break;
           }
           objects.profile.loadstrokes_budzhet.splice(ind, count, ...objects.res.strokes.budzhet)
        }
        ind = objects.profile.loadstrokes_vnebudzhet.findIndex(e => e.uuid == objects.uuid); 
        if (ind >= 0) {     
           let count = 0   
           for (let i=ind; i < objects.profile.loadstrokes_vnebudzhet.length; i++) {
            if (objects.profile.loadstrokes_vnebudzhet[i].uuid == objects.uuid) 
              count++;
            else break;
           }
           objects.profile.loadstrokes_vnebudzhet.splice(ind, count, ...objects.res.strokes.vnebudzhet)
        }
      }
    },
    JOIN_SUBGROUPS_STROKE_IN_STATE: (state, objects) => {
     if (objects.ind >= 0) 
      state.subgroups_stroke_info[objects.group_id].strokesubs.splice(objects.ind, 1);
     else
      state.subgroups_stroke_info[objects.group_id].strokesubs = [];
    },
    CHANGE_STROKES_IN_TABLE: (state, objects) => {
     let ind = objects.profile.loadstrokes_budzhet.findIndex(e => e.uuid == objects.uuid); 
     if (ind >= 0) {  
           let count = 0   
           for (let i=ind; i < objects.profile.loadstrokes_budzhet.length; i++) {
            if (objects.profile.loadstrokes_budzhet[i].uuid == objects.uuid) 
              count++;
            else break;
           }
           objects.profile.loadstrokes_budzhet.splice(ind, count, ...objects.strokes.budzhet)
     }
     ind = objects.profile.loadstrokes_vnebudzhet.findIndex(e => e.uuid == objects.uuid); 
     if (ind >= 0) {     
           let count = 0   
           for (let i=ind; i < objects.profile.loadstrokes_vnebudzhet.length; i++) {
            if (objects.profile.loadstrokes_vnebudzhet[i].uuid == objects.uuid) 
              count++;
            else break;
           }
           objects.profile.loadstrokes_vnebudzhet.splice(ind, count, ...objects.strokes.vnebudzhet)
     }
    },
    CHANGE_STROKES_PROFILE_IN_TABLE: (state, objects) => {
      
     let profile = null; 
     for (let facultyKey of Object.keys(state.ownTableLoad)) {
      let forms = state.ownTableLoad[facultyKey].forms; 
      for (let formKey of Object.keys(forms)) {
        let directions = forms[formKey].directions;
        for (let dir of directions) {
          for (let pKey of Object.keys(dir.profiles)) {
            let p = dir.profiles[pKey];
            if (p.id == objects.res.profile) {
              profile = p;
            }
          }
        }
      }
     }

     if (profile) {
       let ind = profile.loadstrokes_budzhet.findIndex(e => e.uuid == objects.uuid); 
       if (ind >= 0) {  
             let count = 0   
             for (let i=ind; i < profile.loadstrokes_budzhet.length; i++) {
              if (profile.loadstrokes_budzhet[i].uuid == objects.uuid) 
                count++;
              else break;
             }
             profile.loadstrokes_budzhet.splice(ind, count, ...objects.res.strokes.budzhet)
       }
       ind = profile.loadstrokes_vnebudzhet.findIndex(e => e.uuid == objects.uuid); 
       if (ind >= 0) {     
             let count = 0   
             for (let i=ind; i < profile.loadstrokes_vnebudzhet.length; i++) {
              if (profile.loadstrokes_vnebudzhet[i].uuid == objects.uuid) 
                count++;
              else break;
             }
             profile.loadstrokes_vnebudzhet.splice(ind, count, ...objects.res.strokes.vnebudzhet)
       }
     } 
    },

    SET_FLOWS_TO_STATE: (state, objects) => {
      state.flows = objects;
    },
    SET_STROKE_FLOWS_TO_STATE: (state, objects) => {
      state.stroke_flows = objects;
    },
    ADD_NEW_FLOW_TO_STATE: (state, object) => {
      state.flows.push(object)
    },
    DELETE_FLOW_IN_STATE: (state, objects) => {
      let ind = state.flows.findIndex(e => e.id == objects.flow_id); 
      if (ind >= 0) { 
        state.flows.splice(ind, 1);
      } 
    },
    SET_STROKE_OBJECT_TYPES_TO_STATE: (state, objects) => {
      state.stroke_object_types = objects
    },
    SET_COLUMNS_FOR_CHANGE_TO_STATE: (state, objects) => {
      state.columns_for_change = objects
    },
    ADD_STROKES_TO_TABLE: (state, objects) => {
     objects.profile.loadstrokes_budzhet.push(...objects.strokes.budzhet); 
     objects.profile.loadstrokes_vnebudzhet.push(...objects.strokes.vnebudzhet); 
    },
    DELETE_STROKES_FROM_TABLE: (state, objects) => {
     let ind = objects.profile.loadstrokes_budzhet.findIndex(e => e.uuid == objects.uuid); 
     if (ind >= 0) {  
           let count = 0   
           for (let i=ind; i < objects.profile.loadstrokes_budzhet.length; i++) {
            if (objects.profile.loadstrokes_budzhet[i].uuid == objects.uuid) 
              count++;
            else break;
           }
           objects.profile.loadstrokes_budzhet.splice(ind, count)
     }
     ind = objects.profile.loadstrokes_vnebudzhet.findIndex(e => e.uuid == objects.uuid); 
     if (ind >= 0) {     
           let count = 0   
           for (let i=ind; i < objects.profile.loadstrokes_vnebudzhet.length; i++) {
            if (objects.profile.loadstrokes_vnebudzhet[i].uuid == objects.uuid) 
              count++;
            else break;
           }
           objects.profile.loadstrokes_vnebudzhet.splice(ind, count)
     }
    }
 
  },
  actions: {
    async GET_TABLE_COLUMNS_FROM_API({dispatch, commit}, {version, faculty}) {
      dispatch('CLEAR_COLUMNS')
      return axios.get(resource_uri_get_columns,
                      {params: {version: version, faculty: faculty},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_COLUMNS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_COLUMNS({commit}) {
      commit('SET_COLUMNS_TO_STATE', [])
    },
    async GET_OWN_TABLE_LOAD_FROM_API({dispatch, commit}, {version, ownteachers}) {
      dispatch('CLEAR_OWN_TABLE_LOAD')
      return axios.get(resource_uri_get_load,
                      {params: {version: version, ownteachers: ownteachers, onlyotherstudents: 0},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_OWN_TABLE_LOAD_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_OWN_TABLE_LOAD({commit}) {
      commit('SET_OWN_TABLE_LOAD_TO_STATE', [])
    },
    async SET_STROKE_ACTIVITY({commit}, {uuid, version, value, profile}) {
      return axios.put(resource_uri_set_activity,
                        {},
                        {params: {uuid: uuid, value: value, version: version},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then(() => {
                    commit('SET_ACTIVITY_IN_STATE', {"uuid": uuid, "value": value,
                                                     "profile": profile})
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_FACULTY_DEPS_FROM_API({dispatch, commit}, {faculty, own}) {
      dispatch('CLEAR_FACULTY_DEPS')
      return axios.get(resource_uri_get_faculty_deps,
                      {params: {faculty: faculty, own: own},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_FACULTY_DEPS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_ALL_FACULTY_DEPS_FROM_API({commit}) {
      return axios.get(resource_uri_get_faculty_deps,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_ALL_FACULTY_DEPS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CLEAR_FACULTY_DEPS({commit}) {
      commit('SET_FACULTY_DEPS_TO_STATE', [])
    },
    async SET_STROKE_DEPARTMENT({commit}, {uuid, version, department, profile}) {
      return axios.put(resource_uri_set_department,
                        {},
                        {params: {uuid: uuid, department: department, version: version},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {
                    if (response.data.res == "done")
                    commit('SET_DEPARTMENT_IN_STATE', {"uuid": uuid, "department": department,
                                                       "profile": profile})
                    else 
                    commit('changeFlag', response.data.message)  
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_SUBGROUPS_STROKE_INFO_FROM_API({commit}, {uuid, version}) {
      return axios.get(resource_uri_get_subgroups_stroke_info,
                      {params: {uuid: uuid, version: version},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_SUBGROUPS_STROKE_INFO_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async SET_SUBGROUPS_STROKE_TO_API({commit}, {uuid, version, profile, block_id,
                                                 hours_type, departments}) {
      return axios.post(resource_uri_appoint_subgroups,  
              departments,   
              {
               params: {version: version, uuid: uuid,  
                        block_id: block_id, hours_type: hours_type}, 
               headers: {
                  'Access-Control-Allow-Origin': '*', 
                  'Content-Type': 'application/json', 
                  'Authorization': `Bearer ${localStorage.getItem('token')}`
              }}
            )
             .then((response) => {
                commit('SET_SUBGROUPS_STROKE_TO_STATE', {"res": response.data, "profile": profile,
                                                         "uuid": uuid})
              })
             .catch(errorHandler.errorHandler)
    },
    async JOIN_SUBGROUPS_STROKE_IN_API({commit}, {uuid, version, group_id, hours_type,
                                                  profile, ind}) {
      return axios.post(resource_uri_join_subgroups, 
                        {},
                        {
                         params: {version: version, uuid: uuid,  
                                  hours_type: hours_type, gp_id: group_id}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    commit('JOIN_SUBGROUPS_STROKE_IN_STATE', {"group_id": group_id, 
                                                              "ind": ind})
                    commit('CHANGE_STROKES_IN_TABLE', {"strokes": response.data, "profile": profile,
                                                       "uuid": uuid})
                  })
                  .catch(errorHandler.errorHandler)
    },
    async SET_STROKE_SUBGROUP_DEPARTMENT({commit}, {uuid, version, department, profile, 
                                                    subgroup_id}) {
      return axios.put(resource_uri_set_subgroup_dep,
                        {},
                        {params: {version: version, uuid: uuid, 
                                  subgroup_id: subgroup_id, newdepartment: department},
                         headers: {
                           'Access-Control-Allow-Origin': '*', 
                           'Content-Type': 'application/json', 
                           'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                      )
                  .then((response) => {

                    console.log(response.data)

                    if (response.data.strokes != "") {                      
                     commit('CHANGE_STROKES_IN_TABLE', {"uuid": uuid, "strokes": response.data.strokes,
                                                        "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_FLOWS_FROM_API({commit}, {version, own}) {
      return axios.get(resource_uri_get_flows,
                      {params: {version: version, own: own},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_FLOWS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_STROKE_FLOWS_FROM_API({commit}, {version, uuid}) {
      return axios.get(resource_uri_get_stroke_flows,
                      {params: {version: version, uuid: uuid},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_STROKE_FLOWS_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_NEW_FLOW_TO_API({commit}, {version,  dep, hoursType, name}) {
      return axios.post(resource_uri_add_flow, 
                        {},
                        {
                         params: {version: version,  
                                  hours_type: hoursType, dep: dep, name: name}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    commit('ADD_NEW_FLOW_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_NEW_STROKE_FLOW_TO_API({commit}, {uuid, group_id,
                                                group_type, flow_id, profile}) {
      return axios.post(resource_uri_add_flow_stroke, 
                        {},
                        {
                         params: {uuid: uuid, group_id: group_id,
                                  group_type: group_type, 
                                  flow_id: flow_id}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    if (response.data.strokes != "") {                      
                     commit('CHANGE_STROKES_IN_TABLE', {"uuid": uuid, "strokes": response.data.strokes,
                                                        "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_STROKE_FLOW_FROM_API({commit}, {uuid, group_id,
                                                group_type, flow_id, profile}) {
      return axios.delete(resource_uri_add_flow_stroke, 
                        {
                         params: {uuid: uuid, group_id: group_id,
                                  group_type: group_type, 
                                  flow_id: flow_id}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    if (response.data.strokes != "") {                      
                     commit('CHANGE_STROKES_IN_TABLE', {"uuid": uuid, "strokes": response.data.strokes,
                                                        "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CHANGE_MAIN_STROKE_FLOW_IN_API({commit}, {uuid, group_id,
                                                    group_type, flow_id}) {
      return axios.put(resource_uri_add_flow_stroke,
                        {}, 
                        {
                         params: {uuid: uuid, group_id: group_id,
                                  group_type: group_type, 
                                  flow_id: flow_id}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => { 
                     console.log(response.data)                    
                     commit('CHANGE_STROKES_PROFILE_IN_TABLE', {"uuid": response.data.old_uuid, "res": response.data.old}) 
                     commit('CHANGE_STROKES_PROFILE_IN_TABLE', {"uuid": uuid, "res": response.data.new}) 
                  })
                  .catch(errorHandler.errorHandler)
   },
   async DELETE_FLOW_FROM_API({commit}, {flow_id}) {
      return axios.delete(resource_uri_add_flow, 
                        {
                         params: {flow_id: flow_id}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    console.log(response.data)
                    for (let stroke of response.data) {
                     commit('CHANGE_STROKES_PROFILE_IN_TABLE', {"uuid": stroke.uuid, "res": {"strokes": stroke.strokes, "profile": stroke.profile_id}})
                    }
                    commit('DELETE_FLOW_IN_STATE', {"flow_id": flow_id})
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_STROKE_OBJECT_TYPES_FROM_API({commit}) {
      return axios.get(resource_uri_get_stroke_object_types,
                      {params: {},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_STROKE_OBJECT_TYPES_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async ADD_NEW_OWN_STROKE_TO_API({commit}, {version, profile, course, sem, type_id,
                                               add_inf, department}) {
      return axios.post(resource_uri_add_own_stroke, 
                        {},
                        {
                         params: {version: version, profile_id: profile.id,
                                  course: course, sem: sem, type_id: type_id,
                                  add_inf: add_inf, department: department}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    if (response.data.strokes != "") {                      
                     commit('ADD_STROKES_TO_TABLE', {"strokes": response.data.strokes,
                                                     "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    },
    async DELETE_STROKE_OWN_FROM_API({commit}, {uuid, version, profile}) {
      return axios.delete(resource_uri_add_own_stroke, 
                        {
                         params: {uuid: uuid, version: version}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    if (response.data == "ok") {                      
                     commit('DELETE_STROKES_FROM_TABLE', {"uuid": uuid,
                                                          "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    },
    async GET_COLUMNS_FOR_CHANGE_FROM_API({commit}, {faculty, version}) {
      return axios.get(resource_uri_get_columns_for_change,
                      {params: {faculty: faculty, version: version},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_COLUMNS_FOR_CHANGE_TO_STATE', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    },
    async CHANGE_COLUMN_VALUE_IN_API({commit}, {version, profile, uuid,
                                                budzhet, column_id, value, dep}) {
      return axios.post(resource_uri_set_column_value, 
                        {},
                        {
                         params: {version: version, uuid: uuid,
                                  budzhet: budzhet, column_id: column_id, value: value,
                                  dep: dep}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    if (response.data.strokes != "") {                      
                     commit('CHANGE_STROKES_IN_TABLE', {"uuid": uuid, "strokes": response.data.strokes,
                                                        "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    },
    async RETURN_COLUMN_VALUE_IN_API({commit}, {version, profile, uuid,
                                                budzhet, column_id, dep}) {
      return axios.delete(resource_uri_set_column_value, 
                        {
                         params: {version: version, uuid: uuid,
                                  budzhet: budzhet, column_id: column_id,
                                  dep: dep}, 
                         headers: {
                            'Access-Control-Allow-Origin': '*', 
                            'Content-Type': 'application/json', 
                            'Authorization': `Bearer ${localStorage.getItem('token')}`
                        }}
                        )
                  .then((response) => {
                    if (response.data.strokes != "") {                      
                     commit('CHANGE_STROKES_IN_TABLE', {"uuid": uuid, "strokes": response.data.strokes,
                                                        "profile": profile}) 
                    }
                  })
                  .catch(errorHandler.errorHandler)
    }
  },
  getters: {
    TABLE_COLUMNS(state) {
      return state.columns
    },
    OWN_TABLE_LOAD(state) {
      return state.ownTableLoad
    },
    FACULTY_DEPS(state) {
      return state.faculty_deps
    },
    ALL_FACULTY_DEPS(state) {
      return state.all_faculty_deps
    },
    SUBGROUPS_STROKE_INFO(state) {
      return state.subgroups_stroke_info
    },
    TABLE_FLOWS(state) {
      return state.flows
    },
    STROKE_FLOWS(state) {
      return state.stroke_flows
    },
    STROKE_OBJECT_TYPES(state) {
      return state.stroke_object_types
    },
    COLUMNS_FOR_CHANGE(state) {
      return state.columns_for_change
    }
  }  
}    
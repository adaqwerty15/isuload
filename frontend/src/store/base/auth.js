import axios from 'axios'
// import errorHandler from '../_utils'

const resource_uri_auth_login = 'users/login'
const resource_uri_auth_session = 'users/current'
const resource_uri_auth_refresh = 'users/refresh_token'


export default {
  state: () => ({
    session: {
      user: null
    }
  }),
  mutations: {
    CLEAR_SESSION_DATA(state) {
      state.session = {
        user: null
      }
      localStorage.removeItem('token')
      localStorage.removeItem('refresh')
    },
    SET_SESSION_DATA(state, {field, data}) {
      state.session[field] = data
    }
  },
  actions: {
    async SIGN_IN ({ commit }, {login, password}) {
      commit('CLEAR_SESSION_DATA')
      return axios.post(resource_uri_auth_login, {login: login,
                                                  password: password})
        .then((response) => {
          if (response.data.access_token) {
            commit('SET_SESSION_DATA', {
              field: 'user', 
              data: response.data.user
            })                
            localStorage.setItem('token', response.data.access_token)
            localStorage.setItem('refresh', response.data.refresh_token)
        
            return 'ok'
          }
        })
        // .catch(errorHandler.errorHandler)
    },
    async GET_SESSION_DATA ({ commit }) {
      try {
     
        let response = await axios.get(resource_uri_auth_session,
            {
            headers: {
              'Access-Control-Allow-Origin': '*', 
              'Content-Type': 'application/json', 
              'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
           })

           console.log(response.data)

           commit('SET_SESSION_DATA', {
            field: 'user', 
            data: response.data
           })
        
      } catch(e) {
        if (localStorage.getItem('token')) {
          return axios.post(resource_uri_auth_refresh,
            {
              params: {
              }
            }, {
            headers: {
              'Access-Control-Allow-Origin': '*',
              'Authorization': `Bearer ${localStorage.getItem('refresh')}`
            }
           })
           .then((response) => {

            commit('SET_SESSION_DATA', {
             field: 'user', 
             data: response.data.user
            })
            localStorage.setItem('token', response.data.access_token) 

            })
           .catch(() => {
              commit('CLEAR_SESSION_DATA')
              return 'wrong'
           })
        }
        commit('CLEAR_SESSION_DATA')
        return 'wrong'
      }
    },
    async LOGOUT ({commit}) {
      commit('CLEAR_SESSION_DATA')
      return "ok"  
    }
  },
  getters: {
    SESSION(state) {
      return state.session
    },
    IS_AUTHENTICATED(state) {
      return Boolean(state.session && state.session.user)
    },
    LOGIN_URL() {
      return "/signin"
    }
  }

}
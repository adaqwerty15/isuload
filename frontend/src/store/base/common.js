import errorHandler from '../_utils'
import axios from 'axios'
const resource_uri_edit_user_year = 'users/year'

export default {
  state: () => ({
    year: null
  }),
  mutations: {
    SET_YEAR(state, year) {
      state.year = year
    },
  },
  actions: {
    async SET_YEAR ({commit}, {year: year}) {
      commit('SET_YEAR', year) 
    },
    async CHANGE_USER_YEAR({commit}, {year: year}) {
      return axios.put(resource_uri_edit_user_year,
                      {},
                      {params: {year: year},
                      headers: {
                        'Access-Control-Allow-Origin': '*', 
                        'Content-Type': 'application/json', 
                        'Authorization': `Bearer ${localStorage.getItem('token')}`
                      }})
                  .then((response) => {
                     commit('SET_YEAR', response.data)
                  })
                  .catch(errorHandler.errorHandler)
    }
  },
  getters: {
    YEAR(state) {
      return state.year
    },
  }

}
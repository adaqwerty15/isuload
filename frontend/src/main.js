import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

// import * as Vue from 'vue' // in Vue 3
import axios from 'axios'
import VueAxios from 'vue-axios'

import PrimeVue from 'primevue/config'
import Tooltip from 'primevue/tooltip';
import ToastService from 'primevue/toastservice';
import ConfirmationService from 'primevue/confirmationservice';

import "primevue/resources/themes/mdc-light-indigo/theme.css"       //theme
import "primevue/resources/primevue.min.css"                 //core css
import "primeicons/primeicons.css"   
import 'primeflex/primeflex.css';


// axios.defaults.baseURL = 'http://127.0.0.1:8101'
// axios.defaults.baseURL = 'http://185.46.10.143:8101/'
axios.defaults.baseURL = 'http://127.0.0.1:8000/'
axios.defaults.withCredentials = true;
axios.defaults.headers.post['Content-Type'] = 'application/json';


store.dispatch('GET_SESSION_DATA').then(() => {
createApp(App)
.use(store)
.use(router)
.use(VueAxios, axios)
.use(PrimeVue, {ripple: true})
.use(ToastService)
.use(ConfirmationService)
.directive('tooltip', Tooltip)
.mount('#app')
})

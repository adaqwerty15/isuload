from typing import List, Optional
from datetime import date, datetime
from pydantic import BaseModel
from fastapi import UploadFile, File

import app.schemas.dirs as asd


class LessonPlan(BaseModel):
    id: int
    course: int
    year: int
    profile: asd.Profile

    class Config:
        orm_mode = True


class LessonPlanView(BaseModel):
	id: int
	course: int
	qual: str
	form: str
	shifr: str
	dir: str
	profile: str
	bc: Optional[int] = None
	vc: Optional[int] = None
	bg: Optional[int] = None
	vg: Optional[int] = None


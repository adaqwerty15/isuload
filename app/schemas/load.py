from typing import List, Optional
from datetime import date, datetime
from pydantic import BaseModel


class SubgroupDepartments(BaseModel):
    sid: int
    department: int
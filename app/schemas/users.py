from typing import List, Optional
from datetime import date, datetime
from pydantic import BaseModel


class Role(BaseModel):
    id: int
    role: str
    user_id: int
    right: Optional[str] = None
    inf: Optional[str] = None
    faculty_id: int

    class Config:
        orm_mode = True


class UserBase(BaseModel):
    login: str
    surname: str
    name: str
    patr: Optional[str] = None


class User(UserBase):
    id: int
    edited_year: int
    roles: List[Role] = []

    class Config:
        orm_mode = True


class UserInDB(User):
    hashed_pass: str
    edited_year: int


class UserCreate(UserBase):
    password: str        


class Token(BaseModel):
    access_token: str
    refresh_token: str
    user: User


class TokenData(BaseModel):
    login: Optional[str] = None


class NewTokenData(BaseModel):
    access_token: str
    user: User


class Crendetails(BaseModel):
    login: str
    password: str             
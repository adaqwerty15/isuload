from typing import List, Optional
from datetime import date, datetime
from pydantic import BaseModel

class Constr(BaseModel):
    name: str
    value: int


class FormulData(BaseModel):
	formula: str
	formula_text: str
	constrs: List[Constr]

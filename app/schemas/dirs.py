from typing import List, Optional
from datetime import date, datetime
from pydantic import BaseModel


class Faculty(BaseModel):
    id: int
    name: str
    active: bool
    uid_1c: Optional[str] = None

    class Config:
        orm_mode = True


class ConstrDirectory(BaseModel):
     id: int
     name: str
     value: int
     active: bool
     type: str
     own: bool
     add_inf_type: Optional[str] = None

     class Config:
        orm_mode = True


class Direction(BaseModel):
    id: int
    shifr: str
    name: str
    qual_id: int
    qual: ConstrDirectory
    form_id: int
    form: ConstrDirectory
    faculty: Faculty

    class Config:
        orm_mode = True


class Profile(BaseModel):
    id: int 
    direction: Direction
    name: str

    class Config:
        orm_mode = True       
from typing import List, Optional
from datetime import date, datetime
from pydantic import BaseModel


class ConstrDirectory(BaseModel):
     id: int
     name: str
     value: int
     active: bool
     type: str
     own: bool
     add_inf_type: Optional[str] = None

     class Config:
        orm_mode = True


class Direction(BaseModel):
    id: int
    shifr: str
    name: str
    qual_id: int
    qual: ConstrDirectory
    form_id: int
    form: ConstrDirectory

    class Config:
        orm_mode = True


class GroupProfile(BaseModel):
    id: int
    name: str
    budzhet_count: int    
    vnebudzhet_count: int    
    budzhet_foreign: int
    vnebudzhet_foreign: int
    budzhet: bool
    profile_id: int
    course: int
    year: int

    class Config:
        orm_mode = True


class SubgroupInf(BaseModel):
    name: Optional[str] = None
    bc: int
    vc: int
    bf: int
    vf: int
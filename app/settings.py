import os

# DATABASE_URL ="postgresql://adaqwerty15:1@localhost:5432/newplatformisu"
DATABASE_URL =  os.getenv('ISULOAD_DATABASE_URL', "postgresql://adaqwerty15:1@localhost:5432/newplatformisu")

if not DATABASE_URL:
	DATABASE_URL = "postgres://postgres:1@db:8083/newplatformisu"

#CHANGE IN PRODUCTION
SECRET_KEY = "815005d1cf96091a7b352476172a57c526aa63a11f0461d795d2f09a2c81de79"

ALGORITHM = "HS256"

ACCESS_TOKEN_EXPIRE_MINUTES = 60*24*7
REFRESH_TOKEN_EXPIRE_MINUTES = 60*24*7


ORIGINS = ["http://localhost:8080", "http://185.46.10.143:8080", "http://isuload.ru"]


GET_1C_FACULTUES_URL = "http://py.isu.ru:8000/hs/rest_http1c/facultets/?format=json"
GET_1C_DIRECTIONS_URL = "http://py.isu.ru:8000/hs/rest_http1c/directions_all/?format=json"
POST_1C_TEACHERS_URL = "http://py.isu.ru:8000/hs/jsonpost/pps_in_faculty/"
import app.settings as settings
import app.schemas.users as asu
import app.control.users as acu

import app.control.contingent as acc
import app.schemas.contingent as asc

from fastapi import APIRouter, Depends, HTTPException, status, Body, File, UploadFile
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/contingent",
    tags=["contingent"],
    # dependencies=[Depends(get_db)],
    # responses={404: {"description": "Not found"}},
)


@router.get("/profilestree")
async def get_profiles_tree(faculty: int, 
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.get_profiles_tree(db=db, faculty=faculty)


@router.get("/groups")
async def get_all_groups(profile_id: int, 
                         year: int, 
                         db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.get_all_groups(db=db, profile_id=profile_id, year=year)


@router.post("/groups")
async def add_profile_group(profile_id: int, 
                            year: int, 
                            name: str, 
                            bc: int, vc: int,
                            bf: int, vf: int, 
                            course: int, 
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):
    return acc.add_profile_group(db=db, profile_id=profile_id, year=year, name=name,
                                 bc=bc, vc=vc, bf=bf, vf=vf, course=course)


@router.delete("/groups")
async def delete_profile_group(group_id: int, 
                               db: Session = Depends(get_db),
                               current_user: asu.User = Depends(acu.get_current_user)):
    return acc.delete_profile_group(db=db, group_id=group_id)


@router.put("/groups/name")
async def edit_profile_group_name(group_id: int, 
                                  new_name: str, 
                                  db: Session = Depends(get_db),
                                  current_user: asu.User = Depends(acu.get_current_user)):
    return acc.edit_profile_group_name(group_id=group_id, new_name=new_name, db=db)          


@router.put("/subgroups/name")
async def edit_subgroup_name(subgroup_id: int, 
                             new_name: str, 
                             current_user: asu.User = Depends(acu.get_current_user),
                             db: Session = Depends(get_db)):
    return acc.edit_subgroup_name(subgroup_id=subgroup_id, new_name=new_name, db=db)


@router.put("/groups")
async def edit_profile_group_contingent(group_id: int, 
                                        bc: int, vc: int,
                                        bf: int, vf: int,
                                        current_user: asu.User = Depends(acu.get_current_user),
                                        db: Session = Depends(get_db)):
    return acc.edit_profile_group_contingent(group_id=group_id, bc=bc, vc=vc, bf=bf, vf=vf, db=db)


@router.put("/subgroups")
async def edit_subgroup_contingent(subgroup_id: int, 
                                   bc: int, vc: int,
                                   bf: int, vf: int,
                                   current_user: asu.User = Depends(acu.get_current_user),
                                   db: Session = Depends(get_db)):
    return acc.edit_subroup_contingent(subgroup_id=subgroup_id, bc=bc, vc=vc, bf=bf, vf=vf, db=db)


@router.put("/subgroups/deactivation")
async def disable_subgroup_block(block_id: int,
                                 current_user: asu.User = Depends(acu.get_current_user),
                                 db: Session = Depends(get_db)):
    return acc.disable_subgroup_block(block_id=block_id, db=db)


@router.post("/subgroups")
async def add_profile_group(subgroups: List[asc.SubgroupInf],
                            group_id: int, 
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):
    return acc.add_subgroups_block(db=db, group_id=group_id, subgroups=subgroups)


@router.post("/groupschoice")
async def add_choice_group(uuid: str,
                           block: str, 
                           db: Session = Depends(get_db),
                           current_user: asu.User = Depends(acu.get_current_user)):
    return acc.add_choice_group(db=db, uuid=uuid, block=block)


@router.delete("/groupschoice")
async def delete_choice_group(group_id: int,
                              db: Session = Depends(get_db),
                              current_user: asu.User = Depends(acu.get_current_user)):
    return acc.delete_choice_group(db=db, group_id=group_id) 


@router.put("/groupschoice")
async def edit_groupchoice_contingent(group_id: int, 
                                      bc: int, vc: int,
                                      bf: int, vf: int,
                                      current_user: asu.User = Depends(acu.get_current_user),
                                      db: Session = Depends(get_db)):
    return acc.edit_groupchoice_contingent(group_id=group_id, bc=bc, vc=vc, bf=bf, vf=vf, db=db)


@router.post("/generateauto")
async def generate_groups_auto(year: int, faculty: int, db: Session = Depends(get_db)):
    return acc.generate_groups_auto(db, year, faculty)                     
import app.settings as settings
import app.schemas.users as asu
import app.control.users as acu

import app.control.columns as acc
import app.schemas.columns as asc

from fastapi import APIRouter, Depends, HTTPException, status, Body, File, UploadFile
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/columns",
    tags=["columns"],
)

@router.get("/")
async def get_columns(faculty: int, 
                      year: int,
                      db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.get_columns(db=db, faculty=faculty, year=year)


@router.put("/")
async def edit_column(column_id: int, 
                      name: str, alias: str,
                      db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.edit_column(db=db, column_id=column_id, name=name, alias=alias)


@router.put("/visibility")
async def edit_column_visibility(column_id: int, 
                                 db: Session = Depends(get_db),
                                 current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.edit_column_visibility(db=db, column_id=column_id)


@router.put("/existence")
async def edit_column_existence(column_id: int, 
                                db: Session = Depends(get_db),
                                current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.edit_column_existence(db=db, column_id=column_id)


@router.post("/")
async def add_column(faculty: int, year: int,
                     name: str, alias: str,
                     parent_id: Optional[int] = None,
                     db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.add_column(db=db, faculty=faculty, year=year,
                          name=name, alias=alias, parent_id=parent_id)


@router.get("/formuls")
async def get_formuls(column_id: int,
                      db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.get_formuls(db=db, column_id=column_id)                                  


@router.get("/variables")
async def get_formuls(db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):
    return acc.get_formuls_data(db=db)


@router.get("/constrs")
async def get_constrs(db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):
    return acc.get_constrs(db=db)


@router.post("/formuls")
async def add_formula(column_id: int,
                      data: asc.FormulData,
                      db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.add_formula(db=db, data=data, column_id=column_id)


@router.put("/formuls")
async def edit_formula(column_id: int,
                       formul_id: int,
                       data: asc.FormulData,
                       db: Session = Depends(get_db),
                       current_user: asu.User = Depends(acu.get_current_user)):              
    return acc.edit_formula(db=db, data=data, column_id=column_id, formul_id=formul_id)


@router.delete("/formuls")
async def delete_formula(column_id: int, formul_id: int,
                         db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):
    return acc.delete_formula(db=db, column_id=column_id, formul_id=formul_id)


@router.post("/copy")
async def copy_columns_from_previous_year(faculty: int, year: int,
                                           db: Session = Depends(get_db)):
    return acc.copy_columns_from_previous_year(db, faculty, year)
import app.settings as settings
import app.schemas.users as asu
import app.control.users as acu

import app.control.teachers as act
import app.schemas.teachers as ast

from fastapi import APIRouter, Depends, HTTPException, status, Body, File, UploadFile
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/teachers",
    tags=["teachers"],
)


@router.get("/version")
async def get_approved_version(faculty: int, year: int,
                               db: Session = Depends(get_db)):
    return act.get_approved_version(db, faculty, year)


@router.get("/stat")
async def get_teachers_load_stat(faculty: int, 
                                 year: int,
                                 db: Session = Depends(get_db)):
                                 # current_user: asu.User = Depends(acu.get_current_user)):              
    return act.teachers_load_stat(db=db, faculty=faculty, year=year)


@router.get("/load")
async def get_teacher_load_for_dep(department: int, 
                                   year: int,
                                   db: Session = Depends(get_db)):
    return act.get_teacher_load_for_dep(db=db, dep=department, year=year)


@router.get("/dep")
async def get_teachers_of_department(department: int, 
                                     year: int,
                                     db: Session = Depends(get_db)):
    return act.get_dep_teachers(db, department, year)


@router.put("/allstroke") 
async def set_teacher_on_all_stroke(uuid: str, group_type:str, group_id: int,
                                    teacher_id: int, load_version: int, department: int,
                                    key: int,
                                    db: Session = Depends(get_db)):
    return act.set_teacher_on_all_stroke(db, uuid, group_type, group_id, teacher_id, 
                                         load_version, department, key)


@router.put("/loadpart") 
async def set_teacher_on_load_part_stroke(uuid: str, group_type:str, group_id: int,
                                          teacher_id: int, load_version: int, department: int,
                                          key: int, hours_type: str,
                                          db: Session = Depends(get_db)):
    return act.set_teacher_on_load_part_stroke(db, uuid, group_type, group_id, teacher_id, 
                                               load_version, department, key, hours_type)


@router.put("/calcpart") 
async def set_teacher_on_calc_part_stroke(uuid: str, group_type:str, group_id: int,
                                          teacher_id: int, load_version: int, department: int,
                                          key: int, calc_id: int, hours_type: Optional[str] = None,
                                          db: Session = Depends(get_db)):
    return act.set_teacher_on_calc_part_stroke(db, uuid, group_type, group_id, teacher_id, 
                                               load_version, department, key, calc_id, hours_type)


@router.put("/practpart")
async def set_teacher_on_part_of_practice(uuid: str, group_type:str, group_id: int,
                                          teacher_id: int, load_version: int, department: int,
                                          key: int, bc: int, vc: int,
                                          db: Session = Depends(get_db)):
    return act.set_teacher_on_part_of_practice(db, uuid, group_type, group_id, teacher_id, 
                                               load_version, department, key, bc, vc)

@router.delete("/practpart")
async def delete_teacher_on_part_of_practice(uuid: str, group_type:str, group_id: int,
                                             teacher_id: int, load_version: int, department: int,
                                             key: int, 
                                             db: Session = Depends(get_db)):
    return act.delete_teacher_on_part_of_practice(db, uuid, group_type, group_id, teacher_id, 
                                                  load_version, department, key)


@router.get("/report")
async def get_teacher_report(teacher_id: int, department: int, 
                              year: int, db: Session = Depends(get_db)):
    return act.get_teacher_report(db, teacher_id, department, year)                                                                                             


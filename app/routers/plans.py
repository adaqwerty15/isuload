import app.settings as settings
import app.schemas.users as asu
import app.control.users as acu

import app.control.plans as acp
import app.schemas.plans as asp

from fastapi import APIRouter, Depends, HTTPException, status, Body, File, UploadFile
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/plans",
    tags=["plans"],
    # dependencies=[Depends(get_db)],
    # responses={404: {"description": "Not found"}},
)

@router.put("/contingent")
async def add_plan_contingent(plan_id: int, bc: int, vc: int,
                              bg: int, vg: int,
                              db: Session = Depends(get_db)):
    return acp.add_plan_contingent(db, plan_id, bc, vc, bg, vg)


@router.get("/", response_model=List[asp.LessonPlanView])
async def read_plans_by_year(year: int, faculty: int, db: Session = Depends(get_db),
                             current_user: asu.User = Depends(acu.get_current_user)):              
    return acp.get_plans_by_year(db=db, year=year, faculty=faculty)


@router.delete("/")
async def delete_lessonplan(plan_id: int, db: Session = Depends(get_db)):
    return acp.delete_lessonplan(db=db, plan_id=plan_id)    


@router.post("/uploadfile")
async def create_upload_files(year: int = Body(...), faculty: int = Body(...), files: List[UploadFile] = File(...), 
                              db: Session = Depends(get_db),
	                          current_user: asu.User = Depends(acu.get_current_user)):
    results = []
    for file in files:
        results.append(acp.add_plan(db, file, year, faculty))
    return results  
    # return {"filenames": files.filename}   
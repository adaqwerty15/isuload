import app.models.isuload as ami
import app.schemas.users as asu
import app.control.users as acu
import app.settings as settings

import app.control.dirs as acd
import app.schemas.dirs as asd

from fastapi import APIRouter, Depends, HTTPException, status, Body
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/dirs",
    tags=["dirs"],
    # dependencies=[Depends(get_db)],
    # responses={404: {"description": "Not found"}},
)


#FACULTIES

@router.get("/faculties")
async def read_faculties(ids: Optional[str] = None, year: Optional[int] = None, db: Session = Depends(get_db)):
                         # current_user: asu.User = Depends(acu.get_current_user)):              
    return acd.get_faculties(db=db, ids=ids, year=year) 


@router.get("/faculties/{id}")
async def read_faculty(id: int, year: Optional[int] = None,  db: Session = Depends(get_db)): 
                       # current_user: asu.User = Depends(acu.get_current_user)):
    return acd.get_faculty_by_id(db=db, f_id=id, year=year)


@router.post("/faculties/compare1c")
async def compare_faculties_with_1c(db: Session = Depends(get_db)):
    return acd.compare_faculties_with_1c(db=db)    


@router.put("/faculties")
async def edit_faculty(faculty_id: int, name: str, group_template: Optional[str] = None,
                       db: Session = Depends(get_db)):
    return acd.edit_faculty(db, faculty_id, name, group_template)


@router.post("/faculties")
async def add_faculty(name: str, group_template: Optional[str] = None,
                      db: Session = Depends(get_db)):
    return acd.add_faculty(db, name, group_template)


@router.delete("/faculties")
async def delete_faculty(faculty_id: int,
                       db: Session = Depends(get_db)):
    return acd.delete_faculty(db, faculty_id)    


@router.get("/directions", response_model=List[asd.Direction])
async def read_directions(ids: Optional[str] = None, db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):              
    return acd.get_directions(db=db, ids=ids)  


@router.get("/profiles", response_model=List[asd.Profile])
async def read_profiles(ids: Optional[str] = None, db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):              
    return acd.get_profiles(db=db, ids=ids)


@router.get("/profiles/tree")
async def read_profiles_tree(db: Session = Depends(get_db)):
    return acd.get_profiles_tree(db=db)    


@router.get("/departments")
async def read_departments(faculty: Optional[int] = None, own: Optional[bool] = None,
                           db: Session = Depends(get_db),
                           current_user: asu.User = Depends(acu.get_current_user)):              
    return acd.get_departments(db=db, faculty=faculty, own=own)


@router.get("/ownobjecttypes")
async def get_own_object_types(db: Session = Depends(get_db),
                               current_user: asu.User = Depends(acu.get_current_user)):
    return acd.get_own_object_types(db=db)


@router.get("/columnsforchange")
async def get_columns_for_change(faculty: int, version: int, db: Session = Depends(get_db),
                                 current_user: asu.User = Depends(acu.get_current_user)):
    return acd.get_columns_for_change(db, faculty, version)


@router.get("/qualifications")
async def get_qualifications(db: Session = Depends(get_db)):
    return acd.get_qualifications(db)


@router.post("/directions/compare1c")
async def compare_directions_with_1c(db: Session = Depends(get_db)):
    return acd.compare_directions_with_1c(db=db)


@router.post("/directions")
async def add_direction(name: str, shifr: str,  faculty_id: int, qual_id: int,
                        group_template: str, db: Session = Depends(get_db)):
    return acd.add_direction(db, name, shifr, faculty_id, qual_id,  group_template)


@router.put("/directions")
async def edit_direction(name: str, shifr: str, faculty_id: int, qual_id: int,
                        group_template: str, d_id: int, db: Session = Depends(get_db)):
    return acd.edit_direction(db, name, shifr, faculty_id, qual_id,  group_template, d_id)


@router.delete("/directions")
async def delete_direction(faculty_id: int, d_id: int, db: Session = Depends(get_db)):
    return acd.delete_direction(db, faculty_id, d_id) 


@router.post("/profiles")
async def add_profile(d_id: int, name: str, faculty_id: int, db: Session = Depends(get_db)):
    return acd.add_profile(db, d_id, name, faculty_id)


@router.put("/profiles")
async def edit_profile(d_id: int, name: str, faculty_id: int, p_id: int,
                       db: Session = Depends(get_db)):
    return acd.edit_profile(db, d_id, name, faculty_id, p_id)


@router.delete("/profiles")
async def delete_profile(d_id: int, faculty_id: int, p_id: int, db: Session = Depends(get_db)):
    return acd.delete_profile(db, d_id, faculty_id, p_id)                                   


@router.get("/teachers")
async def get_teachers(faculty: int, db: Session = Depends(get_db)):
    return acd.get_teachers(db=db, faculty=faculty)


@router.post("/teachers/compare1c")
async def compare_teachers_with_1c(faculty: int, db: Session = Depends(get_db)):
    return acd.compare_teachers_with_1c(db=db, faculty=faculty)


@router.post("/teachers")
async def add_teacher(name: str, surname: str, fathername: str, position: str, 
                      degree: str, academic_rank: str, rate: float, state: bool, dep: int, faculty: int,
                      db: Session = Depends(get_db)):
    return acd.add_teacher(db, name, surname, fathername, position, degree, academic_rank,
                           rate, state, dep, faculty)


@router.put("/teachers")
async def edit_teacher(name: str, surname: str, fathername: str, position: str, 
                      degree: str, academic_rank: str, rate: float, state: bool, dep: int, 
                      t_id: int, faculty: int,
                      db: Session = Depends(get_db)):
    return acd.edit_teacher(db, name, surname, fathername, position, degree, academic_rank,
                           rate, state, dep, t_id, faculty) 


@router.delete("/teachers")
async def delete_teacher(t_id: int, faculty: int,
                      db: Session = Depends(get_db)):
    return acd.delete_teacher(db, t_id, faculty)


@router.get("/deps")
async def get_departments(db: Session = Depends(get_db)):              
    return acd.get_all_departments(db=db)


@router.post("/deps")
async def add_department(name: str, number: int, faculty_id: int, 
                         db: Session = Depends(get_db)):              
    return acd.add_department(db, name, number, faculty_id) 


@router.put("/deps")
async def edit_department(name: str, number: int, faculty_id: int, dep_id: int,
                          db: Session = Depends(get_db)):              
    return acd.edit_department(db, name, number, faculty_id, dep_id)


@router.delete("/deps")
async def delete_department(dep_id: int,
                            db: Session = Depends(get_db)):              
    return acd.delete_department(db, dep_id)                                                                                               


@router.get("/objecttypes")
async def get_object_types(db: Session = Depends(get_db)):              
    return acd.get_object_types(db=db)


@router.post("/objecttypes")
async def add_object_type(name: str, add_inf: str, db: Session = Depends(get_db)):              
    return acd.add_object_type(db, name, add_inf) 


@router.put("/objecttypes")
async def edit_object_type(name: str, add_inf: str, ot_id: int, db: Session = Depends(get_db)):              
    return acd.edit_object_type(db, name, add_inf, ot_id) 


@router.delete("/objecttypes")
async def delete_object_type(ot_id: int, db: Session = Depends(get_db)):              
    return acd.delete_object_type(db, ot_id)


@router.get("/typesofwork")
async def get_types_of_work(db: Session = Depends(get_db)):              
    return acd.get_types_of_work(db=db)             


@router.post("/typesofwork")
async def add_type_of_work(code: int, name: str, active_calc: bool, type_w: str,
                           db: Session = Depends(get_db)):              
    return acd.add_type_of_work(db, code, name, active_calc, type_w)    


@router.put("/typesofwork")
async def edit_type_of_work(code: int, name: str, active_calc: bool, type_w: str, tw_id: int,
                           db: Session = Depends(get_db)):              
    return acd.edit_type_of_work(db, code, name, active_calc, type_w, tw_id)


@router.delete("/typesofwork")
async def delete_type_of_work(tw_id: int,
                              db: Session = Depends(get_db)):              
    return acd.delete_type_of_work(db, tw_id)        
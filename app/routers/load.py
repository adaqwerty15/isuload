import app.settings as settings
import app.schemas.users as asu
import app.control.users as acu

import app.control.load as acl
import app.schemas.load as asl

from fastapi import APIRouter, Depends, HTTPException, status, Body, File, UploadFile
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/load",
    tags=["load"],
)


@router.get("/versions")
async def get_load_versions(faculty: int, 
	                        year: int,
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):              
    return acl.get_load_versions(db=db, faculty=faculty, year=year)


@router.get("/version")
async def get_load_version(version: int,
                           db: Session = Depends(get_db),
                           current_user: asu.User = Depends(acu.get_current_user)):              
    return acl.get_load_version(db=db, version=version)    


@router.get("/table/columns")
async def get_table_columns(faculty: int, 
                            version: int,
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):              
    return acl.get_table_columns(db=db, faculty=faculty, version_id=version)


@router.get("/table/load")
async def get_table_load(version: int, onlyotherstudents: int, ownteachers: int,
                         db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):
    return acl.get_table_load(db, version, onlyotherstudents, ownteachers)


@router.put("/table/loadstroke/active")
async def set_table_load_stroke_active(version: int, uuid: str, value: bool,
                                       db: Session = Depends(get_db),
                                       current_user: asu.User = Depends(acu.get_current_user)):
    return acl.get_table_load_str_active(db, version, uuid, value)


@router.put("/table/loadstroke/department")
async def set_table_load_stroke_department(version: int, uuid: str, department: int,
                                           db: Session = Depends(get_db),
                                           current_user: asu.User = Depends(acu.get_current_user)):
    return acl.change_stroke_department(db, version, uuid, department)


@router.post("/table/loadstroke/appointsubgroups")
async def appoint_subgroups(version: int, uuid: str,
                            block_id: int, hours_type: str,
                            departments: Optional[List[asl.SubgroupDepartments]] = None,
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):
    return acl.appoint_subgroups(db, uuid, version, block_id, hours_type, departments)

    # return ""   

@router.post("/table/loadstroke/joinsubgroups")
async def appoint_subgroups(version: int, uuid: str,
                            hours_type: str,
                            gp_id: int,
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):
    return acl.join_subgroups(db, uuid, version, gp_id, hours_type)


@router.get("/table/loadstroke/subgroupsinfo")
async def get_subgroups_info(version: int, uuid: str,
                             db: Session = Depends(get_db),
                             current_user: asu.User = Depends(acu.get_current_user)):
    return acl.get_subgroups_info(db, uuid, version)    


@router.get("/table/loadstroke/one")
async def get_one_stroke(version: int, uuid: str,
                         db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):
    return acl.get_one_stroke(db, version, uuid)  


@router.put("/table/loadstroke/subgroupdep")
async def set_subgroup_dep(version: int, uuid: str,
                           subgroup_id: int, newdepartment: int,
                           db: Session = Depends(get_db),
                           current_user: asu.User = Depends(acu.get_current_user)):
    return acl.change_dep_of_subgroup(db, uuid, version, subgroup_id, newdepartment)


@router.get("/table/flows")
async def get_all_flows(version: int,
                        own: bool,
                        db: Session = Depends(get_db),
                        current_user: asu.User = Depends(acu.get_current_user)):
    return acl.get_all_flows(db, version, own) 


@router.get("/table/stroke/flows")
async def get_stroke_flows(version: int, uuid: str, db: Session = Depends(get_db),
                           current_user: asu.User = Depends(acu.get_current_user)):
    return acl.get_stroke_flows(db, version, uuid)


@router.post("/table/flow")
async def add_flow(version: int, name: str, dep: int, hours_type: str,
                   db: Session = Depends(get_db),
                   current_user: asu.User = Depends(acu.get_current_user)):
    return acl.add_flow(db, version, hours_type, name, dep) 


@router.post("/table/stroke/flow")
async def add_flow_participant(uuid: str, flow_id: int, group_type: str, 
                               group_id: int, db: Session = Depends(get_db),
                               current_user: asu.User = Depends(acu.get_current_user)):
    return acl.add_flow_participant(db, uuid, flow_id, group_type, group_id)


@router.delete("/table/stroke/flow")
async def remove_flow_participant(uuid: str, flow_id: int, group_type: str, 
                                  group_id: int, db: Session = Depends(get_db),
                                  current_user: asu.User = Depends(acu.get_current_user)):
    return acl.remove_flow_participant(db, uuid, flow_id, group_type, group_id)
    

@router.put("/table/stroke/flow")
async def change_main_in_flow(uuid: str, flow_id: int, group_type: str, 
                              group_id: int, db: Session = Depends(get_db),
                              current_user: asu.User = Depends(acu.get_current_user)):
    return acl.change_main_in_flow(db, uuid, flow_id, group_type, group_id)


@router.delete("/table/flow")
async def delete_flow(flow_id: int,
                      db: Session = Depends(get_db),
                      current_user: asu.User = Depends(acu.get_current_user)):
    return acl.delete_flow(db, flow_id)


@router.post("/table/ownstroke")
async def add_own_stroke(version: int, profile_id: int, course: int, sem: int,
                         type_id: int, department: int, add_inf: Optional[int] = None,
                         db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):
    return acl.add_own_stroke(db, version, profile_id, course, sem, type_id, add_inf,
                              department)

@router.delete("/table/ownstroke")
async def delete_own_stroke(version: int, uuid: str,
                            db: Session = Depends(get_db),
                            current_user: asu.User = Depends(acu.get_current_user)):
    return acl.delete_own_stroke(db, uuid, version)


@router.post("/table/columnvalue")
async def change_column_value(uuid: str, budzhet: bool, version: int,
                              column_id: int, value: float, dep: int,
                              db: Session = Depends(get_db),
                              current_user: asu.User = Depends(acu.get_current_user)):

    return acl.change_column_value(db, uuid, budzhet, version, column_id, value, dep)


@router.delete("/table/columnvalue")
async def return_to_auto_calc(uuid: str, budzhet: bool, version: int,
                              column_id: int, dep: int,
                              db: Session = Depends(get_db),
                              current_user: asu.User = Depends(acu.get_current_user)):
    return acl.return_to_auto_calc(db, uuid, budzhet, version, column_id, dep)


@router.get("/table/allreport")
async def get_all_faculty_report(faculty: int, version: int, onlyotherstudents: int, 
                                 ownteachers: int, db: Session = Depends(get_db)):
    return acl.get_all_faculty_report(db, faculty, version, onlyotherstudents, ownteachers)


@router.get("/table/depreport")
async def get_dep_faculty_report(faculty: int, dep: int, version: int, onlyotherstudents: int, 
                                 ownteachers: int, db: Session = Depends(get_db)):
    return acl.get_dep_faculty_report(db, faculty, dep, version, onlyotherstudents, ownteachers)           

import app.settings as settings
import app.schemas.users as asu
import app.control.users as acu

import app.control.versions as acv
import app.schemas.versions as asv
import app.control.loadcalc as aclc

from fastapi import APIRouter, Depends, HTTPException, status, Body, File, UploadFile
from sqlalchemy.orm import Session
from app.dependencies import get_db
from typing import List
from datetime import timedelta
from typing import Optional


router = APIRouter(
    prefix="/versions",
    tags=["versions"],
)


@router.put("/current")
async def set_current(version: int, db: Session = Depends(get_db)):
    return acv.set_current(db=db, version=version)


@router.post("/approve")
async def aprove_version(version: int, own: bool, other: bool,
                         comment: asv.Comment,
                         db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):              
    return acv.approve_version(db=db, version=version, comment=comment, own=own, other=other,
                               user=current_user)


@router.post("/calc")
async def calc_load(up_ver: int, new_ver: bool, faculty_id: int, 
	                year: int, name: str, db: Session = Depends(get_db)):
	return acv.calc_load(db, up_ver, new_ver, faculty_id, year, name)


@router.post("/calcvn")
async def calc_vn_load(version: int, db: Session = Depends(get_db)):
    return aclc.load_calc_update(db, version)    


@router.delete("/")
async def delete_version(version: int, db: Session = Depends(get_db),
                         current_user: asu.User = Depends(acu.get_current_user)):
    return acv.delete_version(db, version)     
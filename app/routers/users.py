import app.models.isuload as ami
import app.schemas.users as asu
import app.control.users as acu
import app.settings as settings

from fastapi import APIRouter, Depends, HTTPException, status, Body
from app.dependencies import get_db
from sqlalchemy.orm import Session
from typing import List
from datetime import timedelta


router = APIRouter(
    prefix="/users",
    tags=["users"],
    # dependencies=[Depends(get_db)],
    # responses={404: {"description": "Not found"}},
)


@router.get("/current", response_model=asu.User)
async def get_current_user(current_user: asu.User = Depends(acu.get_current_user)):
    return current_user


@router.get("/", response_model=List[asu.User])
async def read_users(db: Session = Depends(get_db), 
               current_user: asu.User = Depends(acu.get_current_user)):
    return acu.get_users(db=db) 


@router.get("/{login}", response_model=asu.User)
async def read_user(login: str, db: Session = Depends(get_db), 
                    current_user: asu.User = Depends(acu.get_current_user)):
    return acu.get_user_by_login(db=db, login=login)       


@router.post("/", response_model=asu.User)
async def create_user(user: asu.UserCreate, db: Session = Depends(get_db)):
    db_user = acu.get_user_by_login(db, login=user.login)   
    if db_user:
        raise HTTPException(status_code=400, detail="Login already registered")
    return acu.create_user(db=db, user=user)    


@router.post("/login", response_model=asu.Token)
async def login(cr: asu.Crendetails, db: Session = Depends(get_db)):
    user = acu.authenticate_user(db, cr.login, cr.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = acu.create_token(
        data={"sub": user.login, 'type': 'access'}, expires_delta=access_token_expires
    )
    refresh_token_expires = timedelta(minutes=settings.REFRESH_TOKEN_EXPIRE_MINUTES)
    refresh_token = acu.create_token(
        data={"sub": user.login, 'type': 'refresh'}, expires_delta=refresh_token_expires
    )

    return {"access_token": access_token, "refresh_token": refresh_token, "user": user}


@router.post("/refresh_token")
async def refresh_token(new_token: asu.NewTokenData = Depends(acu.refresh)):
    return new_token 

@router.put("/year")
async def set_year(year: int, db: Session = Depends(get_db), 
                   current_user: asu.User = Depends(acu.get_current_user)):
    return acu.change_year(db, current_user, year)

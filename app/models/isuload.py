from sqlalchemy import (Boolean, Column, ForeignKey, 
                        Integer, String, DateTime, SmallInteger, Numeric)
from sqlalchemy.orm import relationship
from app.database import Base


class User(Base):
    __tablename__ = "users"

    id = Column(Integer, primary_key=True)
    login = Column(String(40), unique=True)
    surname = Column(String(40))
    name = Column(String(40))
    patr = Column(String(40))
    hashed_pass = Column(String(64))
    edited_year = Column(Integer)

    roles = relationship("Role", back_populates="user", 
                          cascade="all, delete", passive_deletes=True)



class Role(Base):
    __tablename__ = "users_roles"

    id = Column(Integer, primary_key=True)
    role = Column(String(15))
    user_id = Column(Integer, ForeignKey("users.id", ondelete="CASCADE"))
    right = Column(String(5))
    inf = Column(String(5))
    faculty_id = Column(Integer)

    user = relationship("User", back_populates="roles")


class Faculty(Base):
    __tablename__ = "faculties"

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    active = Column(Boolean)
    uid_1c = Column(String(72))

    directions = relationship("Direction", back_populates="faculty")


class FacultyNameVersion(Base):
    __tablename__ = "faculties_names_versions"

    id = Column(Integer, primary_key=True)
    faculty_id = Column(Integer, ForeignKey("faculties.id"))
    name = Column(String(255))
    date_start = Column(DateTime)
    group_template = Column(String(10))
   

class ConstrDirectory(Base):
    __tablename__ = "constr_directory"

    id = Column(Integer, primary_key=True)
    name = Column(String(150))
    value = Column(Integer)
    active = Column(Boolean)
    type = Column(String(150))
    own = Column(Boolean)
    add_inf_type = Column(String(30))


class Direction(Base):
    __tablename__ = "directions"

    id = Column(Integer, primary_key=True)
    shifr = Column(String(50))
    name = Column(String(250))
    qual_id = Column(SmallInteger)
    form_id = Column(SmallInteger)
    faculty_id = Column(Integer, ForeignKey("faculties.id"))
    uid_1c = Column(String(64))
    group_template = Column(String(10))
    active = Column(Boolean)

    faculty = relationship("Faculty", back_populates="directions")
    qual = relationship("ConstrDirectory", foreign_keys=[qual_id],
                        primaryjoin="and_(Direction.qual_id==ConstrDirectory.value, "
                                          "ConstrDirectory.type=='qualification')")

    form = relationship("ConstrDirectory", foreign_keys=[form_id],
                        primaryjoin="and_(Direction.form_id==ConstrDirectory.value, "
                                          "ConstrDirectory.type=='forma')")

    profiles = relationship("Profile", back_populates="direction")


class Profile(Base):
    __tablename__ = "profiles"

    id = Column(Integer, primary_key=True)
    direction_id = Column(Integer, ForeignKey("directions.id"))
    name = Column(String(255))
    uid_1c = Column(String(64))
    active = Column(Boolean)

    direction = relationship("Direction", back_populates="profiles")


class LessonPlan(Base):
    __tablename__ = "lessonplan"

    id = Column(Integer, primary_key=True)
    course = Column(SmallInteger)
    profile_id = Column(Integer, ForeignKey("profiles.id"))
    year = Column(SmallInteger)
    bc = Column(SmallInteger)
    vc = Column(SmallInteger)
    bg = Column(SmallInteger)
    vg = Column(SmallInteger)

    profile = relationship("Profile")


class TypesOfWorkDirectory(Base):
    __tablename__ = "types_of_work_directory"

    id = Column(Integer, primary_key=True)
    code = Column(Integer)
    name = Column(String(200))
    active = Column(Boolean)    
    active_calc = Column(Boolean)    
    type = Column(String(30)) 


class LessonPlanStroke(Base):
    __tablename__ = "lessonplanstroke"

    id = Column(Integer, primary_key=True)
    uuid = Column(String(36))
    lessonplan_id = Column(Integer, ForeignKey("lessonplan.id"))
    code = Column(String(70))
    name = Column(String(150))
    sem = Column(SmallInteger)
    department = Column(SmallInteger)
    choice = Column(Boolean)
    object_type = Column(Integer)

    lessonplan = relationship("LessonPlan")
    lessonplanworks = relationship("LessonPlanWork", back_populates="lessonplanstroke")


class LessonPlanWork(Base):
    __tablename__ = "lessonplanworks"

    id = Column(Integer, primary_key=True)
    work_type = Column(Integer)
    work_count = Column(Integer)
    lessonplanstroke_id = Column(Integer, ForeignKey("lessonplanstroke.id"))

    lessonplanstroke = relationship("LessonPlanStroke", back_populates="lessonplanworks")


class TimeTable(Base):
    __tablename__ = "timetable"

    id = Column(Integer, primary_key=True)
    lessonplan_id = Column(Integer, ForeignKey("lessonplan.id"))
    sem = Column(SmallInteger)
    hours = Column(Integer)


class GroupProfile(Base):
    __tablename__ = "groups_profile"

    id = Column(Integer, primary_key=True)
    name = Column(String(150))
    budzhet_count = Column(Integer)    
    vnebudzhet_count = Column(Integer)    
    budzhet_foreign = Column(Integer)    
    vnebudzhet_foreign = Column(Integer) 
    budzhet = Column(Boolean)
    profile_id = Column(Integer, ForeignKey("profiles.id"))
    course = Column(SmallInteger)
    year = Column(SmallInteger)

    subgroups = relationship("SubGroup", back_populates="parent_group")


class SubGroup(Base):
    __tablename__ = "subgroups"

    id = Column(Integer, primary_key=True)
    name = Column(String(150))
    parent_id = Column(Integer, ForeignKey("groups_profile.id"))
    block_id = Column(Integer)
    budzhet_count = Column(Integer)   
    vnebudzhet_count = Column(Integer)    
    budzhet_foreign = Column(Integer)    
    vnebudzhet_foreign = Column(Integer)
    active = Column(Boolean)

    parent_group = relationship("GroupProfile", back_populates="subgroups")


class GroupChoice(Base):
    __tablename__ = "groups_choice" 

    id = Column(Integer, primary_key=True)   
    uuid = Column(String(150))
    budzhet_count = Column(Integer)   
    vnebudzhet_count = Column(Integer)    
    budzhet_foreign = Column(Integer)    
    vnebudzhet_foreign = Column(Integer)
    block = Column(String(30))


class LoadVersion(Base):
    __tablename__ = "loadversions"

    id = Column(Integer, primary_key=True)
    faculty_id = Column(Integer)
    year = Column(SmallInteger)
    name = Column(String(150))
    current = Column(Boolean)


class VersionHistory(Base):
    __tablename__ = "versions_history"

    id = Column(Integer, primary_key=True)
    timestamp = Column(DateTime)
    version = Column(Integer, ForeignKey("loadversions.id"))
    user_id = Column(Integer)
    active_own = Column(Boolean)
    comment = Column(String(500))
    active_other = Column(Boolean)


class ColumnLoad(Base):
    __tablename__ = "columns"

    id = Column(Integer, primary_key=True)
    name = Column(String(150))
    alias = Column(String(100))
    year = Column(Integer)
    parent_id = Column(Integer)
    active = Column(Boolean)
    active_view = Column(Boolean)
    faculty_id = Column(Integer)


class CalcConstraint(Base):
    __tablename__ = "calc_constrains"

    id = Column(Integer, primary_key=True)
    calc_id = Column(Integer, ForeignKey("columns_calc.id"))
    constr_name = Column(String(20)) 
    constr_value = Column(Integer)


class ColumnCalc(Base):
    __tablename__ = "columns_calc"

    id = Column(Integer, primary_key=True)
    column_id = Column(Integer, ForeignKey("columns.id"))
    formula = Column(String(400))
    formula_text = Column(String(600))
    active = Column(Boolean)

    constraints = relationship(CalcConstraint)


class Department(Base):
    __tablename__ = "departments"

    id = Column(Integer, primary_key=True)
    name = Column(String(400))
    alias = Column(String(400))
    number = Column(Integer)
    faculty = Column(Integer)


class Flow(Base):
    __tablename__ = "flows"

    id = Column(Integer, primary_key=True)
    type = Column(String(20))
    load_version = Column(Integer)
    name = Column(String(255))
    department = Column(Integer)
    main = Column(Integer)

    loadstrokes = relationship("LoadStroke")


class Load(Base):
    __tablename__ = "load"

    id = Column(Integer, primary_key=True)
    course = Column(SmallInteger)
    profile_id = Column(Integer, ForeignKey("profiles.id"))
    year = Column(Integer)
    version = Column(Integer)
    own_faculty = Column(Boolean)

    profile = relationship("Profile")


class LoadColumn(Base):
    __tablename__ = "loadcolumns"

    id = Column(Integer, primary_key=True)
    calc_id = Column(Integer)
    value = Column(Numeric(10, 4))
    uuid = Column(String(100))
    group_id = Column(Integer)
    group_type = Column(String(20))
    budzhet = Column(Boolean)
    load_version = Column(Integer)
    teacher_id = Column(Integer)
    hand = Column(Boolean)


class LoadStroke(Base):
    __tablename__ = "loadstrokes"

    id = Column(Integer, primary_key=True)
    uuid = Column(String(100))
    load_id = Column(Integer, ForeignKey("load.id"))
    code = Column(String(70))
    name = Column(String(150))
    sem = Column(SmallInteger)
    budzhet = Column(Boolean)
    department = Column(SmallInteger)
    choice = Column(Boolean)
    count_students = Column(SmallInteger)
    count_groups = Column(SmallInteger)
    teacher_id = Column(Integer)
    group_id = Column(Integer)
    group_type = Column(String(20))
    alias_name = Column(String(255))
    count_hours = Column(Integer)
    hours_type = Column(String(20))
    is_own = Column(Boolean)
    active = Column(Boolean)
    current_faculty = Column(Boolean)
    add_inf = Column(Integer)
    object_type = Column(Integer)
    flow_id = Column(Integer, ForeignKey("flows.id"))
    count_foreign = Column(SmallInteger)
    show = Column(Boolean)

    subgroup = relationship("SubGroup", foreign_keys=[group_id],
                             primaryjoin="and_(SubGroup.id==LoadStroke.group_id, "
                                         "LoadStroke.group_type=='subgroup')",
                             viewonly=True)

    group_profile = relationship("GroupProfile", foreign_keys=[group_id],
                                  primaryjoin="and_(GroupProfile.id==LoadStroke.group_id, "
                                                    "LoadStroke.group_type=='profile')",
                                  viewonly=True)



class Teacher(Base):
    __tablename__ = "teachers"

    id = Column(Integer, primary_key=True)
    surname = Column(String(60))
    name = Column(String(60))
    fathername = Column(String(60))
    position = Column(String(150))
    degree = Column(String(150))
    academic_rank = Column(String(150))
    part_of_rate = Column(Numeric(10, 4))
    state = Column(Boolean)
    department = Column(Integer)
    mail = Column(String(100))
    uid_1c = Column(String(72))
    active = Column(Boolean)














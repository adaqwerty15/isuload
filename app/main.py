from fastapi import FastAPI
from app.routers import (users, dirs, plans, contingent, 
                         load, teachers, columns, versions)
from fastapi.middleware.cors import CORSMiddleware
from app import settings


app = FastAPI()

origins = settings.ORIGINS

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)
app.include_router(dirs.router)
app.include_router(plans.router)
app.include_router(contingent.router)
app.include_router(load.router)
app.include_router(teachers.router)
app.include_router(columns.router)
app.include_router(versions.router)

# app.include_router(
#     admin.router,
#     prefix="/admin",
#     tags=["admin"],
#     dependencies=[Depends(get_token_header)],
#     responses={418: {"description": "I'm a teapot"}},
# )


@app.get("/")
async def root():
    return {"message": "Hello World!"}
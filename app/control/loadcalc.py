import app.models.isuload as ami
import app.schemas.users as asu
import app.settings as settings

import app.schemas.load as asl
import app.control.dirs as acd
import copy

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func
from sqlalchemy.dialects import postgresql
from sqlalchemy import update
from sqlalchemy import delete, or_, and_
from sqlalchemy.orm import aliased


def load_calc_for_one(db: Session, uuid: str, version: int):
	ver = db.query(ami.LoadVersion).get({"id":version})

	del_no_active = db.execute("""DELETE
								FROM loadcolumns
								WHERE id in
								    (SELECT lc.id
								     FROM loadcolumns AS lc,
								          columns_calc AS cc
								     WHERE load_version=:version
								       AND lc.calc_id=cc.id
								       AND cc.active=FALSE
								       AND lc.uuid=:uuid)
								  AND loadcolumns.hand=FALSE;""",
								  {"uuid": uuid, "version": version})
	db.commit()															   

	rows = db.execute("""
				  SELECT UUID,
				       sum(count_students) AS count_students,
				       sum(count_groups) AS count_groups,
				       sum(lec) AS lec,
				       sum(pract) AS pract,
				       sum(lab) AS lab,
				       add_inf,
				       object_type,
				       form_id,
				       qual_id,
				       budzhet,
				       group_id,
				       group_type,
				       sum(count_foreign) AS count_foreign,
				       tid,
				       (CASE
				            WHEN mainflow > 0
				                 AND mf > 0
				                 OR mf < 0 THEN 1
				            ELSE 0
				        END) AS mainflow
				FROM
				  (SELECT UUID,
				          count_students,
				          count_groups,
				          SUM (CASE
				                   WHEN hours_type='lek' THEN count_hours
				                   ELSE 0
				               END) AS lec,
		              SUM (CASE
		                       WHEN hours_type='pract' THEN count_hours
		                       ELSE 0
		                   END) AS pract,
                  SUM (CASE
                           WHEN hours_type='lab' THEN count_hours
                           ELSE 0
                       END) AS lab,
                  (CASE
                       WHEN (object_type=3
                             OR object_type=31
                             OR object_type=6) THEN max(add_inf)
                       ELSE sum(add_inf)
                   END) AS add_inf,
                   object_type,
                   form_id,
                   qual_id,
                   budzhet,
                   CASE
                       WHEN group_type='subgroup'
                            AND object_type=2 THEN ss.parent_id
                       ELSE group_id
                   END,
                   CASE
                       WHEN group_type='subgroup'
                            AND object_type=2 THEN 'profile'
                       ELSE group_type
                   END,
                   count_foreign,
                   (CASE
                        WHEN object_type=2 THEN -1
                        ELSE teacher_id
                    END) AS tid,
                    MAX (CASE
                             WHEN main=loadstrokes.id THEN 1
                             ELSE 0
                         END) AS mainflow,
				            MAX (loadstrokes.flow_id) AS mf
				   FROM LOAD,
				        profiles_full,
				        loadstrokes
				   LEFT JOIN subgroups AS ss ON loadstrokes.group_id=ss.id
				   LEFT JOIN flows ON loadstrokes.flow_id=flows.id
				   WHERE load_id=load.id
				     AND VERSION=:version
				     AND YEAR=:year
				     AND load.own_faculty=TRUE
				     AND profiles_full.profile_id=load.profile_id
				     AND UUID=:uuid
				     AND loadstrokes.active=TRUE
				   GROUP BY UUID,
				            count_groups,
				            count_students,
				            group_type,
				            group_id,
				            object_type,
				            form_id,
				            qual_id,
				            budzhet,
				            ss.parent_id,
				            count_foreign,
				            tid) AS res
				GROUP BY UUID,
				         object_type,
				         form_id,
				         qual_id,
				         budzhet,
				         group_id,
				         group_type,
				         tid,
				         add_inf,
				         mainflow,
				         mf;
	       """, {"version": version, "year": ver.year, "uuid": uuid})

	plans = db.query(ami.LessonPlanWork, ami.LessonPlanStroke.uuid).\
	           join(ami.LessonPlanStroke).\
	           filter(ami.LessonPlanStroke.uuid == uuid).\
	           filter(ami.LessonPlanWork.work_count != None).\
	           all()  

	loadstrokes = get_load_strokes_for_calc(db, rows, plans)           	
	calc_load(db, ver, loadstrokes)	                               			

	return loadstrokes


def load_calc_update(db: Session, version: int):
	ver = db.query(ami.LoadVersion).get({"id":version})

	del_no_active = db.execute("""DELETE
								FROM loadcolumns
								WHERE id in
								    (SELECT lc.id
								     FROM loadcolumns AS lc,
								          columns_calc AS cc
								     WHERE load_version=:version
								       AND lc.calc_id=cc.id
								       AND cc.active=FALSE)
								  AND loadcolumns.hand=FALSE;""",
								  {"version": version})
	db.commit()		

	print("del_no_active")													   

	rows = db.execute("""
					  SELECT UUID,
						       sum(count_students) AS count_students,
						       sum(count_groups) AS count_groups,
						       sum(lec) AS lec,
						       sum(pract) AS pract,
						       sum(lab) AS lab,
						       add_inf,
						       object_type,
						       form_id,
						       qual_id,
						       budzhet,
						       group_id,
						       group_type,
						       sum(count_foreign) AS count_foreign,
						       tid,
						       (CASE
						            WHEN mainflow > 0
						                 AND mf > 0
						                 OR mf < 0 THEN 1
						            ELSE 0
						        END) AS mainflow
						FROM
						  (SELECT UUID,
						          count_students as count_students,
						          count_groups,
						          SUM (CASE
						                   WHEN hours_type='lek' THEN count_hours
						                   ELSE 0
						               END) AS lec,
						              SUM (CASE
						                       WHEN hours_type='pract' THEN count_hours
						                       ELSE 0
						                   END) AS pract,
						                  SUM (CASE
						                           WHEN hours_type='lab' THEN count_hours
						                           ELSE 0
						                       END) AS lab,
						                      (CASE
						                           WHEN object_type=3
						                                OR object_type=31
						                                OR object_type=6 THEN max(add_inf)
						                           ELSE sum(add_inf)
						                       END) AS add_inf,
						                      object_type,
						                      form_id,
						                      qual_id,
						                      budzhet,
						                      CASE
						                          WHEN group_type='subgroup'
						                               AND object_type=2 THEN ss.parent_id
						                          ELSE group_id
						                      END,
						                      CASE
						                          WHEN group_type='subgroup'
						                               AND object_type=2 THEN 'profile'
						                          ELSE group_type
						                      END,
						                      count_foreign,
						                      (CASE
						                           WHEN object_type=2 THEN -1
						                           ELSE teacher_id
						                       END) AS tid,
						                      MAX (CASE
						                               WHEN main=loadstrokes.id THEN 1
						                               ELSE 0
						                           END) AS mainflow,
						                          MAX (loadstrokes.flow_id) AS mf
						   FROM LOAD,
						        profiles_full,
						        loadstrokes
						   LEFT JOIN subgroups AS ss ON loadstrokes.group_id=ss.id
						   LEFT JOIN flows ON loadstrokes.flow_id=flows.id
						   WHERE load_id=load.id
						     AND VERSION=:version
						     AND YEAR=:year
						     AND load.own_faculty=TRUE
						     AND profiles_full.profile_id=load.profile_id
						     AND loadstrokes.active=TRUE
						   GROUP BY UUID,
						            count_groups,
						            count_students,
						            group_type,
						            group_id,
						            object_type,
						            form_id,
						            qual_id,
						            budzhet,
						            ss.parent_id,
						            count_foreign,
						            tid) AS res
						GROUP BY UUID,
						         object_type,
						         form_id,
						         qual_id,
						         budzhet,
						         group_id,
						         add_inf,
						         group_type,
						         tid,
						         mainflow,
						         mf;
	       """, {"version": version, "year": ver.year})

	print("get_strokes")

	plans = db.query(ami.LessonPlanWork, ami.LessonPlanStroke.uuid).\
	           join(ami.LessonPlanStroke).\
	           join(ami.LessonPlan).\
	           filter(ami.LessonPlan.year == ver.year).\
	           filter(ami.LessonPlanWork.work_count != None).\
	           all()  

	print("get_plans")

	loadstrokes = get_load_strokes_for_calc(db, rows, plans)

	print("form_strokes")           	
	calc_load(db, ver, loadstrokes)

	print("load_calced")	                               			

	return ""



def get_load_strokes_for_calc(db, rows, plans):
	load = {}
	for row in rows:
		if row.uuid not in load:
			load[row.uuid] = []
			
		load[row.uuid].append({"countStudents": float(row.count_students),
			                   "countGroups": row.count_groups,
			                   "countForeign": row.count_foreign,
	  			               "lecturesFact": float(row.lec),
	  			               "practiceFact": float(row.pract),
	  			               "labFact": float(row.lab),
	  			               "addInf": row.add_inf,
	  			               "mainFlow": row.mainflow,
	  			               "uuid": row.uuid,
	  			               "objectType": row.object_type,
	  			               "forma": row.form_id,
	  			               "qualification": row.qual_id,
	  			               "budzhet": row.budzhet,
	  			               "group": row.group_id,
	  			               "groupType": row.group_type,
	  			               "teacherId": row.tid})

	tow = db.query(ami.TypesOfWorkDirectory.code).all()
	types_of_work = [t[0] for t in tow]
	
	plans_values = {}
	for row in plans:
		if row.uuid not in plans_values:
			plans_values[row.uuid] = {}
		plans_values[row.uuid][row.LessonPlanWork.work_type] = row.LessonPlanWork.work_count

	for key in plans_values:
		tw = plans_values[key]
		for k in types_of_work:
			if k not in tw:
				tw["t_e_{}".format(k)] = 0
				tw["t_c_{}".format(k)] = 0
			else:
				tw["t_e_{}".format(k)] = 1
				tw["t_c_{}".format(k)] = tw[k]

	loadstrokes = []			

	for row in load:
		for l in load[row]:
			if (row in plans_values):
				l["typesOfWork"] = plans_values[row]
			else:
				l["typesOfWork"] = {}	
			loadstrokes.append(l)

	return loadstrokes		


def evaluate(expression, d):
    code = compile(expression, "<string>", "eval")
    try:
      return eval(code, {"__builtins__": {}}, {"loadstroke": d})
    except:
      return '-'


def calc_load(db, ver, loadstrokes):
	formuls = db.query(ami.CalcConstraint, ami.ColumnCalc).\
	             join(ami.ColumnCalc).\
	             join(ami.ColumnLoad).\
	             filter(ami.ColumnLoad.year == ver.year).\
	             filter(ami.ColumnCalc.active == True).\
	             filter(ami.ColumnLoad.active == True).\
	             all()

	fmls = {}             

	for row in formuls:
		if (row.ColumnCalc.id not in fmls):
			fmls[row.ColumnCalc.id] = {"id": row.ColumnCalc.id, "formula": row.ColumnCalc.formula,
			                           "constrs": {row.CalcConstraint.constr_name: [row.CalcConstraint.constr_value]}}
		else:
			constrs = fmls[row.ColumnCalc.id]["constrs"]
			if row.CalcConstraint.constr_name not in constrs:
				constrs[row.CalcConstraint.constr_name] = [row.CalcConstraint.constr_value]
			else:
				constrs[row.CalcConstraint.constr_name].append(row.CalcConstraint.constr_value)	        

	for f in fmls:
		loadf = copy.deepcopy(loadstrokes)
		for c in fmls[f]["constrs"]:
			if (c == "object_type"):
					loadf = [l for l in loadf if l["objectType"] in fmls[f]["constrs"][c]]
			elif (c == "forma"):
					loadf = [l for l in loadf if l["forma"] in fmls[f]["constrs"][c]]
			elif (c == "qualification"):
					loadf = [l for l in loadf if l["qualification"] in fmls[f]["constrs"][c]]	

		formula = fmls[f]["formula"]
		formula_calc = ""
		s = formula.split('|')
		for index, op in enumerate(s):
			if 'l_' in op:
				s[index] = 'loadstroke["{}"]'.format(op[2:])
			elif 't_' in op:
				s[index] = 'loadstroke["typesOfWork"]["{}"]'.format(op)	
		formula_calc = ''.join(s)

		for row in loadf:
			val = evaluate(formula_calc, row)
			print(val)
			print(formula_calc)
			print(row)
			if (val != '-'):
				if row["objectType"] == 2:
					stmt = update(ami.LoadColumn).\
					       where(ami.LoadColumn.uuid == row["uuid"]).\
								 where(ami.LoadColumn.group_id == row["group"]).\
								 where(ami.LoadColumn.group_type == row["groupType"]).\
								 where(ami.LoadColumn.calc_id == fmls[f]["id"]).\
								 where(ami.LoadColumn.budzhet == row["budzhet"]).\
								 where(ami.LoadColumn.load_version == ver.id).\
								 where(ami.LoadColumn.hand == False).\
								 values(value=val).\
								 execution_options(synchronize_session="fetch")

					res = db.execute(stmt)
					db.commit()

					if (res.rowcount == 0):
						ins = db.execute("""
						                  INSERT INTO loadcolumns (UUID, group_id, group_type, calc_id, 
						                              value, budzhet, load_version, teacher_id, hand)
						                  SELECT :uuid,
						                         :group,
						                         :group_type,
						                         :calc_id,
						                         :val,
						                         :budzhet,
						                         :version,
						                         :tid,
						                         :hand
						                  WHERE NOT EXISTS
						                      (SELECT 1
						                       FROM loadcolumns
						                       WHERE UUID=:uuid
						                         AND group_id=:group
						                         AND group_type=:group_type
						                         AND calc_id=:calc_id
						                         AND budzhet=:budzhet
						                         AND load_version=:version
						                         AND hand=FALSE ) ON CONFLICT (calc_id,
						                                                       UUID,
						                                                       group_id,
						                                                       group_type,
						                                                       load_version,
						                                                       budzhet,
						                                                       teacher_id) DO NOTHING
							               """, {"uuid": row["uuid"], "group": row["group"], "group_type": row["groupType"],
							                     "calc_id": fmls[f]["id"], "val": val, "budzhet": row["budzhet"],
							                     "version": ver.id, "tid": row["teacherId"], "hand": False})
						db.commit()
				else:
					ins = db.execute("""
									  INSERT INTO loadcolumns (UUID, group_id, group_type, calc_id, value, budzhet, 
														                         load_version, teacher_id, hand)
					                  SELECT :uuid,
						                       :group,
						                       :group_type,
						                       :calc_id,
						                       :val,
						                       :budzhet,
						                       :version,
						                       :tid,
						                       :hand
					                  WHERE NOT EXISTS
					                      (SELECT 1
					                       FROM loadcolumns
					                       WHERE UUID=:uuid
					                         AND group_id=:group
					                         AND group_type=:group_type
					                         AND calc_id=:calc_id
					                         AND budzhet=:budzhet
					                         AND load_version=:version
					                         AND hand=TRUE ) ON CONFLICT (calc_id,
					                                                      UUID,
					                                                      group_id,
					                                                      group_type,
					                                                      load_version,
					                                                      budzhet,
					                                                      teacher_id) DO
					                    UPDATE
					                    SET value=:val;
						               """, {"uuid": row["uuid"], "group": row["group"], "group_type": row["groupType"],
							                   "calc_id": fmls[f]["id"], "val": val, "budzhet": row["budzhet"],
							                   "version": ver.id, "tid": row["teacherId"], "hand": False})
					db.commit()


	return ""
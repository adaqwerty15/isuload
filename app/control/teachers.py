import app.models.isuload as ami
import app.models.consts as amc
import app.schemas.users as asu
import app.settings as settings

import app.schemas.teachers as ast
import app.control.loadcalc as acl
import app.control.dirs as acd
import app.control.load as acld
import copy
import xlsxwriter

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File, Response
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func
from sqlalchemy.dialects import postgresql
from sqlalchemy import update
from sqlalchemy import delete, or_, and_
from sqlalchemy.orm import aliased
from io import BytesIO

def teachers_load_stat(db: Session, faculty: int, year: int):
    ver = get_approved_version(db, faculty, year)
    vals = []

    if (ver):

        values = db.execute("""
                            SELECT ah.count_hours AS all_hours,
                                   th.count_hours AS t_hours,
                                   th.number AS department,
                                   rates.count_hours AS rates
                            FROM
                              (SELECT coalesce(sum(count_hours), 0) AS count_hours, number
                               FROM departments AS d
                               LEFT JOIN
                                 (SELECT count_hours,
                                         department
                                  FROM loadstrokes AS ls,
                                       LOAD
                                  WHERE ls.load_id=load.id
                                    AND VERSION=:ver
                                    AND active=TRUE
                                  UNION ALL SELECT value,
                                                   department
                                  FROM loadcolumns AS lc
                                  INNER JOIN
                                    (SELECT DISTINCT UUID,
                                                     department,
                                                     group_id,
                                                     group_type,
                                                     budzhet
                                     FROM loadstrokes AS ls,
                                          LOAD
                                     WHERE ls.load_id=load.id
                                       AND VERSION=:ver
                                       AND active=TRUE) AS res2 ON res2.uuid=lc.uuid
                                  AND res2.group_id=lc.group_id
                                  AND res2.group_type=lc.group_type
                                  AND res2.budzhet=lc.budzhet
                                  WHERE lc.load_version=:ver ) AS res ON d.number=res.department
                               WHERE d.faculty=:faculty
                               GROUP BY number) AS ah
                            LEFT JOIN
                              (SELECT coalesce(sum(count_hours), 0) AS count_hours, number
                               FROM departments AS d
                               LEFT JOIN
                                 (SELECT count_hours,
                                         department
                                  FROM loadstrokes AS ls,
                                       LOAD
                                  WHERE ls.load_id=load.id
                                    AND VERSION=:ver
                                    AND active=TRUE
                                    AND teacher_id<>-1
                                  UNION ALL SELECT value,
                                                   department
                                  FROM loadcolumns AS lc
                                  INNER JOIN
                                    (SELECT DISTINCT UUID,
                                                     department,
                                                     group_id,
                                                     group_type,
                                                     budzhet
                                     FROM loadstrokes AS ls,
                                          LOAD
                                     WHERE ls.load_id=load.id
                                       AND VERSION=:ver
                                       AND active=TRUE) AS res2 ON res2.uuid=lc.uuid
                                  AND res2.group_id=lc.group_id
                                  AND res2.group_type=lc.group_type
                                  AND res2.budzhet=lc.budzhet
                                  WHERE lc.load_version=:ver
                                    AND teacher_id>0 ) AS res ON d.number=res.department
                               WHERE d.faculty=:faculty
                               GROUP BY number) AS th ON th.number=ah.number
                            LEFT JOIN
                              (SELECT coalesce(sum(part_of_rate), 0) AS count_hours, number
                               FROM departments AS d
                               LEFT JOIN
                                 (SELECT part_of_rate,
                                         department
                                  FROM teachers) AS res ON d.number=res.department
                               WHERE d.faculty=:faculty
                               GROUP BY number) AS rates ON ah.number=rates.number;
                            """, {'ver': ver.version, "faculty": faculty})
        
        for row in values:
            vals.append({"all_hours": row.all_hours.normalize(),
                         "t_hours": row.t_hours.normalize(),
                         "department": row.department,
                         "rates": row.rates.normalize()})

    return vals


def get_approved_version(db: Session, faculty: int, year: int):
    ver = db.query(ami.VersionHistory).\
             join(ami.LoadVersion).\
             filter(ami.LoadVersion.faculty_id == faculty).\
             filter(ami.LoadVersion.year == year).\
             filter(ami.VersionHistory.active_own == True).\
             order_by(ami.VersionHistory.timestamp.desc()).\
             first()

    if ver:         
        ver.formatted_date = ver.timestamp.strftime("%d.%m.%Y в %H:%M")         
    
    return ver


def get_teacher_load_for_dep(db: Session, dep: int, year: int):
    faculty = db.query(ami.Department.faculty).\
                 filter(ami.Department.number == dep).\
                 first()

    faculty_id = faculty.faculty

    ver = get_approved_version(db, faculty_id, year)

    selbase = db.execute("""
                            SELECT sum(count_students) AS cs,
                           sum(count_hours) AS ch,
                           UUID,
                           load_id,
                           code,
                           res.name,
                           sem,
                           res.department,
                           teacher_id,
                           group_id,
                           group_type,
                           alias_name,
                           hours_type,
                           object_type,
                           flow_id,
                           res.profile_id,
                           res.course,
                           gp.name AS groupname,
                           subgroups.name AS sname,
                           profiles_full.dir_name,
                           profiles_full.qualification,
                           profiles_full.shifr,
                           profiles_full.forma,
                           profiles_full.profile_name,
                           teachers.name AS tname,
                           teachers.surname,
                           teachers.fathername,
                           sum(bc) AS bc,
                           sum(vc) AS vc
                    FROM
                      (SELECT ls.id,
                              UUID,
                              load.profile_id,
                              load_id,
                              load.course,
                              code,
                              ls.name,
                              count_students,
                              sem,
                              department,
                              teacher_id,
                              group_id,
                              group_type,
                              budzhet,
                              alias_name,
                              count_hours,
                              hours_type,
                              object_type,
                              flow_id,
                              (CASE
                                   WHEN ls.budzhet=TRUE THEN ls.count_students
                                   ELSE 0
                               END) AS bc,
                              (CASE
                                   WHEN ls.budzhet=FALSE THEN ls.count_students
                                   ELSE 0
                               END) AS vc
                       FROM loadstrokes AS ls,
                            LOAD
                       WHERE load.id=ls.load_id
                         AND load.version=:ver
                         AND active=TRUE
                         AND current_faculty=TRUE
                         AND department=:dep) AS res
                    LEFT JOIN groups_profile AS gp ON res.group_id=gp.id
                    AND res.group_type='profile'
                    LEFT JOIN subgroups ON res.group_id=subgroups.id
                    AND res.group_type='subgroup'
                    LEFT JOIN profiles_full ON res.profile_id=profiles_full.profile_id
                    LEFT JOIN teachers ON res.teacher_id=teachers.id
                    GROUP BY UUID,
                             load_id,
                             code,
                             res.name,
                             sem,
                             res.department,
                             teacher_id,
                             group_id,
                             group_type,
                             alias_name,
                             hours_type,
                             object_type,
                             flow_id,
                             res.profile_id,
                             res.course,
                             gp.name,
                             subgroups.name,
                             profiles_full.dir_name,
                             profiles_full.qualification,
                             profiles_full.shifr,
                             profiles_full.forma,
                             profiles_full.profile_name,
                             teachers.name,
                             teachers.surname,
                             teachers.fathername;
                        """, {"dep": dep, "ver": ver.version})

    strokes = {}
    strokes_res = []

    for row in selbase:
        key = "{}_{}_{}".format(row.uuid, row.group_type, row.group_id)

        if key not in strokes:
            strokes[key] = []

        group_name = ""     
        if (row.group_type == "profile"):
            group_name = row.groupname
        elif (row.group_type == "subgroup"):
            group_name = row.sname  

        strokes[key].append({"uuid": row.uuid, "code": row.code,
                             "name": row.name, "alias_name": row.alias_name,
                             "course": row.course, "sem": row.sem,
                             "count_students": row.cs, "bc": row.bc,
                             "vc": row.vc, "count_hours": row.ch,
                             "hours_type": row.hours_type, "object_type": row.object_type,
                             "group_id": row.group_id, "group_type": row.group_type,
                             "group_name": group_name, "dir_name": row.dir_name,
                             "profile_name": row.profile_name, "shifr": row.shifr,
                             "qual": row.qualification, "form": row.forma,
                             "teacher_id": row.teacher_id, "tname": row.tname,
                             "tsurname": row.surname, "tfathername": row.fathername
                             })

    colsvalues = db.execute("""
                            SELECT calc_id,
                                   val,
                                   UUID,
                                   group_id,
                                   group_type,
                                   teacher_id,
                                   res3.column_id,
                                   res3.parent_id,
                                   res3.name AS name,
                                   columns.name AS pname,
                                   tname,
                                   surname,
                                   fathername
                            FROM
                              (SELECT calc_id,
                                      val,
                                      UUID,
                                      group_id,
                                      group_type,
                                      teacher_id,
                                      column_id,
                                      parent_id,
                                      name,
                                      tname,
                                      surname,
                                      fathername
                               FROM
                                 (SELECT calc_id,
                                         sum(value) AS val,
                                         UUID,
                                         group_id,
                                         group_type,
                                         teacher_id,
                                         cc.column_id,
                                         tname,
                                         surname,
                                         fathername
                                  FROM
                                    (SELECT DISTINCT loadcolumns.id,
                                                     calc_id,
                                                     loadcolumns.budzhet,
                                                     value,
                                                     loadcolumns.uuid,
                                                     loadcolumns.group_id,
                                                     loadcolumns.group_type,
                                                     loadcolumns.teacher_id,
                                                     teachers.name AS tname,
                                                     teachers.surname,
                                                     teachers.fathername
                                     FROM loadstrokes AS ls,
                                          loadcolumns
                                     LEFT JOIN teachers ON loadcolumns.teacher_id=teachers.id
                                     WHERE load_version=:ver
                                       AND value<>0.0000
                                       AND loadcolumns.uuid=ls.uuid
                                       AND loadcolumns.group_id=ls.group_id
                                       AND loadcolumns.group_type=ls.group_type
                                       AND ls.department=:dep
                                       AND ls.load_id in
                                         (SELECT id
                                          FROM LOAD
                                          WHERE VERSION=:ver)) AS res
                                  LEFT JOIN columns_calc AS cc ON cc.id=res.calc_id
                                  GROUP BY calc_id,
                                           UUID,
                                           group_id,
                                           group_type,
                                           teacher_id,
                                           cc.column_id,
                                           tname,
                                           surname,
                                           fathername) AS res2
                               INNER JOIN columns ON columns.id=res2.column_id) AS res3
                            LEFT JOIN columns ON columns.id=res3.parent_id;
                            """, {"dep": dep, "ver": ver.version})

    cols = {}

    for row in colsvalues:
        key = "{}_{}_{}".format(row.uuid, row.group_type, row.group_id)

        if key not in cols:
            cols[key] = []    

        cols[key].append({"calc_id": row.calc_id, "name": row.name, 
                          "pname": row.pname, "val": row.val.normalize(),
                          "group_id": row.group_id, "group_type": row.group_type,
                          "uuid": row.uuid, "teacher_id": row.teacher_id,
                          "tname": row.tname,
                          "tsurname": row.surname, "tfathername": row.fathername
                          })

    types_hours = {"lek": "Лекции", "pract": "Практики", "lab": "Лабораторные"}    

    for (i,key) in enumerate(strokes):
        teachers_ids = []
        teachers = []
        summ_all = 0
        summ_derivated = 0

        load_fact_hours = []
        column_hours = []
        types = []

        teacher = {}
        count_students = 0
        bc = 0
        vbc = 0
        strcommon = {}
        size = len(strokes[key]) - 1
        strs = strokes[key]

        key_child = 0

        for idx, st in enumerate(strokes[key]):
            teacher = {"teacher_id": st["teacher_id"],
                       "tname": st["tname"],
                       "tsurname": st["tsurname"],
                       "tfathername": st["tfathername"]}
            if (st["teacher_id"] >= 0):      
                if (st["teacher_id"] not in teachers_ids):
                    teachers.append(teacher)
                    teachers_ids.append(st["teacher_id"])
                summ_derivated += st["count_hours"]

            if not "other" in st["hours_type"]:
                summ_all += st["count_hours"]
                if st["count_hours"] > 0: 
                    teachers_n = ""
                    if (teacher["teacher_id"] >= 0):
                        teachers_n = "{} {}.{}.".format(teacher["tsurname"], teacher["tname"][0], teacher["tfathername"][0])
                    column_hours.append({"key": "{}-{}".format(i, key_child), "data": {"name": types_hours[st["hours_type"]], 
                                                                                          "cs": "",
                                                                                          "count_students": "",
                                                                                          "group_name": "",
                                                                                          "hours_type": st["hours_type"], 
                                                                                          "count_hours": st["count_hours"],
                                                                                          "group_id": st["group_id"], 
                                                                                          "group_type": st["group_type"],
                                                                                          "uuid": st["uuid"],
                                                                                          "hours_derivared": "", 
                                                                                          "summ_all": st["count_hours"],
                                                                                          "teacher": teacher,
                                                                                          "teachers": teachers_n,
                                                                                          "load_version": ver.version,
                                                                                          "object_type": st["object_type"]},
                                        "styleClass": "distributed" if teacher["teacher_id"] >= 0 else ""})
            else:
                if (st["object_type"] not in [2, 3, 31, 6]):
                    types.append({"hours_type": st["hours_type"], "teacher": teacher})

            if (st["object_type"] in (3, 31, 6)):
                count_students += st["count_students"]
                bc += st["bc"]
                vbc += st["vc"]

            if (idx == size):
                if (not st["object_type"] in (3, 31, 6)):
                    count_students = st["count_students"]
                    bc = st["bc"]
                    vbc = st["vc"]
                strcommon = st 

            key_child+=1    


        if key in cols:
            for col in cols[key]:
                name = col["name"]
                if col["pname"]:
                    name = "{} / {}".format(col["pname"], col["name"])
                teacher = {"teacher_id": col["teacher_id"],
                           "tname": col["tname"],
                           "tsurname": col["tsurname"],
                           "tfathername": col["tfathername"]}
                teachers_n = ""
                if (teacher["teacher_id"] >= 0):
                    teachers_n = "{} {}.{}.".format(teacher["tsurname"], teacher["tname"][0], teacher["tfathername"][0])           

                if (col["teacher_id"] >= 0): 
                    if (not col["teacher_id"] in teachers_ids):     
                        teachers.append(teacher)
                        teachers_ids.append(col["teacher_id"])
                    summ_derivated += col["val"]

                summ_all += col["val"]

                if strcommon["object_type"] != 2:
                    st = [x for x in strs if x["teacher_id"]==col["teacher_id"]]
                    if len(st)==0:
                        st = [strs[0]]

                    if (not strcommon["object_type"] in (3, 31, 6) and len(types) > 0):
                        ts = [x for x in types if x["teacher"]["teacher_id"]==col["teacher_id"]]
                        
                        if (len(ts) > 0):
                            val = col["val"] / len(ts)
                            for row in ts:
                                teachers_n2 = ""
                                if (row["teacher"]["teacher_id"] >= 0):
                                    teachers_n2 = "{} {}.{}.".format(row["teacher"]["tsurname"], row["teacher"]["tname"][0], row["teacher"]["tfathername"][0])
                                column_hours.append({"key": "{}-{}".format(i, key_child), "data":{"calc_id": col["calc_id"], 
                                                                                                 "name": name,
                                                                                                 "count_students": "",
                                                                                                 "group_name": "",
                                                                                                 "count_hours": val, 
                                                                                                 "uuid": col["uuid"], 
                                                                                                 "group_id": col["group_id"],
                                                                                                 "group_type": col["group_type"],
                                                                                                 "teacher": row["teacher"], 
                                                                                                 "bc": st[0]["bc"], "vbc": st[0]["vc"],
                                                                                                 "hours_derivared": "", 
                                                                                                 "summ_all": val,
                                                                                                 "teachers": teachers_n2,
                                                                                                 "hours_type": row["hours_type"],
                                                                                                 "load_version": ver.version,
                                                                                                 "object_type": strcommon["object_type"]},
                                                     "styleClass": "distributed" if row["teacher"]["teacher_id"] >= 0 else ""})
                    else:
                        column_hours.append({"key": "{}-{}".format(i, key_child), "data": {"calc_id": col["calc_id"],
                                             "name": name,
                                             "count_students": st[0]["bc"]+st[0]["vc"],
                                             "group_name": "",
                                             "count_hours": col["val"], 
                                             "uuid": col["uuid"], 
                                             "group_id": col["group_id"],
                                             "group_type": col["group_type"],
                                             "teacher": teacher, 
                                             "bc": st[0]["bc"], "vbc": st[0]["vc"],
                                             "hours_derivared": "", 
                                             "summ_all": col["val"],
                                             "teachers": teachers_n,
                                             "hours_type": None,
                                             "load_version": ver.version,
                                             "object_type": strcommon["object_type"]},
                                             "styleClass": "distributed" if teacher["teacher_id"] >= 0 else ""})
                else:
                    column_hours.append({"key": "{}-{}".format(i, key_child), "data": {"calc_id": col["calc_id"],
                                         "name": name,
                                         "count_students": "",
                                         "group_name": "",
                                         "count_hours": col["val"], 
                                         "uuid": col["uuid"], 
                                         "group_id": col["group_id"],
                                         "group_type": col["group_type"],
                                         "teacher": teacher,
                                         "bc": strcommon["bc"], "vbc": strcommon["vc"],
                                         "hours_derivared": "", 
                                         "summ_all": col["val"],
                                         "teachers": teachers_n,
                                         "hours_type": None,
                                         "load_version": ver.version,
                                         "object_type": strcommon["object_type"]},
                                         "styleClass": "distributed" if teacher["teacher_id"] >= 0 else ""})

                key_child += 1        


        if (summ_all > 0):
            cl = "distributed" if summ_all==summ_derivated else ""
            teacher_n = ""
            if len(teachers) == 1:
                teacher_n = "{} {}.{}.".format(teachers[0]["tsurname"], teachers[0]["tname"][0], teachers[0]["tfathername"][0])
            elif len(teachers) > 1:
                teacher_n = "{} преподавателя(ей)".format(len(teachers))
            strokes_res.append({"key": str(i), "data": {"name": "{} ({})".format(strcommon["name"], strcommon["code"]),
                                                        "cs": "{}/{}".format(strcommon["course"], strcommon["sem"]),
                                                        "bc": bc, "vbc": vbc,
                                                        "count_students": count_students, 
                                                        "group_name": strcommon["group_name"] if strcommon["group_name"] else "", 
                                                        "group_info":  "({}, профиль {})".format(strcommon["shifr"], strcommon["profile_name"]),
                                                        "hours_derivared": summ_derivated, "summ_all": summ_all,
                                                        "teachers": teacher_n,
                                                        "uuid": strcommon["uuid"], "shifr": strcommon["shifr"],
                                                        "group_id": strcommon["group_id"], "group_type": strcommon["group_type"],
                                                        "load_version": ver.version,
                                                        "object_type": strcommon["object_type"]},
                                "styleClass": cl,                        
                                "children": column_hours})

    strokes_res = sorted(strokes_res, key = lambda x: (x["data"]["shifr"], x["data"]["group_name"], 
                                                       x["data"]["cs"], x["data"]["name"]))        

    return strokes_res


def get_dep_teachers(db, department, year):
    faculty = db.query(ami.Department.faculty).\
                 filter(ami.Department.number == department).\
                 first()

    faculty_id = faculty.faculty

    ver = get_approved_version(db, faculty_id, year)

    teachers = []

    if ver:
        teachers = db.query(ami.Teacher).\
                      filter(ami.Teacher.active == True).\
                      filter(ami.Teacher.department == department).\
                      order_by(ami.Teacher.surname).\
                      all()

        for row in teachers:
            info = get_teacher_hours_info(db, row.id, ver.version)
            # info = {"all": 0, "first_sem": 0, "second_sem": 0,
            # "pair_first": 0, "pair_second": 0}
            row.hours = row.part_of_rate*900 
            row.hours_info = info          

    return teachers


def get_teacher_hours_info(db, teacher_id, version):
    sel_all = db.execute("""
                          SELECT coalesce(sum(count_hours), 0)
                            FROM
                              (SELECT count_hours
                               FROM loadstrokes AS ls,
                                    LOAD
                               WHERE load.version=:ver
                                 AND load.id=ls.load_id
                                 AND teacher_id=:tid
                               UNION ALL SELECT value AS count_hours
                               FROM loadcolumns
                               WHERE load_version=:ver
                                 AND teacher_id=:tid) AS res;

                         """, {"ver": version, "tid": teacher_id})
    all_hours = 0
    for row in sel_all:
        all_hours = row[0].normalize()

    first_sem = 0
    second_sem = 0
    sel_first = db.execute("""
                            SELECT coalesce(sum(count_hours), 0) as ch,
                                   sem
                            FROM
                              (SELECT count_hours,
                                      sem % 2 AS sem
                               FROM loadstrokes AS ls,
                                    LOAD
                               WHERE load.version=:ver
                                 AND load.id=ls.load_id
                                 AND teacher_id=:tid

                               UNION ALL SELECT sum(value),
                                                sem % 2 AS sem
                               FROM
                                 (SELECT DISTINCT lc.id,
                                                  value,
                                                  sem,
                                                  lc.uuid
                                  FROM loadcolumns AS lc
                                  LEFT JOIN loadstrokes AS ls ON ls.uuid=lc.uuid
                                  WHERE load_version=:ver
                                    AND lc.teacher_id=:tid
                                    AND ls.load_id in
                                      (SELECT id
                                       FROM LOAD
                                       WHERE VERSION=139)
                             ) AS res2
                               GROUP BY sem) AS res
                            GROUP BY sem;
                           """, {"ver": version, "tid": teacher_id})

    for row in sel_first:
        if row.sem == 1:
            first_sem = row.ch.normalize()
        else:
            second_sem =  row.ch.normalize()   

                           
    sel_second = db.execute("""
                            SELECT coalesce(sum(count_hours), 0) as ch,
                                   sem
                            FROM
                              (SELECT ls.name,
                                      (count_hours / 2.0 / hours) AS count_hours,
                                      hours,
                                      ls.sem % 2 AS sem
                               FROM loadstrokes AS ls,
                                    lessonplanstroke AS lp,
                                    LOAD,
                                    timetable AS tt
                               WHERE load.version=:ver
                                 AND load.id=ls.load_id
                                 AND teacher_id=:tid
                                 AND ls.sem % 2 = tt.sem % 2
                                 AND tt.lessonplan_id=lp.lessonplan_id
                                 AND ls.hours_type in ('lek',
                                                       'lab',
                                                       'pract')
                                 AND lp.uuid=ls.uuid ) AS res
                            GROUP BY sem;
                           """, {"ver": version, "tid": teacher_id})
    pair_second = 0
    pair_first = 0
    for row in sel_second:
        if row.sem == 1:
            pair_first = row.ch.normalize()
        else:    
            pair_second = row.ch.normalize()
                           

    sel_types = db.execute("""
                            SELECT hours_type,
                                   sum(count_hours) AS ch,
                                   sum(v) AS v,
                                   object_type
                            FROM
                              (SELECT ls.uuid,
                                      hours_type,
                                      count_hours,
                                      object_type,
                                      sum(value) AS v
                               FROM LOAD,
                                    loadstrokes AS ls
                               LEFT JOIN loadcolumns AS lc ON ls.uuid=lc.uuid
                               AND ls.group_type=lc.group_type
                               AND lc.group_id=ls.group_id
                               AND lc.budzhet=ls.budzhet
                               WHERE load.version=:ver
                                 AND load.id=ls.load_id
                                 AND ls.teacher_id=:tid
                                 AND lc.teacher_id=:tid
                               GROUP BY hours_type,
                                        object_type,
                                        count_hours,
                                        ls.uuid) AS res
                            GROUP BY hours_type,
                                     object_type;
                           """, {"ver": version, "tid": teacher_id})

    types_dict = {"lek": 0, "pract": 0, "lab": 0, "allaud": 0, 
                  "allvneaud": 0, "practice": 0, "gak": 0, "other": 0, "all": 0, "gia": 0,
                  "procaud": 0, "proclek": 0, "proclab": 0, "procpract": 0, "procvne": 0}

    for row in sel_types:
        if row.object_type == 2:
            types_dict[row.hours_type] += row.ch
            types_dict["allaud"] += row.ch
            types_dict["allvneaud"] += row.v
            types_dict["all"] += row.ch
            types_dict["all"] += row.v
        elif row.object_type == 32:
            types_dict["gak"] += row.v
            types_dict["all"] += row.v
        elif row.object_type == 6:
            types_dict["gia"] += row.v
            types_dict["all"] += row.v
        elif row.object_type in (3, 31):
            types_dict["practice"] += row.v
            types_dict["all"] += row.v
        else:
            types_dict["other"] += row.v
            types_dict["all"] += row.v

    types_dict["allvneaud"] += (all_hours-types_dict["all"])
    if all_hours > 0:
        types_dict["procaud"] = "{:.2f}".format((types_dict["allaud"]+types_dict["allvneaud"])/all_hours*100)
    all_aud =  types_dict["allaud"]+types_dict["allvneaud"]
    if all_aud > 0:
        types_dict["proclek"] = "{:.2f}".format((types_dict["lek"])/all_aud*100)
        types_dict["proclab"] = "{:.2f}".format((types_dict["lab"])/all_aud*100)
        types_dict["procpract"] = "{:.2f}".format((types_dict["pract"])/all_aud*100)
        types_dict["procvne"] = "{:.2f}".format((types_dict["allvneaud"])/all_aud*100)
                                                                                                                   

    return {"all": all_hours, "first_sem": first_sem, "second_sem": second_sem,
            "pair_first": pair_first, "pair_second": pair_second, "types": types_dict}

    return {"all": all_hours, "first_sem": 0, "second_sem": 0,
            "pair_first": 0, "pair_second": 0}        


def get_one_stroke(db, uuid, group_type, group_id, load_version, keyst):

    selbase = db.execute("""
                            SELECT sum(count_students) AS cs,
                           sum(count_hours) AS ch,
                           UUID,
                           load_id,
                           code,
                           res.name,
                           sem,
                           res.department,
                           teacher_id,
                           group_id,
                           group_type,
                           alias_name,
                           hours_type,
                           object_type,
                           flow_id,
                           res.profile_id,
                           res.course,
                           gp.name AS groupname,
                           subgroups.name AS sname,
                           profiles_full.dir_name,
                           profiles_full.qualification,
                           profiles_full.shifr,
                           profiles_full.forma,
                           profiles_full.profile_name,
                           teachers.name AS tname,
                           teachers.surname,
                           teachers.fathername,
                           sum(bc) AS bc,
                           sum(vc) AS vc
                    FROM
                      (SELECT ls.id,
                              UUID,
                              load.profile_id,
                              load_id,
                              load.course,
                              code,
                              ls.name,
                              count_students,
                              sem,
                              department,
                              teacher_id,
                              group_id,
                              group_type,
                              budzhet,
                              alias_name,
                              count_hours,
                              hours_type,
                              object_type,
                              flow_id,
                              (CASE
                                   WHEN ls.budzhet=TRUE THEN ls.count_students
                                   ELSE 0
                               END) AS bc,
                              (CASE
                                   WHEN ls.budzhet=FALSE THEN ls.count_students
                                   ELSE 0
                               END) AS vc
                       FROM loadstrokes AS ls,
                            LOAD
                       WHERE load.id=ls.load_id
                         AND load.version=:ver
                         AND active=TRUE
                         AND current_faculty=TRUE
                         AND uuid=:uuid
                         AND group_id=:group_id
                         AND group_type=:group_type) AS res
                    LEFT JOIN groups_profile AS gp ON res.group_id=gp.id
                    AND res.group_type='profile'
                    LEFT JOIN subgroups ON res.group_id=subgroups.id
                    AND res.group_type='subgroup'
                    LEFT JOIN profiles_full ON res.profile_id=profiles_full.profile_id
                    LEFT JOIN teachers ON res.teacher_id=teachers.id
                    GROUP BY UUID,
                             load_id,
                             code,
                             res.name,
                             sem,
                             res.department,
                             teacher_id,
                             group_id,
                             group_type,
                             alias_name,
                             hours_type,
                             object_type,
                             flow_id,
                             res.profile_id,
                             res.course,
                             gp.name,
                             subgroups.name,
                             profiles_full.dir_name,
                             profiles_full.qualification,
                             profiles_full.shifr,
                             profiles_full.forma,
                             profiles_full.profile_name,
                             teachers.name,
                             teachers.surname,
                             teachers.fathername;
                        """, {"ver": load_version, "uuid": uuid, "group_type": group_type, "group_id": group_id})

    strokes = {}
    strokes_res = []

    for row in selbase:
        key = "{}_{}_{}".format(row.uuid, row.group_type, row.group_id)

        if key not in strokes:
            strokes[key] = []

        group_name = ""     
        if (row.group_type == "profile"):
            group_name = row.groupname
        elif (row.group_type == "subgroup"):
            group_name = row.sname  

        strokes[key].append({"uuid": row.uuid, "code": row.code,
                             "name": row.name, "alias_name": row.alias_name,
                             "course": row.course, "sem": row.sem,
                             "count_students": row.cs, "bc": row.bc,
                             "vc": row.vc, "count_hours": row.ch,
                             "hours_type": row.hours_type, "object_type": row.object_type,
                             "group_id": row.group_id, "group_type": row.group_type,
                             "group_name": group_name, "dir_name": row.dir_name,
                             "profile_name": row.profile_name, "shifr": row.shifr,
                             "qual": row.qualification, "form": row.forma,
                             "teacher_id": row.teacher_id, "tname": row.tname,
                             "tsurname": row.surname, "tfathername": row.fathername
                             })

    colsvalues = db.execute("""
                            SELECT calc_id,
                                   val,
                                   UUID,
                                   group_id,
                                   group_type,
                                   teacher_id,
                                   res3.column_id,
                                   res3.parent_id,
                                   res3.name AS name,
                                   columns.name AS pname,
                                   tname,
                                   surname,
                                   fathername
                            FROM
                              (SELECT calc_id,
                                      val,
                                      UUID,
                                      group_id,
                                      group_type,
                                      teacher_id,
                                      column_id,
                                      parent_id,
                                      name,
                                      tname,
                                      surname,
                                      fathername
                               FROM
                                 (SELECT calc_id,
                                         sum(value) AS val,
                                         UUID,
                                         group_id,
                                         group_type,
                                         teacher_id,
                                         cc.column_id,
                                         tname,
                                         surname,
                                         fathername
                                  FROM
                                    (SELECT DISTINCT loadcolumns.id,
                                                     calc_id,
                                                     loadcolumns.budzhet,
                                                     value,
                                                     loadcolumns.uuid,
                                                     loadcolumns.group_id,
                                                     loadcolumns.group_type,
                                                     loadcolumns.teacher_id,
                                                     teachers.name AS tname,
                                                     teachers.surname,
                                                     teachers.fathername
                                     FROM loadstrokes AS ls,
                                          loadcolumns
                                     LEFT JOIN teachers ON loadcolumns.teacher_id=teachers.id
                                     WHERE load_version=:ver
                                       AND value<>0.0000
                                       AND loadcolumns.uuid=ls.uuid
                                       AND loadcolumns.group_id=ls.group_id
                                       AND loadcolumns.group_type=ls.group_type
                                       AND ls.uuid=:uuid
                                       AND ls.group_id=:group_id
                                       AND ls.group_type=:group_type
                                       AND ls.load_id in
                                         (SELECT id
                                          FROM LOAD
                                          WHERE VERSION=:ver)) AS res
                                  LEFT JOIN columns_calc AS cc ON cc.id=res.calc_id
                                  GROUP BY calc_id,
                                           UUID,
                                           group_id,
                                           group_type,
                                           teacher_id,
                                           cc.column_id,
                                           tname,
                                           surname,
                                           fathername) AS res2
                               INNER JOIN columns ON columns.id=res2.column_id) AS res3
                            LEFT JOIN columns ON columns.id=res3.parent_id;
                            """, {"ver": load_version, "uuid": uuid, "group_type": group_type, "group_id": group_id})

    cols = {}

    for row in colsvalues:
        key = "{}_{}_{}".format(row.uuid, row.group_type, row.group_id)

        if key not in cols:
            cols[key] = []    

        cols[key].append({"calc_id": row.calc_id, "name": row.name, 
                          "pname": row.pname, "val": row.val.normalize(),
                          "group_id": row.group_id, "group_type": row.group_type,
                          "uuid": row.uuid, "teacher_id": row.teacher_id,
                          "tname": row.tname,
                          "tsurname": row.surname, "tfathername": row.fathername
                          })

    types_hours = {"lek": "Лекции", "pract": "Практики", "lab": "Лабораторные"}    

    for (i,key) in enumerate(strokes):
        teachers_ids = []
        teachers = []
        summ_all = 0
        summ_derivated = 0

        load_fact_hours = []
        column_hours = []
        types = []

        teacher = {}
        count_students = 0
        bc = 0
        vbc = 0
        strcommon = {}
        size = len(strokes[key]) - 1
        strs = strokes[key]

        key_child = 0

        for idx, st in enumerate(strokes[key]):
            teacher = {"teacher_id": st["teacher_id"],
                       "tname": st["tname"],
                       "tsurname": st["tsurname"],
                       "tfathername": st["tfathername"]}
            if (st["teacher_id"] >= 0):      
                if (st["teacher_id"] not in teachers_ids):
                    teachers.append(teacher)
                    teachers_ids.append(st["teacher_id"])
                summ_derivated += st["count_hours"]

            if not "other" in st["hours_type"]:
                summ_all += st["count_hours"]
                if st["count_hours"] > 0: 
                    teachers_n = ""
                    if (teacher["teacher_id"] >= 0):
                        teachers_n = "{} {}.{}.".format(teacher["tsurname"], teacher["tname"][0], teacher["tfathername"][0])
                    column_hours.append({"key": "{}-{}".format(keyst, key_child), "data": {"name": types_hours[st["hours_type"]], 
                                                                                          "cs": "",
                                                                                          "count_students": "",
                                                                                          "group_name": "",
                                                                                          "hours_type": st["hours_type"], 
                                                                                          "count_hours": st["count_hours"],
                                                                                          "group_id": st["group_id"], 
                                                                                          "group_type": st["group_type"],
                                                                                          "uuid": st["uuid"],
                                                                                          "hours_derivared": "", 
                                                                                          "summ_all": st["count_hours"],
                                                                                          "teacher": teacher,
                                                                                          "teachers": teachers_n,
                                                                                          "load_version": load_version,
                                                                                          "object_type": st["object_type"]},
                                                                                          "loading": False,
                                        "styleClass": "distributed" if teacher["teacher_id"] >= 0 else ""})
            else:
                if (st["object_type"] not in [2, 3, 31, 6]):
                    types.append({"hours_type": st["hours_type"], "teacher": teacher})

            if (st["object_type"] in (3, 31, 6)):
                count_students += st["count_students"]
                bc += st["bc"]
                vbc += st["vc"]

            if (idx == size):
                if (not st["object_type"] in (3, 31, 6)):
                    count_students = st["count_students"]
                    bc = st["bc"]
                    vbc = st["vc"]
                strcommon = st 

            key_child+=1    


        if key in cols:
            for col in cols[key]:
                name = col["name"]
                if col["pname"]:
                    name = "{} / {}".format(col["pname"], col["name"])
                teacher = {"teacher_id": col["teacher_id"],
                           "tname": col["tname"],
                           "tsurname": col["tsurname"],
                           "tfathername": col["tfathername"]}
                teachers_n = ""
                if (teacher["teacher_id"] >= 0):
                    teachers_n = "{} {}.{}.".format(teacher["tsurname"], teacher["tname"][0], teacher["tfathername"][0])           

                if (col["teacher_id"] >= 0): 
                    if (not col["teacher_id"] in teachers_ids):     
                        teachers.append(teacher)
                        teachers_ids.append(col["teacher_id"])
                    summ_derivated += col["val"]

                summ_all += col["val"]

                if strcommon["object_type"] != 2:
                    st = [x for x in strs if x["teacher_id"]==col["teacher_id"]]
                    if len(st)==0:
                        st = [strs[0]]

                    if (not strcommon["object_type"] in (3, 31, 6) and len(types) > 0):
                        ts = [x for x in types if x["teacher"]["teacher_id"]==col["teacher_id"]]
                        
                        if (len(ts) > 0):
                            val = col["val"] / len(ts)
                            for row in ts:
                                teachers_n2 = ""
                                if (row["teacher"]["teacher_id"] >= 0):
                                    teachers_n2 = "{} {}.{}.".format(row["teacher"]["tsurname"], row["teacher"]["tname"][0], row["teacher"]["tfathername"][0])
                                column_hours.append({"key": "{}-{}".format(keyst, key_child), "data":{"calc_id": col["calc_id"], 
                                                                                                 "name": name,
                                                                                                 "count_students": "",
                                                                                                 "group_name": "",
                                                                                                 "count_hours": val, 
                                                                                                 "uuid": col["uuid"], 
                                                                                                 "group_id": col["group_id"],
                                                                                                 "group_type": col["group_type"],
                                                                                                 "teacher": row["teacher"], 
                                                                                                 "bc": st[0]["bc"], "vbc": st[0]["vc"],
                                                                                                 "hours_derivared": "", 
                                                                                                 "summ_all": val,
                                                                                                 "teachers": teachers_n2,
                                                                                                 "hours_type": row["hours_type"],
                                                                                                 "load_version": load_version,
                                                                                                 "loading": False,
                                                                                                 "object_type": strcommon["object_type"]},
                                                     "styleClass": "distributed" if row["teacher"]["teacher_id"] >= 0 else ""})
                    else:
                        column_hours.append({"key": "{}-{}".format(keyst, key_child), "data": {"calc_id": col["calc_id"],
                                             "name": name,
                                             "count_students": st[0]["bc"]+st[0]["vc"],
                                             "group_name": "",
                                             "count_hours": col["val"], 
                                             "uuid": col["uuid"], 
                                             "group_id": col["group_id"],
                                             "group_type": col["group_type"],
                                             "teacher": teacher, 
                                             "bc": st[0]["bc"], "vbc": st[0]["vc"],
                                             "hours_derivared": "", 
                                             "summ_all": col["val"],
                                             "teachers": teachers_n,
                                             "hours_type": None,
                                             "load_version": load_version,
                                             "object_type": strcommon["object_type"]},
                                             "styleClass": "distributed" if teacher["teacher_id"] >= 0 else "",
                                             "loading": False})
                else:
                    column_hours.append({"key": "{}-{}".format(keyst, key_child), "data": {"calc_id": col["calc_id"],
                                         "name": name,
                                         "count_students": "",
                                         "group_name": "",
                                         "count_hours": col["val"], 
                                         "uuid": col["uuid"], 
                                         "group_id": col["group_id"],
                                         "group_type": col["group_type"],
                                         "teacher": teacher,
                                         "bc": strcommon["bc"], "vbc": strcommon["vc"],
                                         "hours_derivared": "", 
                                         "summ_all": col["val"],
                                         "teachers": teachers_n,
                                         "hours_type": None,
                                         "load_version": load_version,
                                         "object_type": strcommon["object_type"]},
                                         "styleClass": "distributed" if teacher["teacher_id"] >= 0 else "",
                                         "loading": False})

                key_child += 1        


        if (summ_all > 0):
            cl = "distributed" if summ_all==summ_derivated else ""
            teacher_n = ""
            if len(teachers) == 1:
                teacher_n = "{} {}.{}.".format(teachers[0]["tsurname"], teachers[0]["tname"][0], teachers[0]["tfathername"][0])
            elif len(teachers) > 1:
                teacher_n = "{} преподавателя(ей)".format(len(teachers))
            strokes_res.append({"key": str(keyst), "data": {"name": "{} ({})".format(strcommon["name"], strcommon["code"]), 
                                                            "cs": "{}/{}".format(strcommon["course"], strcommon["sem"]),
                                                            "bc": bc, "vbc": vbc,
                                                            "count_students": count_students, 
                                                            "group_name":  "{} ({}, профиль {})".format(strcommon["group_name"], strcommon["shifr"], strcommon["profile_name"]),
                                                            "hours_derivared": summ_derivated, "summ_all": summ_all,
                                                            "teachers": teacher_n,
                                                            "uuid": strcommon["uuid"], "shifr": strcommon["shifr"],
                                                            "group_id": strcommon["group_id"], "group_type": strcommon["group_type"],
                                                            "load_version": load_version, "object_type": strcommon["object_type"],
                                                            "loading": False},
                                "styleClass": cl,                        
                                "children": column_hours})

    return strokes_res[0]


def set_teacher_on_all_stroke(db, uuid, group_type, group_id, teacher_id, load_version, department, key):

    ver = db.query(ami.LoadVersion).get({"id":load_version})

    ls_ids = db.query(ami.LoadStroke.id).\
                join(ami.Load).\
                filter(ami.Load.version == load_version).\
                filter(ami.LoadStroke.uuid == uuid).\
                filter(ami.LoadStroke.group_type == group_type).\
                filter(ami.LoadStroke.group_id == group_id).\
                all()
    lss_ids = [x[0] for x in ls_ids]

    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(lss_ids)).\
           values(teacher_id=teacher_id).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    stmt = update(ami.LoadColumn).\
           where(ami.LoadColumn.uuid==uuid).\
           where(ami.LoadColumn.load_version==load_version).\
           where(ami.LoadColumn.group_type==group_type).\
           where(ami.LoadColumn.group_id==group_id).\
           values(teacher_id=teacher_id).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return {"stroke": get_one_stroke(db, uuid, group_type, group_id, load_version, key),
            "teachers": get_dep_teachers(db, department, ver.year)}


def set_teacher_on_load_part_stroke(db, uuid, group_type, group_id, teacher_id, 
                                    load_version, department, key, hours_type):

    ver = db.query(ami.LoadVersion).get({"id":load_version})

    ls_ids = db.query(ami.LoadStroke.id).\
                join(ami.Load).\
                filter(ami.Load.version == load_version).\
                filter(ami.LoadStroke.uuid == uuid).\
                filter(ami.LoadStroke.group_type == group_type).\
                filter(ami.LoadStroke.group_id == group_id).\
                filter(ami.LoadStroke.hours_type == hours_type).\
                all()
    lss_ids = [x[0] for x in ls_ids]

    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(lss_ids)).\
           values(teacher_id=teacher_id).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return {"stroke": get_one_stroke(db, uuid, group_type, group_id, load_version, key),
            "teachers": get_dep_teachers(db, department, ver.year)}


def set_teacher_on_calc_part_stroke(db, uuid, group_type, group_id, teacher_id, 
                                    load_version, department, key, calc_id, hours_type=None):

    ver = db.query(ami.LoadVersion).get({"id":load_version})

    if (hours_type == None):
        stmt = update(ami.LoadColumn).\
               where(ami.LoadColumn.uuid==uuid).\
               where(ami.LoadColumn.load_version==load_version).\
               where(ami.LoadColumn.group_type==group_type).\
               where(ami.LoadColumn.group_id==group_id).\
               where(ami.LoadColumn.calc_id==calc_id).\
               values(teacher_id=teacher_id).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()

    else:
        ls_ids = db.query(ami.LoadStroke.id).\
                join(ami.Load).\
                filter(ami.Load.version == load_version).\
                filter(ami.LoadStroke.uuid == uuid).\
                filter(ami.LoadStroke.group_type == group_type).\
                filter(ami.LoadStroke.group_id == group_id).\
                filter(ami.LoadStroke.hours_type == hours_type).\
                all()
        lss_ids = [x[0] for x in ls_ids]

        stmt = update(ami.LoadStroke).\
               where(ami.LoadStroke.id.in_(lss_ids)).\
               values(teacher_id=teacher_id).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()

        stmt = delete(ami.LoadColumn).\
               where(ami.LoadColumn.uuid==uuid).\
               where(ami.LoadColumn.group_type==group_type).\
               where(ami.LoadColumn.group_id==group_id).\
               where(ami.LoadColumn.load_version==load_version).\
               where(ami.LoadColumn.hand==False).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

        acl.load_calc_for_one(db, uuid, load_version)
            

    return {"stroke": get_one_stroke(db, uuid, group_type, group_id, load_version, key),
            "teachers": get_dep_teachers(db, department, ver.year)}


def set_teacher_on_part_of_practice(db, uuid, group_type, group_id, teacher_id, 
                                    load_version, department, key, bc, vc):

    ver = db.query(ami.LoadVersion).get({"id":load_version})

    sel_st = db.query(ami.LoadStroke).\
                join(ami.Load).\
                filter(ami.Load.version == load_version).\
                filter(ami.LoadStroke.uuid == uuid).\
                filter(ami.LoadStroke.group_id == group_id).\
                filter(ami.LoadStroke.group_type == group_type).\
                filter(ami.LoadStroke.teacher_id == -1).\
                all()

    for stroke in sel_st:
        cs = bc if stroke.budzhet else vc
        st = ami.LoadStroke(uuid=stroke.uuid,
                            load_id=stroke.load_id,
                            code=stroke.code,
                            name=stroke.name,
                            sem=stroke.sem,
                            budzhet=stroke.budzhet,
                            department=stroke.department,
                            choice=stroke.choice,
                            count_students=cs,
                            count_groups=0,
                            teacher_id=teacher_id,
                            group_id=stroke.group_id,
                            group_type=stroke.group_type,
                            alias_name=stroke.alias_name,
                            count_hours=0,
                            hours_type=stroke.hours_type,
                            is_own=stroke.is_own,
                            active=True,
                            current_faculty=stroke.current_faculty,
                            add_inf=stroke.add_inf,
                            object_type=stroke.object_type,
                            flow_id=stroke.flow_id,
                            count_foreign=0,
                            show=stroke.show)

        db.add(st)
        db.commit()
        db.refresh(st)

        stmt = update(ami.LoadStroke).\
               where(ami.LoadStroke.uuid==stroke.uuid).\
               where(ami.LoadStroke.group_type==stroke.group_type).\
               where(ami.LoadStroke.group_id==stroke.group_id).\
               where(ami.LoadStroke.budzhet==stroke.budzhet).\
               where(ami.LoadStroke.teacher_id==-1).\
               where(ami.LoadStroke.load_id==stroke.load_id).\
               values(count_students=(stroke.count_students - cs)).\
               execution_options(synchronize_session="fetch")
        res = db.execute(stmt)
        db.commit()

    acl.load_calc_for_one(db, uuid, load_version)           

    return {"stroke": get_one_stroke(db, uuid, group_type, group_id, load_version, key),
            "teachers": get_dep_teachers(db, department, ver.year)}


def delete_teacher_on_part_of_practice(db, uuid, group_type, group_id, teacher_id, 
                                       load_version, department, key):

    ver = db.query(ami.LoadVersion).get({"id":load_version})

    if (teacher_id > 0):
        sel_st = db.query(ami.LoadStroke).\
                    join(ami.Load).\
                    filter(ami.Load.version == load_version).\
                    filter(ami.LoadStroke.uuid == uuid).\
                    filter(ami.LoadStroke.group_id == group_id).\
                    filter(ami.LoadStroke.group_type == group_type).\
                    filter(ami.LoadStroke.teacher_id == teacher_id).\
                    all()
    else:
        sel_st = db.query(ami.LoadStroke).\
                    join(ami.Load).\
                    filter(ami.Load.version == load_version).\
                    filter(ami.LoadStroke.uuid == uuid).\
                    filter(ami.LoadStroke.group_id == group_id).\
                    filter(ami.LoadStroke.group_type == group_type).\
                    filter(ami.LoadStroke.teacher_id != -1).\
                    all()
                    

    for stroke in sel_st:
        cs = stroke.count_students

        stmt = update(ami.LoadStroke).\
               where(ami.LoadStroke.uuid==stroke.uuid).\
               where(ami.LoadStroke.group_type==stroke.group_type).\
               where(ami.LoadStroke.group_id==stroke.group_id).\
               where(ami.LoadStroke.budzhet==stroke.budzhet).\
               where(ami.LoadStroke.teacher_id==-1).\
               where(ami.LoadStroke.load_id==stroke.load_id).\
               values(count_students = ami.LoadStroke.count_students + cs).\
               execution_options(synchronize_session="fetch")
        res = db.execute(stmt)
        db.commit()

        stmt = delete(ami.LoadStroke).\
               where(ami.LoadStroke.id==stroke.id).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

        stmt = delete(ami.LoadColumn).\
               where(ami.LoadColumn.uuid==stroke.uuid).\
               where(ami.LoadColumn.group_type==stroke.group_type).\
               where(ami.LoadColumn.group_id==stroke.group_id).\
               where(ami.LoadColumn.budzhet==stroke.budzhet).\
               where(ami.LoadColumn.teacher_id==stroke.teacher_id).\
               where(ami.LoadColumn.load_version==load_version).\
               where(ami.LoadColumn.hand==False).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

    acl.load_calc_for_one(db, uuid, load_version)           

    return {"stroke": get_one_stroke(db, uuid, group_type, group_id, load_version, key),
            "teachers": get_dep_teachers(db, department, ver.year)}                                   



def get_teacher_report(db, teacher_id, dep, year):
    faculty = db.query(ami.Department.faculty).\
                 filter(ami.Department.number == dep).\
                 first()

    faculty_id = faculty.faculty

    vh = get_approved_version(db, faculty_id, year)
    ver = db.query(ami.LoadVersion).get({"id":vh.version})
    teacher = acd.get_teacher(db, teacher_id)
    department = acd.get_department_by_number(db, dep)

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})

    cols = acld.get_table_columns(db, faculty_id, vh.version, False)
    load = get_table_load(db, vh.version, dep, teacher_id)

    worksheet = workbook.add_worksheet("Бюджет")
    add_data_to_worksheet(workbook, worksheet, cols, load, teacher, ver, department, True) 

    worksheet = workbook.add_worksheet("Внебюджет")
    add_data_to_worksheet(workbook, worksheet, cols, load, teacher, ver, department, False)               

    workbook.close()
    output.seek(0)

    headers = {'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
               'Content-Disposition': 'attachment; filename=reportfive.xlsx'}
    return Response(content=output.getvalue(), status_code=200, headers=headers)


def get_table_load(db: Session, version: int, dep: int, teacher_id: int):
    ver = db.query(ami.LoadVersion).get({"id":version})

    columnvaluesres = db.execute("""
                                 SELECT UUID,
                                        c_id,
                                        department,
                                        val,
                                        coalesce(formula_text, '') AS formula,
                                        budzhet,
                                        parent_id,
                                        coalesce(hand, FALSE) AS hand
                                 FROM
                                   (SELECT res.uuid,
                                           c_id,
                                           department,
                                           coalesce(sum(value), 0.0000) AS val,
                                           min(cc_id) AS cc_id,
                                           res.budzhet,
                                           res.parent_id,
                                           bool_or(hand) as hand
                                    FROM
                                      (SELECT DISTINCT ls.group_id,
                                                       ls.group_type,
                                                       ls.uuid,
                                                       c.id AS c_id,
                                                       coalesce(c.parent_id, c.id) AS parent_id,
                                                       ls.department,
                                                       ls.budzhet
                                       FROM loadstrokes AS ls,
                                            columns AS c,
                                            LOAD
                                       WHERE load.id=ls.load_id
                                         AND load.version=:ver
                                         AND ls.active=TRUE
                                         AND ls.department=:dep
                                         AND c.active=TRUE
                                         AND c.active_view=TRUE
                                         AND c.year=:year
                                         AND (c.id not in
                                                (SELECT parent_id
                                                 FROM columns
                                                 WHERE parent_id IS NOT NULL)) ) AS res
                                    LEFT JOIN
                                      (SELECT group_id,
                                              group_type,
                                              budzhet,
                                              UUID,
                                              value,
                                              column_id,
                                              cc.id AS cc_id,
                                              lc.hand
                                       FROM loadcolumns AS lc,
                                            columns_calc AS cc
                                       WHERE lc.calc_id=cc.id
                                         AND lc.load_version=:ver
                                         AND lc.teacher_id=:tid) AS res2 ON res2.column_id=res.c_id
                                    AND res.uuid=res2.uuid
                                    AND res.group_id=res2.group_id
                                    AND res.group_type=res2.group_type
                                    AND res.budzhet=res2.budzhet
                                    GROUP BY res.uuid,
                                             c_id,
                                             department,
                                             res.budzhet,
                                             res.parent_id
                                             ) AS res3
                                 LEFT JOIN columns_calc AS cc ON cc.id=cc_id
                                 ORDER BY UUID,
                                          budzhet,
                                          parent_id,
                                          c_id;
                                 """, {'ver': version, 'year': ver.year, 'tid': teacher_id, 'dep':dep})

    column_values_dict = {}

    for row in columnvaluesres:
        if row["uuid"] not in column_values_dict:
            column_values_dict[row["uuid"]] = {"strokes": [], "True": 0, "False": 0}
        column_values_dict[row["uuid"]]["strokes"].append({"c_id": row["c_id"], "department": row["department"],
                                                           "val": row["val"].normalize(), "formula": row["formula"],
                                                           "budzhet": row["budzhet"], "parent_id": row["parent_id"],
                                                           "hand": row["hand"], "uuid": row["uuid"]})
        column_values_dict[row["uuid"]][str(row["budzhet"])] += row["val"].normalize()    

    profiles = db.query(ami.Profile).\
                  join(ami.Load).\
                  join(ami.Direction).\
                  filter(ami.Load.version == version).\
                  order_by(ami.Load.own_faculty.desc(), ami.Direction.form_id, ami.Direction.shifr)


    profiles = profiles.all()                                           

    load = {} 
    directions = {}              

    for row in profiles:
        direction = row.direction
        if (direction.id not in directions):
            directions[direction.id] = {"id": direction.id,
                                        "name": direction.name,
                                        "shifr": direction.shifr,
                                        "faculty_id": direction.faculty_id,
                                        "form_id": direction.form_id,
                                        "profiles": {},
                                        "show_profiles": True,
                                        "nb": 0,
                                        "nvb": 0}

        if row.id not in directions[direction.id]["profiles"]:
            lsb = get_load_strokes(db, True, row.id, version, teacher_id, column_values_dict, dep)
            if len(lsb) > 0:
                directions[direction.id]["nb"] += 1
            lsvb = get_load_strokes(db, False, row.id, version, teacher_id, column_values_dict, dep)
            if len(lsvb) > 0:
                directions[direction.id]["nvb"] += 1
            directions[direction.id]["profiles"][row.id] = {"name": row.name,
                                                            "id": row.id,
                                                            "loadstrokes_budzhet": lsb,
                                                            "loadstrokes_vnebudzhet": lsvb,
                                                            "show_budzhet": True,
                                                            "show_vnebudzhet": True,
                                                            "show_strokes": True}

        faculty = row.direction.faculty
        if (faculty.id not in load):
            faculty_inf = acd.get_faculty_by_id(db, faculty.id, ver.year)
            load[faculty.id] = {"id": faculty.id,
                                "name": faculty_inf.name,
                                "forms": {},
                                "show_forms": True}
            
        form = row.direction.form
        if (form.value not in load[faculty.id]["forms"]):
            load[faculty.id]["forms"][form.value] = {"id": form.value,
                                                     "name": form.name,
                                                     "directions": [],
                                                     "show_dirs": True}


    for d in directions:
        dn = directions[d]
        load[dn["faculty_id"]]["forms"][dn["form_id"]]["directions"].append(dn)

    return load


def get_load_strokes(db, budzhet, profile_id, version_id, teacher_id, colsvalues, dep):
    ls = db.execute("""
                  SELECT course,
                         UUID,
                         code,
                         name,
                         sem,
                         department,
                         choice,
                         sum(count_students) as cs,
                         sum(count_groups) as cg,
                         sum(lecture_fact) as lf,
                         sum(practice_fact) as pf,
                         sum(lab_fact) as labf,
                         is_own,
                         active,
                         add_inf,
                         current_faculty,
                         lec_count,
                         pract_count,
                         object_type,
                         sum(CASE
                                 WHEN group_type='subgroup' THEN 1
                                 ELSE 0
                             END) AS is_subgroup,
                         alias_name,
                         own_faculty,
                         SHOW
                  FROM
                    (SELECT course,
                            UUID,
                            code,
                            name,
                            sem,
                            department,
                            choice,
                            count_students,
                            count_groups,
                            sum(lec) AS lecture_fact,
                            sum(pract) AS practice_fact,
                            sum(lab) AS lab_fact,
                            is_own,
                            active,
                            (CASE
                                 WHEN object_type in (3,
                                                      6,
                                                      31) THEN max(add_inf)
                                 ELSE sum(add_inf)
                             END) AS add_inf,
                            current_faculty,
                            lec_count,
                            pract_count,
                            object_type,
                            group_type,
                            group_id,
                            alias_name,
                            own_faculty,
                            SHOW
                     FROM
                       (SELECT course,
                               ls.uuid,
                               ls.code,
                               ls.name,
                               ls.sem,
                               ls.department,
                               ls.choice,
                               ls.count_students,
                               ls.count_groups,
                               (CASE
                                    WHEN ls.hours_type='lek' THEN ls.count_hours
                                    ELSE 0
                                END) AS lec,
                               (CASE
                                    WHEN ls.hours_type='pract' THEN ls.count_hours
                                    ELSE 0
                                END) AS pract,
                               (CASE
                                    WHEN ls.hours_type='lab' THEN ls.count_hours
                                    ELSE 0
                                END) AS lab,
                               ls.is_own,
                               (CASE
                                    WHEN ls.object_type=2
                                         AND ls.choice=FALSE THEN TRUE
                                    ELSE ls.active
                                END) AS active,
                               ls.add_inf,
                               ls.current_faculty,
                               SUM (CASE
                                        WHEN work_type=101 THEN work_count
                                        ELSE 0
                                    END) AS lec_count,
                                   SUM (CASE
                                            WHEN (work_type=103
                                                  OR work_type=104) THEN work_count
                                            ELSE 0
                                        END) AS pract_count,
                                       ls.object_type,
                                       ls.group_type,
                                       ls.group_id,
                                       ls.alias_name,
                                       load.own_faculty,
                                       ls.show
                        FROM LOAD,
                             loadstrokes AS ls
                        LEFT JOIN
                          (SELECT *
                           FROM lessonplanstroke AS lp,
                                lessonplanworks AS lw
                           WHERE lw.lessonplanstroke_id=lp.id) AS reslp ON reslp.uuid=ls.uuid
                        WHERE load.profile_id=:profile_id
                          AND ls.load_id=load.id
                          AND budzhet=:budzhet
                          AND load.version=:version
                          AND ls.active=TRUE
                          AND ls.department=:dep
                          and ls.teacher_id=:tid 
                        GROUP BY ls.id,
                                 course,
                                 ls.uuid,
                                 ls.code,
                                 ls.name,
                                 ls.sem,
                                 ls.department,
                                 ls.choice,
                                 ls.is_own,
                                 active,
                                 ls.current_faculty,
                                 ls.object_type,
                                 ls.count_students,
                                 ls.count_groups,
                                 ls.group_type,
                                 ls.alias_name,
                                 ls.group_id,
                                 load.own_faculty,
                                 ls.show) AS res_table
                     GROUP BY course,
                              UUID,
                              code,
                              name,
                              sem,
                              department,
                              choice,
                              count_students,
                              count_groups,
                              is_own,
                              active,
                              current_faculty,
                              object_type,
                              lec_count,
                              pract_count,
                              alias_name,
                              group_type,
                              group_id,
                              own_faculty,
                              SHOW) AS res2
                  GROUP BY course,
                           UUID,
                           code,
                           name,
                           sem,
                           department,
                           choice,
                           is_own,
                           active,
                           add_inf,
                           current_faculty,
                           lec_count,
                           pract_count,
                           object_type,
                           alias_name,
                           own_faculty,
                           SHOW
                  ORDER BY course,
                           object_type,
                           code,
                           sem,
                           UUID;                      
                  """, {"profile_id": profile_id, "budzhet": budzhet,
                        "dep": dep, "version": version_id, "tid": teacher_id})
    
    loadstrokes = []
    uuids = []              

    for row in ls: 
        vals = colsvalues[row["uuid"]]["strokes"]
        valsfilter = [s for s in vals if s["department"]==row.department and s["budzhet"]==budzhet]

        sum_stroke = 0
        vc = []
        for val in valsfilter:
            sum_stroke += val["val"]
            vc.append({"c_id": val["c_id"], "formula": val["formula"],
                         "val": val["val"], "hand": val["hand"]})

        sum_stroke += row.lf + row.pf + row.labf

        uuids.append(row.uuid)
    
        stroke = {"uuid": row.uuid, "code": row.code, "name": row.name,
                  "alias": row.alias_name, "course": row.course,
                  "sem": row.sem, "choice": row.choice, 
                  "contingent": row.cs, "groups": row.cg,
                  "lecplan": row.lec_count, "lecfact": row.lf.normalize(),
                  "practplan": row.pract_count, "practfact": row.pf.normalize(),
                  "labfact": row.labf.normalize(), "add_inf": row.add_inf,
                  "is_own": row.is_own, "show": row.show,
                  "ownstudents": row.own_faculty, "ownteachers": row.current_faculty,
                  "summ": sum_stroke, "department": row.department, 
                  "budzhet": budzhet, "object_type": row.object_type,
                  "columns": vc}
        loadstrokes.append(stroke)


    colsvaluesnz = []

    for row in colsvalues:
        if colsvalues[row][str(budzhet)] > 0:
            colsvaluesnz.append(row)

    uuids_cols = []  

    for row in colsvaluesnz:
        if row not in uuids and row not in uuids_cols:
            uuids_cols.append(str(row))       

    ls = db.execute("""
                  SELECT course,
                         UUID,
                         code,
                         name,
                         sem,
                         department,
                         choice,
                         sum(count_students) as cs,
                         sum(count_groups) as cg,
                         sum(lecture_fact) as lf,
                         sum(practice_fact) as pf,
                         sum(lab_fact) as labf,
                         is_own,
                         active,
                         add_inf,
                         current_faculty,
                         lec_count,
                         pract_count,
                         object_type,
                         sum(CASE
                                 WHEN group_type='subgroup' THEN 1
                                 ELSE 0
                             END) AS is_subgroup,
                         alias_name,
                         own_faculty,
                         SHOW
                  FROM
                    (SELECT course,
                            UUID,
                            code,
                            name,
                            sem,
                            department,
                            choice,
                            count_students,
                            count_groups,
                            sum(lec) AS lecture_fact,
                            sum(pract) AS practice_fact,
                            sum(lab) AS lab_fact,
                            is_own,
                            active,
                            (CASE
                                 WHEN object_type in (3,
                                                      6,
                                                      31) THEN max(add_inf)
                                 ELSE sum(add_inf)
                             END) AS add_inf,
                            current_faculty,
                            lec_count,
                            pract_count,
                            object_type,
                            group_type,
                            group_id,
                            alias_name,
                            own_faculty,
                            SHOW
                     FROM
                       (SELECT course,
                               ls.uuid,
                               ls.code,
                               ls.name,
                               ls.sem,
                               ls.department,
                               ls.choice,
                               ls.count_students,
                               ls.count_groups,
                               (CASE
                                    WHEN ls.hours_type='lek' THEN ls.count_hours
                                    ELSE 0
                                END) AS lec,
                               (CASE
                                    WHEN ls.hours_type='pract' THEN ls.count_hours
                                    ELSE 0
                                END) AS pract,
                               (CASE
                                    WHEN ls.hours_type='lab' THEN ls.count_hours
                                    ELSE 0
                                END) AS lab,
                               ls.is_own,
                               (CASE
                                    WHEN ls.object_type=2
                                         AND ls.choice=FALSE THEN TRUE
                                    ELSE ls.active
                                END) AS active,
                               ls.add_inf,
                               ls.current_faculty,
                               SUM (CASE
                                        WHEN work_type=101 THEN work_count
                                        ELSE 0
                                    END) AS lec_count,
                                   SUM (CASE
                                            WHEN (work_type=103
                                                  OR work_type=104) THEN work_count
                                            ELSE 0
                                        END) AS pract_count,
                                       ls.object_type,
                                       ls.group_type,
                                       ls.group_id,
                                       ls.alias_name,
                                       load.own_faculty,
                                       ls.show
                        FROM LOAD,
                             loadstrokes AS ls
                        LEFT JOIN
                          (SELECT *
                           FROM lessonplanstroke AS lp,
                                lessonplanworks AS lw
                           WHERE lw.lessonplanstroke_id=lp.id) AS reslp ON reslp.uuid=ls.uuid
                        WHERE load.profile_id=:profile_id
                          AND ls.load_id=load.id
                          AND budzhet=:budzhet
                          AND load.version=:version
                          AND ls.active=TRUE
                          AND ls.department=:dep
                          and ls.uuid in """ +"('{}')".format(("', '").join(uuids_cols))+

                          """ 
                        GROUP BY ls.id,
                                 course,
                                 ls.uuid,
                                 ls.code,
                                 ls.name,
                                 ls.sem,
                                 ls.department,
                                 ls.choice,
                                 ls.is_own,
                                 active,
                                 ls.current_faculty,
                                 ls.object_type,
                                 ls.count_students,
                                 ls.count_groups,
                                 ls.group_type,
                                 ls.alias_name,
                                 ls.group_id,
                                 load.own_faculty,
                                 ls.show) AS res_table
                     GROUP BY course,
                              UUID,
                              code,
                              name,
                              sem,
                              department,
                              choice,
                              count_students,
                              count_groups,
                              is_own,
                              active,
                              current_faculty,
                              object_type,
                              lec_count,
                              pract_count,
                              alias_name,
                              group_type,
                              group_id,
                              own_faculty,
                              SHOW) AS res2
                  GROUP BY course,
                           UUID,
                           code,
                           name,
                           sem,
                           department,
                           choice,
                           is_own,
                           active,
                           add_inf,
                           current_faculty,
                           lec_count,
                           pract_count,
                           object_type,
                           alias_name,
                           own_faculty,
                           SHOW
                  ORDER BY course,
                           object_type,
                           code,
                           sem,
                           UUID;                      
                  """, {"profile_id": profile_id, "budzhet": budzhet,
                        "dep": dep, "version": version_id})       

    for row in ls: 
        vals = colsvalues[row["uuid"]]["strokes"]
        valsfilter = [s for s in vals if s["department"]==row.department and s["budzhet"]==budzhet]

        sum_stroke = 0
        vc = []
        for val in valsfilter:
            sum_stroke += val["val"]
            vc.append({"c_id": val["c_id"], "formula": val["formula"],
                         "val": val["val"], "hand": val["hand"]})

        # sum_stroke += row.lf + row.pf + row.labf

    
        stroke = {"uuid": row.uuid, "code": row.code, "name": row.name,
                  "alias": row.alias_name, "course": row.course,
                  "sem": row.sem, "choice": row.choice, 
                  "contingent": row.cs, "groups": row.cg,
                  "lecplan": row.lec_count, "lecfact": 0,
                  "practplan": row.pract_count, "practfact": 0,
                  "labfact": 0, "add_inf": row.add_inf,
                  "is_own": row.is_own, "show": row.show,
                  "ownstudents": row.own_faculty, "ownteachers": row.current_faculty,
                  "summ": sum_stroke, "department": row.department, 
                  "budzhet": budzhet, "object_type": row.object_type,
                  "columns": vc}
        loadstrokes.append(stroke)         
    
    return loadstrokes    



def add_data_to_worksheet(workbook, worksheet, cols, load, teacher, ver, department, budzhet):
    merge_format = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial'})
    merge_format_left = workbook.add_format({'align': 'left', 'font_size': 12, 'font_name': 'Arial'})
    cell_format_c = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial', 'border': 1})
    cell_format = workbook.add_format({'align': 'left', 'font_size': 12, 'font_name': 'Arial', 'border': 1, 'text_wrap': True})
    merge_format_rotate = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'rotation': 90,
                                               'border': 1, 'text_wrap': True, 'valign': 'vcenter'})
    merge_format_rotate_b = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'rotation': 90,
                                               'border': 1, 'text_wrap': True})
    merge_format_header = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial',
                                               'border': 1, 'text_wrap': True, 'valign': 'vcenter'})
    merge_format_bold = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial', 'bold': True})
    merge_format_boldr = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'bold': True,
                                              'border': 1})
    merge_format_right = workbook.add_format({'align': 'right', 'bold': True, 'font_size': 12, 'font_name': 'Arial'})
    merge_format_fname = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 20, 'font_name': 'Arial',
                                              'border': 1, 'bg_color': "#00CCFF"})
    merge_format_form = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 20, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#FFFF00"})
    merge_format_dir = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#00FF00"})
    merge_format_profile = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#00FFFF"})
    cell_format_itog = workbook.add_format({'align': 'left', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'text_wrap': True})
    
    worksheet.set_zoom(75)

    worksheet.set_column('A:A', 10)
    worksheet.set_column('B:B', 20)
    worksheet.set_column(2, 34, 5)

    worksheet.merge_range('AC1:AJ1', 'Таблица №5', merge_format_right)
    worksheet.merge_range('L2:Y2', 'МИНОБРНАУКИ РОССИИ', merge_format)
    worksheet.merge_range('L3:Y3', 'Федеральное государственное бюджетное', merge_format)
    worksheet.merge_range('L4:Y4', 'образовательное учреждение', merge_format)
    worksheet.merge_range('L5:Y5', 'высшего образования', merge_format)
    worksheet.merge_range('L6:Y6', '"ИРКУТСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ"', merge_format)
    # worksheet.merge_range('AC3:AK3', 'УТВЕРЖДАЮ', merge_format)
    # worksheet.merge_range('AC4:AK5', 'Проректор по учебной работе', merge_format)
    # worksheet.merge_range('AC6:AK6', '____________________А. И. Вокин', merge_format)
    # worksheet.merge_range('AC8:AK8', '"______"___________________ 20     г.', merge_format)
    worksheet.merge_range('I8:AA8', 'ИНДИВИДУАЛЬНЫЙ РАСЧЕТ УЧЕБНОЙ НАГРУЗКИ ПРЕПОДАВАТЕЛЯ на {}-{} учебный год'.format(ver.year, ver.year+1), merge_format_bold)
    # worksheet.merge_range('I10:AA10', 'на {}-{} учебный год'.format(ver.year, ver.year+1), merge_format_bold)
    worksheet.write("B6", "БЮДЖЕТ" if budzhet else "ВНЕБЮДЖЕТ", merge_format)
    worksheet.merge_range('B10:M10', 'Кафедра: {}'.format(department.name), merge_format_left)
    worksheet.merge_range('B11:M11', 'ФИО: {} {} {}'.format(teacher.surname, teacher.name, teacher.fathername or ""), merge_format_left)
    worksheet.merge_range('B12:M12', 'Ученая степень, ученое звание: {}, {}'.format(teacher.degree, teacher.academic_rank), merge_format_left)
    worksheet.merge_range('B13:M13', 'Должность: {}'.format(teacher.position), merge_format_left)
    worksheet.merge_range('B14:M14', '{}'.format("штатник" if teacher.state else "совместитель"), merge_format_left)
    worksheet.merge_range('B15:M15', 'Размер ставки: {}'.format(teacher.part_of_rate.normalize()), merge_format_left)

    worksheet.set_row(17, 150)
    worksheet.merge_range('A17:A18', 'Код дисциплины по учебному плану', merge_format_rotate)
    worksheet.merge_range('B17:B18', 'Наименование дисциплины', merge_format_header)
    worksheet.merge_range('C17:C18', 'Курс / семестр', merge_format_rotate_b)
    worksheet.merge_range('D17:D18', 'Контингент студентов', merge_format_rotate_b)
    worksheet.merge_range('E17:E18', 'Количество учебных групп', merge_format_rotate_b)
    worksheet.merge_range('F17:G17', 'Лекции', merge_format_boldr)
    worksheet.write("F18", "По плану", merge_format_rotate_b)
    worksheet.write("G18", "Всего", merge_format_rotate_b)
    worksheet.merge_range('H17:I17', 'Практ., семинар. занятия', merge_format_boldr)
    worksheet.write("H18", "По плану", merge_format_rotate_b)
    worksheet.write("I18", "Всего", merge_format_rotate_b)
    worksheet.merge_range('J17:J18', 'Лаборат. занятия', merge_format_rotate_b)

    c = 10
    cisd = {}

    for row in cols["columns"]:
        col = cols["columns"][row]
        if (len(col["children"]) == 0):
            worksheet.merge_range(16, c, 17, c, col["name"], merge_format_rotate_b)
            cisd[col["id"]] = c
            c += 1
        else:
            worksheet.merge_range(16, c, 16, c+len(col["children"])-1, col["name"] , merge_format_boldr)
            for ch in col["children"]:
               worksheet.write(17, c, ch["name"], merge_format_rotate_b)
               cisd[ch["id"]] = c
               c += 1

    worksheet.merge_range(16, c, 17, c, "ВСЕГО", merge_format_rotate_b)
    
    for i in range(1, cols["colspan"]-2):
        worksheet.write(18, i-1, i, cell_format_c)

    len_name = cols["colspan"]-5

    active_row = 19

    fill_empty = ["" for x in range(0, cols["colspan"]-3)]
    sums_forms = {}

    for key in load:
        f = load[key]
        worksheet.set_row(active_row, 22)
        worksheet.write(active_row, 0, "", cell_format_c)
        worksheet.write(active_row, 1, "", cell_format_c)
        worksheet.merge_range(active_row, 2, active_row, len_name, f["name"].upper(), merge_format_fname)
        worksheet.write(active_row, len_name+1, "", cell_format_c)       
        active_row += 1

        for kf in f["forms"]:
            form = f["forms"][kf]
            sums_forms[kf] = [0 for x in range(0, cols["colspan"]-3)]
            worksheet.set_row(active_row, 22)
            worksheet.write(active_row, 0, "", cell_format_c)
            worksheet.write(active_row, 1, "", cell_format_c)
            worksheet.merge_range(active_row, 2, active_row, len_name, form["name"].upper(), merge_format_form)
            worksheet.write(active_row, len_name+1, "", cell_format_c)       
            active_row += 1

            for dirc in form["directions"]:
                k = "nb" if budzhet else "nvb"

                if dirc[k] > 0:
                    worksheet.write(active_row, 0, "", cell_format_c)
                    worksheet.write(active_row, 1, "", cell_format_c)
                    worksheet.merge_range(active_row, 2, active_row, len_name, 'Направление {} "{}"'.format(dirc["shifr"], dirc["name"]), merge_format_dir)
                    worksheet.write(active_row, len_name+1, "", cell_format_c)       
                    active_row += 1

                for j, kp in enumerate(dirc["profiles"]):

                    profile = dirc["profiles"][kp]
                    strokes = profile["loadstrokes_budzhet"] if budzhet else profile["loadstrokes_vnebudzhet"] 

                    if len(strokes)>0:
                        worksheet.write(active_row, 0, "", cell_format_c)
                        worksheet.write(active_row, 1, "", cell_format_c)
                        worksheet.merge_range(active_row, 2, active_row, len_name, 'профиль "{}"'.format(profile["name"]), merge_format_profile)
                        worksheet.write(active_row, len_name+1, "", cell_format_c)       
                        active_row += 1     

                        for stroke in strokes:
                            if stroke["show"]:
                                worksheet.write_row(active_row, 0, fill_empty, cell_format_c)
                                worksheet.write(active_row, 0, stroke["code"], cell_format_c)
                                worksheet.write(active_row, 1, stroke["name"], cell_format)
                                worksheet.write(active_row, 2, "{}/{}".format(stroke["course"], stroke["sem"]), cell_format_c)
                                if stroke["contingent"] != 0:
                                    worksheet.write(active_row, 3, stroke["contingent"], cell_format_c)
                                if stroke["groups"] != 0:    
                                    worksheet.write(active_row, 4, stroke["groups"], cell_format_c)
                                if stroke["lecplan"] != 0:
                                    worksheet.write(active_row, 5, stroke["lecplan"], cell_format_c)
                                    sums_forms[kf][5] += stroke["lecplan"] or 0
                                if stroke["lecfact"] != 0:
                                    worksheet.write(active_row, 6, stroke["lecfact"], cell_format_c)
                                    sums_forms[kf][6] += stroke["lecfact"] or 0
                                if stroke["practplan"] != 0:    
                                    worksheet.write(active_row, 7, stroke["practplan"], cell_format_c)
                                    sums_forms[kf][7] += stroke["practplan"] or 0
                                if stroke["practfact"] != 0:    
                                    worksheet.write(active_row, 8, stroke["practfact"], cell_format_c)
                                    sums_forms[kf][8] += stroke["practfact"] or 0
                                if stroke["labfact"] != 0:    
                                    worksheet.write(active_row, 9, stroke["labfact"], cell_format_c)
                                    sums_forms[kf][9] += stroke["labfact"] or 0

                                for cval in stroke["columns"]:
                                    if cval["val"] != 0:
                                        worksheet.write(active_row, cisd[cval["c_id"]], cval["val"], cell_format_c)
                                        sums_forms[kf][cisd[cval["c_id"]]] += cval["val"]

                                worksheet.write(active_row, len_name+1, stroke["summ"], cell_format_c) 
                                sums_forms[kf][len_name+1] += stroke["summ"]      
                                active_row += 1

            worksheet.set_row(active_row, 40)
            worksheet.write_row(active_row, 0, fill_empty, cell_format_itog)
            worksheet.write(active_row, 1, 'Всего по форме обучения "{}": '.format(form["name"]), cell_format_itog)
            for idx, v in enumerate(sums_forms[kf]):
                if v != 0:
                    worksheet.write(active_row, idx, v, cell_format_itog)
            active_row += 1

    sums_all = [0 for x in range(0, cols["colspan"]-3)]        
    sums_all = [x + y for x, y in zip(sums_all, sums_forms[key])]


    worksheet.set_row(active_row, 50)
    worksheet.write_row(active_row, 0, fill_empty, cell_format_itog)
    worksheet.write(active_row, 1, 'ИТОГО: ', cell_format_itog)          

    for idx, v in enumerate(sums_all):
        if v != 0:
            worksheet.write(active_row, idx, v, cell_format_itog)
    active_row += 2        


    worksheet.merge_range(active_row, 0, active_row, 10, 'Преподаватель _______________________________', merge_format_left)        
    worksheet.merge_range(active_row+2, 0, active_row+2, 10, 'Зав. кафедрой _______________________________', merge_format_left)

    worksheet.merge_range(active_row, 25, active_row, 35, 'УТВЕРЖДАЮ', merge_format)
    worksheet.merge_range(active_row+1, 25, active_row+1, 35, 'Директор института', merge_format)
    worksheet.merge_range(active_row+2, 25, active_row+2, 35, '______________________', merge_format)
    worksheet.merge_range(active_row+3, 25, active_row+3, 35, '"______"___________________ 20     г.', merge_format)  



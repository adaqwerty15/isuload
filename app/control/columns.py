import app.models.isuload as ami
import app.models.consts as amc
import app.schemas.users as asu
import app.settings as settings

import app.schemas.columns as asc
import copy, uuid

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func
from sqlalchemy.dialects import postgresql
from sqlalchemy import update, delete, or_, and_
from sqlalchemy.orm import aliased
from sqlalchemy import nullsfirst


def get_columns(db: Session, faculty: int, year: int):

    columns = db.query(ami.ColumnLoad).\
                 filter(ami.ColumnLoad.faculty_id == faculty).\
                 filter(ami.ColumnLoad.year == year).\
                 filter(ami.ColumnLoad.active == True).\
                 order_by(nullsfirst(ami.ColumnLoad.parent_id), ami.ColumnLoad.id).\
                 all()

    cols = {}             

    for row in columns:
        if (row.id not in cols and row.parent_id == None):
            row.childrens = []
            cols[row.id] = row
        if (row.parent_id):
            cols[row.parent_id].childrens.append(row)
                       

    return cols


def edit_column(db: Session, column_id: int, name: str, alias: str):
    stmt = update(ami.ColumnLoad).\
           where(ami.ColumnLoad.id == column_id).\
           values(name=name, alias=alias).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    col = db.query(ami.ColumnLoad).get({"id": column_id})
    return get_columns(db, col.faculty_id, col.year)


def edit_column_visibility(db: Session, column_id: int):
    col = db.query(ami.ColumnLoad).get({"id": column_id})
    visibility = not col.active_view

    if (visibility and col.parent_id):
        stmt = update(ami.ColumnLoad).\
           where((ami.ColumnLoad.id == column_id) | 
                 (ami.ColumnLoad.parent_id == column_id) |
                 (ami.ColumnLoad.id == col.parent_id)).\
           values(active_view=visibility).\
           execution_options(synchronize_session="fetch")
    else:   
        stmt = update(ami.ColumnLoad).\
               where(or_(ami.ColumnLoad.id == column_id, 
                         ami.ColumnLoad.parent_id == column_id)).\
               values(active_view=visibility).\
               execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_columns(db, col.faculty_id, col.year)


def edit_column_existence(db: Session, column_id: int):
    col = db.query(ami.ColumnLoad).get({"id": column_id})

    stmt = update(ami.ColumnLoad).\
           where(or_(ami.ColumnLoad.id == column_id, 
                     ami.ColumnLoad.parent_id == column_id)).\
           values(active=False).\
           execution_options(synchronize_session="fetch")
    res = db.execute(stmt)
    db.commit()

    return get_columns(db, col.faculty_id, col.year)


def add_column(db: Session, faculty: int, year: int, 
               name: str, alias: str, parent_id=None):
    view = True

    if (parent_id):
        col = db.query(ami.ColumnLoad).get({"id": parent_id})
        view = col.active_view

    col = ami.ColumnLoad(name=name, alias=alias, year=year,
                         faculty_id=faculty, active=True,
                         active_view=view, parent_id=parent_id)
    db.add(col)
    db.commit()
    db.refresh(col)

    return get_columns(db, faculty, year)


def get_formuls(db: Session, column_id: int):
    formuls = db.query(ami.ColumnCalc).\
                 filter(ami.ColumnCalc.column_id == column_id).\
                 filter(ami.ColumnCalc.active == True).\
                 all()

    cd = db.query(ami.ConstrDirectory).all()
    cd_dir = {}

    for row in cd:
        cd_dir["{}.{}".format(row.type, row.value)] = row.name


    for row in formuls:
        constrs = {}  
        for c in row.constraints:
            c.name = cd_dir["{}.{}".format(c.constr_name, c.constr_value)]
            if c.constr_name not in constrs:
                constrs[c.constr_name] = []
            constrs[c.constr_name].append(c.constr_value)
        row.constrs = constrs                     

    return formuls


def get_formuls_data(db: Session):
    variables = {"l_countStudents": {"label": "КонтингентСтудентов", "value": "l_countStudents"},
                 "l_countForeign": {"label": "КонтингентИностранныхСтудентов", "value": "l_countForeign"},
                 "l_countGroups": {"label": "КоличествоУчебныхГрупп", "value": "l_countGroups"},
                 "l_lecturesFact": {"label": "ЛекцииВсего", "value": "l_lecturesFact"},
                 "l_practiceFact": {"label": "ПрактикиВсего", "value": "l_practiceFact"},
                 "l_labFact": {"label": "ЛабораторныеВсего", "value": "l_labFact"},
                 "l_addInf": {"label": "ДопИнформация(нед.,чел.)", "value": "l_addInf"},
                 "l_mainFlow": {"label": "ГлавныйВПотоке", "value": "l_mainFlow"},
                 }

    sel = db.query(ami.TypesOfWorkDirectory).\
             filter(ami.TypesOfWorkDirectory.active == True).\
             filter(ami.TypesOfWorkDirectory.active_calc == True).\
             all()

    for row in sel:
        newname = row.name.split(" ")
        n = ""
        for r in newname:
            n = "{}{}{}".format(n, r[0].upper(), r[1:])

        code = "t_e_{}".format(row.code)    
        variables[code] = {"label": "{}ЕстьВПлане".format(n), "value": code}

        code = "t_c_{}".format(row.code)    
        variables[code] = {"label": "{}КоличествоВПлане".format(n), "value": code}            

    return variables


def get_constrs(db: Session):
    sel = db.query(ami.ConstrDirectory).\
             filter(ami.ConstrDirectory.active == True).\
             all()

    constrs = {}
    
    for row in sel:
        if row.type not in constrs:
            constrs[row.type] = []
        constrs[row.type].append(row)             

    return constrs  

def add_formula_simple(db: Session, column_id: int, data):
    col = ami.ColumnCalc(column_id=column_id, formula=data.formula,
                         formula_text=data.formula_text, active=True)
    db.add(col)
    db.commit()
    db.refresh(col)

    for row in data.constrs:
        c = ami.CalcConstraint(calc_id=col.id, constr_name=row.name, 
                               constr_value=row.value)
        db.add(c)
        db.commit()
        db.refresh(c)


def add_formula(db: Session, column_id: int, data):
    add_formula_simple(db, column_id, data)
    return get_formuls(db, column_id)


def edit_formula(db: Session, column_id: int, formul_id: int, data):
    delete_formula_simple(db, formul_id)
    add_formula_simple(db, column_id, data)

    return get_formuls(db, column_id)


def delete_formula_simple(db: Session, formul_id: int):
    stmt = update(ami.ColumnCalc).\
           where(ami.ColumnCalc.id == formul_id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    stmt = delete(ami.CalcConstraint).\
           where(ami.CalcConstraint.calc_id == formul_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)


def delete_formula(db: Session, column_id: int, formul_id: int):
    delete_formula_simple(db, formul_id)
    return get_formuls(db, column_id)


def copy_columns_from_previous_year(db:Session, faculty: int, year: int):
    selcols = db.query(ami.ColumnLoad).\
                 filter(ami.ColumnLoad.active == True).\
                 filter(ami.ColumnLoad.faculty_id == faculty).\
                 filter(ami.ColumnLoad.year == year-1).\
                 order_by(nullsfirst(ami.ColumnLoad.parent_id), ami.ColumnLoad.id).\
                 all()

    columns = {}
    formuls = {}

    for row in selcols:
        if row.parent_id:
            col = ami.ColumnLoad(name=row.name, alias=row.alias, year=year,
                                 faculty_id=row.faculty_id, active=True,
                                 active_view=row.active_view, parent_id=columns[row.parent_id])
            db.add(col)
            db.commit()
            db.refresh(col)
            columns[row.id] = col.id
        else:
            col = ami.ColumnLoad(name=row.name, alias=row.alias, year=year,
                                 faculty_id=row.faculty_id, active=True,
                                 active_view=row.active_view)
            db.add(col)
            db.commit()
            db.refresh(col)
            columns[row.id] = col.id

    if len(columns.keys()) > 0:
        sel_formuls = db.query(ami.ColumnCalc).\
                         filter(ami.ColumnCalc.column_id.in_(columns.keys())).\
                         filter(ami.ColumnCalc.active == True).\
                         all()

        for row in sel_formuls:
            col = ami.ColumnCalc(column_id=columns[row.column_id], formula=row.formula,
                                 formula_text=row.formula_text, active=True)
            db.add(col)
            db.commit()
            db.refresh(col)
            formuls[row.id] = col.id

    if len(formuls.keys()) > 0:
        sel_constrs = db.query(ami.CalcConstraint).\
                         filter(ami.CalcConstraint.calc_id.in_(formuls.keys())).\
                         all()

        for row in sel_constrs:
            c = ami.CalcConstraint(calc_id=formuls[row.calc_id], constr_name=row.constr_name, 
                                   constr_value=row.constr_value)
            db.add(c)
            db.commit()
            db.refresh(c)                                        

    return get_columns(db, faculty, year)      



import app.models.isuload as ami
import app.schemas.users as asu
import app.settings as settings
import requests
import json

import app.schemas.dirs as asd

from sqlalchemy.orm import Session, aliased
from fastapi import Depends, HTTPException, status
from typing import Optional
from sqlalchemy import nullsfirst, distinct, update
from sqlalchemy import delete, or_, and_, func
from datetime import timedelta, datetime
from sqlalchemy.dialects import postgresql


#FACULTIES

def get_faculty_by_id(db: Session, f_id: int, year: int = None):
    y = 9998

    if year:
           y = year
    dt = datetime.strptime(str(y+1), '%Y') - timedelta(seconds=1)

    return db.query(ami.FacultyNameVersion.faculty_id, ami.Faculty.id, ami.FacultyNameVersion.name,
                        ami.Faculty.active, ami.FacultyNameVersion.group_template,
                        ami.Faculty.uid_1c, ami.FacultyNameVersion.date_start).\
              distinct(ami.FacultyNameVersion.faculty_id).\
              join(ami.FacultyNameVersion).\
              filter(ami.Faculty.active == True).\
              filter(ami.Faculty.id == f_id).\
              filter(ami.FacultyNameVersion.date_start <= dt).\
              order_by(ami.FacultyNameVersion.faculty_id).\
              first() 


def get_faculties(db: Session, ids: str = None, year: int = None):
    y = 9998

    if year:
           y = year

    dt = datetime.strptime(str(y+1), '%Y') - timedelta(seconds=1)   

    if ids:
        l = [int(x) for x in ids.split(",")]

        return db.query(ami.FacultyNameVersion.faculty_id, ami.Faculty.id, ami.FacultyNameVersion.name,
                        ami.Faculty.active, ami.FacultyNameVersion.group_template,
                        ami.Faculty.uid_1c, ami.FacultyNameVersion.date_start).\
                  distinct(ami.FacultyNameVersion.faculty_id).\
                  join(ami.FacultyNameVersion).\
                  filter(ami.Faculty.id.in_(l)).\
                  filter(ami.Faculty.active == True).\
                  filter(ami.FacultyNameVersion.date_start <= dt).\
                  order_by(ami.FacultyNameVersion.faculty_id).\
                  all()

    else:
        return db.query(ami.FacultyNameVersion.faculty_id, ami.Faculty.id, ami.FacultyNameVersion.name,
                        ami.Faculty.active, ami.FacultyNameVersion.group_template,
                        ami.Faculty.uid_1c, ami.FacultyNameVersion.date_start).\
                  distinct(ami.FacultyNameVersion.faculty_id).\
                  join(ami.FacultyNameVersion).\
                  filter(ami.Faculty.active == True).\
                  filter(ami.FacultyNameVersion.date_start <= dt).\
                  order_by(ami.FacultyNameVersion.faculty_id).\
                  all()


def compare_faculties_with_1c(db: Session):
    r = requests.get(settings.GET_1C_FACULTUES_URL)
    res = r.json()
    data = res['response']['hs_json']['Output']['Data']
    data_1c = {}

    for row in data:
        row_s = row["Ссылка"]
        data_1c[row_s["uid"]] = row_s["name"]

    faculties = get_faculties(db)

    data_loc = {}

    for f in faculties:
        if f["uid_1c"]:
            data_loc[f["uid_1c"]] = f

    for uid in data_1c:
        if uid not in data_loc:
            new_f = ami.Faculty(name=data_1c[uid], active=True, uid_1c=uid)
            db.add(new_f)
            db.commit()
            db.refresh(new_f)

            new_f_inf = ami.FacultyNameVersion(name=data_1c[uid], faculty_id=new_f.id,
                                               date_start=datetime.now())
            db.add(new_f_inf)
            db.commit()
            db.refresh(new_f_inf)

    for uid in data_loc:
        if uid not in data_1c:
           stmt = update(ami.Faculty).\
                  where(ami.Faculty.uid_1c == uid).\
                  values(active=False).\
                  execution_options(synchronize_session="fetch")

           res = db.execute(stmt)
           db.commit()

        else:
            if data_loc[uid]["name"] != data_1c[uid]:
                stmt = update(ami.Faculty).\
                       where(ami.Faculty.uid_1c == uid).\
                       values(name=data_loc[uid]["name"]).\
                       execution_options(synchronize_session="fetch")

                res = db.execute(stmt)
                db.commit()

                new_f_inf = ami.FacultyNameVersion(name=data_1c[uid], faculty_id=data_loc[uid]["id"],
                                                   date_start=datetime.now(), group_template=data_loc[uid]["group_template"])
                db.add(new_f_inf)
                db.commit()
                db.refresh(new_f_inf)

    return get_faculties(db)            


def add_faculty(db: Session, name: str, group_template: str):
    new_f = ami.Faculty(name=name, active=True)
    db.add(new_f)
    db.commit()
    db.refresh(new_f)    

    new_f_inf = ami.FacultyNameVersion(name=name, faculty_id=new_f.id,
                                       date_start=datetime.now(), group_template=group_template)
    db.add(new_f_inf)
    db.commit()
    db.refresh(new_f_inf)

    return get_faculties(db)


def edit_faculty(db: Session, faculty_id: int, name: str, group_template: str):
    new_f_inf = ami.FacultyNameVersion(name=name, faculty_id=faculty_id,
                                       date_start=datetime.now(), group_template=group_template)
    db.add(new_f_inf)
    db.commit()
    db.refresh(new_f_inf)

    return get_faculties(db)


def delete_faculty(db: Session, faculty_id: int):
    stmt = update(ami.Faculty).\
           where(ami.Faculty.id == faculty_id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_faculties(db)    


##DIRECTIONS AND PROFILES

def get_directions(db: Session, ids: str = None):
    if ids:
        l = [int(x) for x in ids.split(",")]
        return db.query(ami.Direction).filter(ami.Direction.id.in_(l)).filter(ami.Direction.active==True).all()
    return db.query(ami.Direction).all()


def get_profiles_tree(db: Session):
    faculties = get_faculties(db)
    faculties_dir = {}
    for row in faculties:
        faculties_dir[row.faculty_id] = {"key": str(row.id), "data": {"name": row.name, "id": row.id, "type": "faculty", "uid_1c": row.uid_1c}, "children": []}

    all_dirs = db.query(ami.Direction).\
                  filter(ami.Direction.active == True).\
                  filter(ami.Direction.form_id == 1).\
                  order_by(ami.Direction.shifr).\
                  all()

    for row in all_dirs:
        direction = {"key": "{}-{}".format(row.faculty_id, row.id), 
                     "data": {
                     "shifr": row.shifr, 
                     "qual": row.qual.name, "faculty_id": row.faculty_id,
                     "name": row.name, "id": row.id, "group_template": row.group_template,
                     "uid_1c": row.uid_1c, "qual_id": row.qual.value,
                     "type": "direction"},
                     "children": []}
        profiles = []
        for p in row.profiles:
            if p.active:
                profiles.append({"key": "{}-{}-{}".format(row.faculty_id, row.id, p.id),
                                 "data": {"name": p.name, "id": p.id, "type": "profile", "uid_1c": row.uid_1c,
                                          "faculty_id": row.faculty_id, "direction_id": row.id}})

        direction["children"] = profiles
        faculties_dir[row.faculty_id]["children"].append(direction)

    res = []
    for row in faculties_dir:
        res.append(faculties_dir[row])         
                      
    return res


def get_profiles(db: Session, ids: str = None):
    if ids:
        l = [int(x) for x in ids.split(",")]
        return db.query(ami.Profile).filter(ami.Profile.id.in_(l)).all()
    return db.query(ami.Profile).all()


def get_departments(db: Session, faculty: int = None, own: bool = None):
    if faculty:
        if own == True or own == None:
            return db.query(ami.Department).filter(ami.Department.faculty == faculty).all()
        elif own == False:
            return db.query(ami.Department).\
                      filter(or_(ami.Department.faculty != faculty, ami.Department.faculty == None)).\
                      order_by(ami.Department.number).\
                      all()    
    return db.query(ami.Department).all()


def get_department_by_number(db: Session, d_id: int):
    return db.query(ami.Department).filter(ami.Department.number == d_id).first()  


def add_direction(db: Session, name: str, shifr: str, 
                  faculty_id: int, qual_id: int,
                  group_template: str):
    forms = [1, 2, 3]
    new_d = None

    for form in forms:
        new_d = ami.Direction(name=name, 
                              active=True, shifr=shifr,
                              faculty_id=faculty_id,
                              qual_id=qual_id,
                              form_id=form,
                              group_template=group_template)
        db.add(new_d)
        db.commit()
        db.refresh(new_d)

    qual = get_qualification(db, qual_id)    

    return {"key": "{}-{}".format(faculty_id, new_d.id), 
            "data": {
                     "shifr": shifr, 
                     "qual": qual.name, "faculty_id": faculty_id, "qual_id": qual_id,
                     "name": name, "id": new_d.id, "group_template": group_template,
                     "uid_1c": None,
                     "type": "direction"},
            "children": []
           }


def edit_direction(db: Session, name: str, shifr: str, 
                   faculty_id: int, qual_id: int,
                   group_template: str, d_id: int):
    new_d = None

    sel = db.query(ami.Direction).\
             filter(ami.Direction.id == d_id).\
             first()

    sel_ids = db.query(ami.Direction.id).\
                 filter(ami.Direction.name == sel.name).\
                 filter(ami.Direction.shifr == sel.shifr).\
                 filter(ami.Direction.faculty_id == sel.faculty_id).\
                 filter(ami.Direction.qual_id == sel.qual_id).\
                 all() 

    d_ids = [x[0] for x in sel_ids]           

    stmt = update(ami.Direction).\
           where(ami.Direction.id.in_(d_ids)).\
           values(name=name, shifr=shifr, qual_id=qual_id, group_template=group_template).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    qual = get_qualification(db, qual_id)    

    return {"key": "{}-{}".format(faculty_id, d_id), 
            "data": {
                     "shifr": shifr, 
                     "qual": qual.name, "faculty_id": faculty_id,
                     "name": name, "id": d_id, "group_template": group_template,
                     "uid_1c": sel.uid_1c,  "qual_id": qual_id,
                     "type": "direction"},
            "children": []
           }


def delete_direction(db: Session,  
                     faculty_id: int, d_id: int):
    new_d = None

    sel = db.query(ami.Direction).\
             filter(ami.Direction.id == d_id).\
             first()

    sel_ids = db.query(ami.Direction.id).\
                 filter(ami.Direction.name == sel.name).\
                 filter(ami.Direction.shifr == sel.shifr).\
                 filter(ami.Direction.faculty_id == sel.faculty_id).\
                 filter(ami.Direction.qual_id == sel.qual_id).\
                 all() 

    d_ids = [x[0] for x in sel_ids]           

    stmt = update(ami.Direction).\
           where(ami.Direction.id.in_(d_ids)).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()  

    return {"data": {"faculty_id": faculty_id, "id": d_id}}


def add_profile(db: Session, d_id: int, name: str, faculty_id: int):
    sel = db.query(ami.Direction).\
             filter(ami.Direction.id == d_id).\
             first()

    sel_ids = db.query(ami.Direction.id).\
                 filter(ami.Direction.name == sel.name).\
                 filter(ami.Direction.shifr == sel.shifr).\
                 filter(ami.Direction.faculty_id == sel.faculty_id).\
                 filter(ami.Direction.qual_id == sel.qual_id).\
                 all() 

    d_ids = [x[0] for x in sel_ids]

    new_p = None

    for d in d_ids:
        new_p = ami.Profile(name=name,
                            active=True,
                            direction_id=d)
        
        db.add(new_p)
        db.commit()
        db.refresh(new_p)

    return {"key": "{}-{}-{}".format(faculty_id, d_id, new_p.id),
            "data": {"name": name, "id": new_p.id, "type": "profile", "uid_1c": None,
                     "faculty_id": faculty_id, "direction_id": d_id}}


def edit_profile(db: Session, d_id: int, name: str, faculty_id: int, p_id: int):
    sel = db.query(ami.Direction).\
             filter(ami.Direction.id == d_id).\
             first()

    sel_ids = db.query(ami.Direction.id).\
                 filter(ami.Direction.name == sel.name).\
                 filter(ami.Direction.shifr == sel.shifr).\
                 filter(ami.Direction.faculty_id == sel.faculty_id).\
                 filter(ami.Direction.qual_id == sel.qual_id).\
                 all() 

    d_ids = [x[0] for x in sel_ids]

    sel_p = db.query(ami.Profile).\
               filter(ami.Profile.id == p_id).\
               first()

    stmt = update(ami.Profile).\
           where(ami.Profile.direction_id.in_(d_ids)).\
           where(ami.Profile.name == sel_p.name).\
           values(name=name).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return {"key": "{}-{}-{}".format(faculty_id, d_id, p_id),
            "data": {"name": name, "id": p_id, "type": "profile", "uid_1c": sel_p.uid_1c,
                     "faculty_id": faculty_id, "direction_id": d_id}}


def delete_profile(db: Session, d_id: int, faculty_id: int, p_id: int):
    sel = db.query(ami.Direction).\
             filter(ami.Direction.id == d_id).\
             first()

    sel_ids = db.query(ami.Direction.id).\
                 filter(ami.Direction.name == sel.name).\
                 filter(ami.Direction.shifr == sel.shifr).\
                 filter(ami.Direction.faculty_id == sel.faculty_id).\
                 filter(ami.Direction.qual_id == sel.qual_id).\
                 all() 

    d_ids = [x[0] for x in sel_ids]

    sel_p = db.query(ami.Profile).\
               filter(ami.Profile.id == p_id).\
               first()

    stmt = update(ami.Profile).\
           where(ami.Profile.direction_id.in_(d_ids)).\
           where(ami.Profile.name == sel_p.name).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return {"data": {"faculty_id": faculty_id, "direction_id": d_id, "id": p_id}}                                             


def compare_directions_with_1c(db: Session):
    r = requests.get(settings.GET_1C_DIRECTIONS_URL)
    res = r.json()
    data = res['response']['hs_json']['Output']['Data']
    data_1c = {}

    for row in data:
        row_p = row["Профиль"]
        if row_p != "" and row_p["name"] != "":
            row_f = row["Факультет"]
            if row_f["uid"] not in data_1c:
                data_1c[row_f["uid"]] = {}
            row_d = row["СпециальностьСсылка"]    
            if row_d["uid"] not in data_1c[row_f["uid"]]:
                qual = row["УровеньПодготовки"]["name"] if row["УровеньПодготовки"]["name"] != "" else "Бакалавриат"
                data_1c[row_f["uid"]][row_d["uid"]] = {"name": row_d["name"], 
                                                       "shifr": row["СпециальностьКодСпециальности"],
                                                       "qual": qual,
                                                       "profiles": {},
                                                       "ids": []}
            if row_p["uid"] not in data_1c[row_f["uid"]][row_d["uid"]]["profiles"]:
               data_1c[row_f["uid"]][row_d["uid"]]["profiles"][row_p["uid"]] = row_p["name"]                                            


    faculties = db.query(ami.Faculty).filter(ami.Faculty.active == True).all()
    data_loc = {}

    for row in faculties:
        if (row.uid_1c):
            data_loc[row.uid_1c] = {"id": row.id, "directions": {}}
            for d in row.directions:
                if d.active and d.uid_1c:
                    if d.uid_1c not in data_loc[row.uid_1c]["directions"]:
                        data_loc[row.uid_1c]["directions"][d.uid_1c] = {"name": d.name, "shifr": d.shifr, 
                                                                        "profiles": {}, "ids": [d.id]}
                    else:
                         data_loc[row.uid_1c]["directions"][d.uid_1c]["ids"].append(d.id)                                                   
                    for p in d.profiles:
                        if p.active and p.uid_1c and p.uid_1c not in data_loc[row.uid_1c]["directions"][d.uid_1c]["profiles"]:
                           data_loc[row.uid_1c]["directions"][d.uid_1c]["profiles"][p.uid_1c] = p.name      

    forms = [1, 2, 3]
    quals = {"Бакалавриат": 1, "Магистратура": 2, "Аспирантура": 3, "Специалитет": 4, "СПО": 5}                
    for fuid in data_1c:
        if fuid in data_loc:
            for duid in data_1c[fuid]:
                if duid not in data_loc[fuid]["directions"]:
                    for form in forms:
                        new_d = ami.Direction(name=data_1c[fuid][duid]["name"], 
                                              active=True, uid_1c=duid, 
                                              shifr=data_1c[fuid][duid]["shifr"],
                                              faculty_id=data_loc[fuid]["id"],
                                              qual_id=quals[data_1c[fuid][duid]["qual"]],
                                              form_id=form)
                        db.add(new_d)
                        db.commit()
                        db.refresh(new_d)
                        data_1c[fuid][duid]["ids"].append(new_d.id)

                    for puid in data_1c[fuid][duid]["profiles"]:
                        for idd in data_1c[fuid][duid]["ids"]:
                            new_p = ami.Profile(name=data_1c[fuid][duid]["profiles"][puid],
                                                uid_1c=puid,
                                                active=True,
                                                direction_id=idd)
                            db.add(new_p)
                            db.commit()
                            db.refresh(new_p)
                else:            
                    for puid in data_1c[fuid][duid]["profiles"]:
                        if puid not in data_loc[fuid]["directions"][duid]["profiles"]:
                            for idd in data_loc[fuid]["directions"][duid]["ids"]:
                                new_p = ami.Profile(name=data_1c[fuid][duid]["profiles"][puid],
                                                    uid_1c=puid,
                                                    active=True,
                                                    direction_id=idd)
                                db.add(new_p)
                                db.commit()
                                db.refresh(new_p)                                                         
                                   

    for fuid in data_loc:
        if fuid in data_1c:
            for duid in data_loc[fuid]["directions"]:
                if duid not in data_1c[fuid]:
                   stmt = update(ami.Direction).\
                          where(ami.Direction.uid_1c == duid).\
                          values(active=False).\
                          execution_options(synchronize_session="fetch")

                   res = db.execute(stmt)
                   db.commit()

                else:
                    d = data_loc[fuid]["directions"][duid]
                    if d["name"] != data_1c[fuid][duid]["name"] or d["shifr"] != data_1c[fuid][duid]["shifr"]:
                        stmt = update(ami.Direction).\
                                      where(ami.Direction.uid_1c == duid).\
                                      values(name=data_1c[fuid][duid]["name"], 
                                             shifr=data_1c[fuid][duid]["shifr"]).\
                                      execution_options(synchronize_session="fetch")

                        res = db.execute(stmt)
                        db.commit()

                    for puid in data_loc[fuid]["directions"][duid]["profiles"]:
                        if puid not in data_1c[fuid][duid]["profiles"]:
                           stmt = update(ami.Profile).\
                                  where(ami.Profile.uid_1c == puid).\
                                  values(active=False).\
                                  execution_options(synchronize_session="fetch")

                           res = db.execute(stmt)
                           db.commit()
                        else:
                            if data_loc[fuid]["directions"][duid]["profiles"][puid] != data_1c[fuid][duid]["profiles"][puid]:
                                stmt = update(ami.Profile).\
                                       where(ami.Profile.uid_1c == puid).\
                                       values(name=data_1c[fuid][duid]["profiles"][puid]).\
                                       execution_options(synchronize_session="fetch")

                                res = db.execute(stmt)
                                db.commit()

    return get_profiles_tree(db)


def get_own_object_types(db:Session):
    return db.query(ami.ConstrDirectory).\
              filter(ami.ConstrDirectory.active == True).\
              filter(ami.ConstrDirectory.type == 'object_type').\
              filter(ami.ConstrDirectory.own == True).\
              all()


def get_columns_for_change(db: Session, faculty: int, version: int):

    ver = db.query(ami.LoadVersion).get({"id":version})
    
    columns = db.query(ami.ColumnLoad).\
                 filter(ami.ColumnLoad.faculty_id == faculty).\
                 filter(ami.ColumnLoad.active == True).\
                 filter(ami.ColumnLoad.active_view == True).\
                 filter(ami.ColumnLoad.year == ver.year).\
                 order_by(nullsfirst(ami.ColumnLoad.parent_id), ami.ColumnLoad.id).\
                 all()

    cols = {}             

    for row in columns:
        if (row.id not in cols and row.parent_id == None):
            row.childrens = []
            cols[row.id] = row
        if (row.parent_id):
            cols[row.parent_id].childrens.append(row)

    res = []
    
    for row in cols:
        col = cols[row]
        if (len(col.childrens)==0):
            res.append({"name": col.name, "id": col.id})
        else:
            for child in col.childrens:
                res.append({"name": "{} / {}".format(col.name, child.name), "id": child.id})                            

    return res


def get_qualifications(db: Session):
    return db.query(ami.ConstrDirectory).\
              filter(ami.ConstrDirectory.type=="qualification").\
              filter(ami.ConstrDirectory.active == True).\
              all()


def get_qualification(db: Session, qid: int):
    return db.query(ami.ConstrDirectory).\
              filter(ami.ConstrDirectory.type=="qualification").\
              filter(ami.ConstrDirectory.active == True).\
              filter(ami.ConstrDirectory.value == qid).\
              first()              


def get_teachers(db: Session, faculty: int):

    deps_all = db.query(ami.Department.number).filter(ami.Department.faculty == faculty).all()
    deps = [r[0] for r in deps_all]

    teachers = db.query(ami.Teacher).\
                  filter(ami.Teacher.department.in_(deps)).\
                  filter(ami.Teacher.active == True).\
                  order_by(ami.Teacher.surname).\
                  all()

    for row in teachers:
        if row.state:
            row.state_text = "Да"
        else:
            row.state_text = "Нет"    


    return teachers


def get_teacher(db: Session, teacher_id: int):

    teacher = db.query(ami.Teacher).\
                  filter(ami.Teacher.id==teacher_id).\
                  filter(ami.Teacher.active == True).\
                  first()    

    return teacher   


def compare_teachers_with_1c(db: Session, faculty: int):
    facult = db.query(ami.Faculty).get({"id":faculty})

    deps = {}
    deps_list = []

    departments = db.query(ami.Department).filter(ami.Department.faculty==faculty).all()
    for row in departments:
        deps[row.name] = row.number
        deps_list.append(row.number)

    teachers = db.query(ami.Teacher).\
                  filter(ami.Teacher.active == True).\
                  filter(ami.Teacher.department.in_(deps_list)).\
                  all()
    teachers_loc = {}

    for row in teachers:
        if row.uid_1c:
            teachers_loc[row.uid_1c] = row

    if (facult.uid_1c):       
        r = requests.post('http://py.isu.ru:8000/hs/jsonpost/pps_in_faculty/', data = json.dumps({"guid": "3c9467d8-b710-11e6-943c-005056100702",
                                                                                                  "facultet": facult.uid_1c}),
                                                                               headers={'Content-Type': 'application/json'})
        res = r.json()
        data = res['hs_json']['Output']['Data']
        data_1c = []

        
        for row in data:
            data_1c.append(row["СотрудникФизическоеЛицо"]["uid"])
            if row["СотрудникФизическоеЛицо"]["uid"] not in teachers_loc:
                dep = 0
                if row["Кафедра"]["name"] in deps:
                    dep = deps[row["Кафедра"]["name"]]

                names = row["СотрудникФизическоеЛицо"]["name"].split(" ")
                state = True if row["ВидЗанятости"]["name"] == "ОсновноеМестоРаботы" else False  

                teacher = ami.Teacher(surname=names[0], name=names[1], fathername=names[2],
                                      position=row["Должность"]["name"], academic_rank=row["УченоеЗвание"]["name"],
                                      degree=row["УченаяСтепень"]["name"], part_of_rate=row["Ставка"],
                                      state=state, department=dep, uid_1c=row["СотрудникФизическоеЛицо"]["uid"],
                                      active=True)

                db.add(teacher)
                db.commit()
                db.refresh(teacher)

            else:
                stmt = update(ami.Teacher).\
                       where(ami.Teacher.uid_1c == row["СотрудникФизическоеЛицо"]["uid"]).\
                       values(surname=names[0], name=names[1], fathername=names[2],
                              position=row["Должность"]["name"], academic_rank=row["УченоеЗвание"]["name"],
                              degree=row["УченаяСтепень"]["name"], part_of_rate=row["Ставка"],
                              state=state, department=dep).\
                       execution_options(synchronize_session="fetch")

                res = db.execute(stmt)
                db.commit()

        for tuid in teachers_loc:
            if tuid not in data_1c:
                stmt = update(ami.Teacher).\
                       where(ami.Teacher.uid_1c == tuid).\
                       values(active=False).\
                       execution_options(synchronize_session="fetch")

                res = db.execute(stmt)
                db.commit()    
                                             
    return get_teachers(db, faculty)


def add_teacher(db:Session, name: str, surname: str, fathername: str, position: str, 
                degree: str, academic_rank: str, rate: float, state: bool, dep: int, faculty: int):
    t = ami.Teacher(name=name, surname=surname, fathername=fathername,
                    position=position, degree=degree, academic_rank=academic_rank,
                    part_of_rate=rate, state=state, department=dep, active=True)
    db.add(t)
    db.commit()
    db.refresh(t)

    return get_teachers(db, faculty)


def edit_teacher(db:Session, name: str, surname: str, fathername: str, position: str, 
                  degree: str, academic_rank: str, rate: float, state: bool, dep: int, t_id: int,
                  faculty: int):
    stmt = update(ami.Teacher).\
            where(ami.Teacher.id == t_id).\
            values(name=name, surname=surname, fathername=fathername,
                    position=position, degree=degree, academic_rank=academic_rank,
                    part_of_rate=rate, state=state, department=dep).\
            execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_teachers(db, faculty)


def delete_teacher(db: Session, t_id: int, faculty: int):
    stmt = update(ami.Teacher).\
           where(ami.Teacher.id == t_id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_teachers(db, faculty)


def get_all_departments(db: Session):   
    deps = db.query(ami.Department).\
              order_by(ami.Department.name).\
              all()

    faculties = get_faculties(db)
    faculties_dir = {}

    for row in faculties:
        faculties_dir[row.id] = row.name

    for row in deps:
        if (row.faculty):
            row.faculty_name = faculties_dir[row.faculty]
        else:
            row.faculty_name = None   

    return deps


def add_department(db: Session, name: str, number: int, faculty_id: int):
    dep = ami.Department(name=name, faculty=faculty_id, 
                         alias=name, number=number)

    db.add(dep)
    db.commit()
    db.refresh(dep)

    return get_all_departments(db)


def edit_department(db: Session, name: str, number: int, faculty_id: int, dep_id: int):  
    stmt = update(ami.Department).\
           where(ami.Department.id == dep_id).\
           values(name=name, faculty=faculty_id, 
                  alias=name, number=number).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_all_departments(db)


def delete_department(db: Session, dep_id: int):
    stmt = delete(ami.Department).\
           where(ami.Department.id == dep_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    return get_all_departments(db)


def get_object_types(db:Session):
    return db.query(ami.ConstrDirectory).\
              filter(ami.ConstrDirectory.active == True).\
              filter(ami.ConstrDirectory.type == 'object_type').\
              all()


def add_object_type(db: Session, name: str, add_inf: str):
    sel = db.query(func.max(ami.ConstrDirectory.value)).\
             filter(ami.ConstrDirectory.type == 'object_type').\
             first()

    add_inf_v = None if add_inf=="0" else add_inf     

    ot = ami.ConstrDirectory(name=name, value=sel[0]+1, active=True, type="object_type",
                             own=True, add_inf_type=add_inf_v)

    db.add(ot)
    db.commit()
    db.refresh(ot)

    return get_object_types(db)


def edit_object_type(db: Session, name: str, add_inf: str, ot_id: int):
    add_inf_v = None if add_inf=="0" else add_inf

    stmt = update(ami.ConstrDirectory).\
           where(ami.ConstrDirectory.id == ot_id).\
           values(name=name, add_inf_type=add_inf_v).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit() 

    return get_object_types(db)


def delete_object_type(db: Session, ot_id: int):
    stmt = update(ami.ConstrDirectory).\
           where(ami.ConstrDirectory.id == ot_id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit() 

    return get_object_types(db)


def get_types_of_work(db: Session):
    return db.query(ami.TypesOfWorkDirectory).filter(ami.TypesOfWorkDirectory.active==True).all()


def add_type_of_work(db: Session, code: int, name: str, active_calc: bool, type_w: str):
    tw = ami.TypesOfWorkDirectory(code=code, name=name, active=True, active_calc=active_calc, type=type_w)

    db.add(tw)
    db.commit()
    db.refresh(tw)

    return get_types_of_work(db)


def edit_type_of_work(db: Session, code: int, name: str, active_calc: bool, type_w: str, tw_id: int): 
    stmt = update(ami.TypesOfWorkDirectory).\
           where(ami.TypesOfWorkDirectory.id == tw_id).\
           values(code=code, name=name, active_calc=active_calc, type=type_w).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_types_of_work(db)


def delete_type_of_work(db: Session, tw_id: int):
    stmt = update(ami.TypesOfWorkDirectory).\
           where(ami.TypesOfWorkDirectory.id == tw_id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return get_types_of_work(db)    


import app.models.isuload as ami
import app.schemas.users as asu
import app.settings as settings

from sqlalchemy.orm import Session
from passlib.context import CryptContext
from jose import JWTError, jwt
from fastapi import Depends, HTTPException, status
from typing import Optional
from fastapi.security import OAuth2PasswordBearer
from datetime import timedelta, datetime
from app.dependencies import get_db


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_scheme = OAuth2PasswordBearer(tokenUrl="token") 


def create_user(db: Session, user: asu.UserCreate):
    hashed_password = pwd_context.hash(user.password)
    db_user = ami.User(login=user.login,
                       surname=user.surname,
                       name=user.name,
                       patr=user.patr,
                       hashed_pass=hashed_password,
                       edited_year=datetime.now().year)
    db.add(db_user)
    db.commit()
    db.refresh(db_user)
    return db_user


def get_user_by_login(db: Session, login: str):
    return db.query(ami.User).filter(ami.User.login == login).first()


def get_users(db: Session):
    return db.query(ami.User).all()


def change_year(db, current_user, year):
    current_user.edited_year = year
    db.commit()
    db.refresh(current_user)
    return year   


def verify_password(plain_password, hashed_password):
    return pwd_context.verify(plain_password, hashed_password)


def authenticate_user(db: Session, login: str, password: str):
    user = get_user_by_login(db, login)
    if not user:
        return False
    if not verify_password(password, user.hashed_pass):
        return False
    return user       


# async def get_current_user(token: str, db: Session = Depends(get_db)):
async def get_current_user(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        login: str = payload.get("sub")
        if login is None or payload.get('type') != 'access':
            raise credentials_exception
        token_data = asu.TokenData(login=login)
    except JWTError:
        raise credentials_exception
    user = get_user_by_login(db, login=token_data.login)
    if user is None:
        raise credentials_exception    
    return user


# async def refresh(token: str, db: Session = Depends(get_db)):
async def refresh(token: str = Depends(oauth2_scheme), db: Session = Depends(get_db)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Could not validate credentials",
        headers={"WWW-Authenticate": "Bearer"},
    )
    try:
        payload = jwt.decode(token, settings.SECRET_KEY, algorithms=[settings.ALGORITHM])
        login: str = payload.get("sub")
        if login is None or payload.get('type') != 'refresh':
            raise credentials_exception
        token_data = asu.TokenData(login=login)
    except JWTError:
        raise credentials_exception
    user = get_user_by_login(db, login=token_data.login)
    if user is None:
        raise credentials_exception

    access_token_expires = timedelta(minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_token(
        data={"sub": user.login, 'type': 'access'}, expires_delta=access_token_expires
    )    
    return asu.NewTokenData(access_token=access_token, user=user)       


def create_token(data: dict, expires_delta: Optional[timedelta] = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=15)
    to_encode.update({"exp": expire})
    encoded_jwt = jwt.encode(to_encode, settings.SECRET_KEY, algorithm=settings.ALGORITHM)
    return encoded_jwt


                     
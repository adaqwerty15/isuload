import app.models.isuload as ami
import app.models.consts as amc
import app.schemas.users as asu
import app.settings as settings

import app.control.dirs as acd
import app.control.loadcalc as aclc
import app.control.load as acl
import app.control.users as acu
import app.control.plans as acp
import copy, uuid

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func
from sqlalchemy.dialects import postgresql
from sqlalchemy import update
from sqlalchemy import delete, or_, and_
from sqlalchemy.orm import aliased


def approve_version(db, version, comment, own, other, user):
    ver = db.query(ami.LoadVersion).get({"id":version})

    if own:
        ids = []
        subq = db.query(ami.LoadVersion.id).\
                  filter(ami.LoadVersion.year == ver.year).\
                  filter(ami.LoadVersion.faculty_id == ver.faculty_id).\
                  all()                 
        for row in subq:
            ids.append(row[0])                 

        stmt = update(ami.VersionHistory).\
               where(ami.VersionHistory.version.in_(ids)).\
               values(active_own=False).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()
    else:
        stmt = update(ami.VersionHistory).\
               where(ami.VersionHistory.version==version).\
               values(active_own=False).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()


    if other:
        ids = []
        subq = db.query(ami.LoadVersion.id).\
                  filter(ami.LoadVersion.year == ver.year).\
                  filter(ami.LoadVersion.faculty_id == ver.faculty_id).\
                  all()                 
        for row in subq:
            ids.append(row[0])                 

        stmt = update(ami.VersionHistory).\
               where(ami.VersionHistory.version.in_(ids)).\
               values(active_other=False).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()
    else:
        stmt = update(ami.VersionHistory).\
               where(ami.VersionHistory.version==version).\
               values(active_other=False).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()


    if own or other:
        st = ami.VersionHistory(timestamp=datetime.now(), 
                                version=version,
                                user_id=user.id,
                                active_own=own,
                                active_other=other,
                                comment=comment.comment)
        db.add(st)
        db.commit()
        db.refresh(st)      


    return acl.get_load_versions(db, ver.faculty_id, ver.year)


def set_current(db, version):
    ver = db.query(ami.LoadVersion).get({"id":version})
    stmt = update(ami.LoadVersion).\
           where(ami.LoadVersion.faculty_id == ver.faculty_id).\
           where(ami.LoadVersion.year == ver.year).\
           values(current=False).\
           execution_options(synchronize_session="fetch")
    res = db.execute(stmt)
    db.commit()

    ver.current = True
          
    db.commit()
    db.refresh(ver)

    return acl.get_load_versions(db, ver.faculty_id, ver.year)


def calc_load(db, up_ver, new_ver, faculty_id, year, ver_name):
    v_to = 0
    v_in = 0

    if (up_ver > 0):
        v_in = up_ver

    if (up_ver < 0 or new_ver):
        current = False
        if new_ver:
            sel = db.query(func.count(ami.LoadVersion.id)).\
                     filter(ami.LoadVersion.year == year).\
                     filter(ami.LoadVersion.faculty_id == faculty_id).\
                     first()

            if sel[0]==0:
                current = True

        nw = ami.LoadVersion(faculty_id=faculty_id,
                             year=year,
                             name=ver_name,
                             current=current)
        db.add(nw)
        db.commit()
        db.refresh(nw)
        v_to = nw.id

    else:
        v_to = v_in

    strokes = []
    
    if (up_ver < 0):
        sel = db.query(ami.LessonPlan).\
                 join(ami.Profile).\
                 join(ami.Direction).\
                 filter(ami.Direction.faculty_id == faculty_id).\
                 filter(ami.LessonPlan.year == year).\
                 all()

        for row in sel:
            create_profile_load(db, row.profile_id, row.course, row.id, year, v_to, faculty_id)

    else:
      del_sub = []
      up_sub = []
      pract = {}
      del_loads = []

      copy = db.execute("""
                        SELECT (CASE
                            WHEN gpbc IS NOT NULL THEN gpbc
                            WHEN gcbc IS NOT NULL THEN gcbc
                            WHEN gsbc IS NOT NULL THEN gsbc
                            ELSE NULL
                        END) AS bc,
                       (CASE
                            WHEN gpvc IS NOT NULL THEN gpvc
                            WHEN gcvc IS NOT NULL THEN gcvc
                            WHEN gsvc IS NOT NULL THEN gsvc
                            ELSE NULL
                        END) AS vc,
                       (CASE
                            WHEN gpbf IS NOT NULL THEN gpbf
                            WHEN gcbf IS NOT NULL THEN gcbf
                            WHEN gsbf IS NOT NULL THEN gsbf
                            ELSE NULL
                        END) AS bf,
                       (CASE
                            WHEN gpvf IS NOT NULL THEN gpvf
                            WHEN gcvf IS NOT NULL THEN gcvf
                            WHEN gsvf IS NOT NULL THEN gsvf
                            ELSE NULL
                        END) AS vf,
                       res2.id,
                       res2.uuid,
                       res2.load_id,
                       res2.code,
                       res2.name,
                       res2.sem,
                       res2.budzhet,
                       res2.department,
                       res2.choice,
                       res2.count_students,
                       res2.count_groups,
                       res2.teacher_id,
                       res2.group_id,
                       res2.group_type,
                       res2.alias_name,
                       res2.count_hours,
                       res2.hours_type,
                       res2.is_own,
                       res2.active,
                       res2.current_faculty,
                       res2.add_inf,
                       res2.object_type,
                       res2.flow_id,
                       coalesce(res2.work_count, 0) as work_count,
                       res2.fg,
                       res2.gt,
                       res2.main,
                       res2.profile_id,
                       res2.course,
                       res2.own_faculty,
                       res2.count_foreign,
                       res2.show
                FROM
                  (SELECT gp.budzhet_count AS gpbc,
                          gp.vnebudzhet_count AS gpvc,
                          gp.budzhet_foreign AS gpbf,
                          gp.vnebudzhet_foreign AS gpvf,
                          gc.budzhet_count AS gcbc,
                          gc.vnebudzhet_count AS gcvc,
                          gc.budzhet_foreign AS gcbf,
                          gc.vnebudzhet_foreign AS gcvf,
                          gs.budzhet_count AS gsbc,
                          gs.vnebudzhet_count AS gsvc,
                          gs.budzhet_foreign AS gsbf,
                          gs.vnebudzhet_foreign AS gsvf,
                          strokes.id,
                          strokes.uuid,
                          strokes.load_id,
                          strokes.code,
                          strokes.name,
                          strokes.sem,
                          strokes.budzhet,
                          strokes.department,
                          strokes.choice,
                          strokes.count_students,
                          strokes.count_groups,
                          strokes.teacher_id,
                          strokes.group_id,
                          strokes.group_type,
                          strokes.alias_name,
                          strokes.count_hours,
                          strokes.hours_type,
                          strokes.is_own,
                          strokes.active,
                          strokes.current_faculty,
                          strokes.add_inf,
                          strokes.object_type,
                          strokes.flow_id,
                          strokes.work_count,
                          strokes.fg,
                          strokes.gt,
                          strokes.main,
                          strokes.profile_id,
                          strokes.course,
                          strokes.own_faculty,
                          strokes.count_foreign,
                          strokes.show
                   FROM
                     (SELECT ls.id,
                             ls.uuid,
                             ls.load_id,
                             ls.code,
                             ls.name,
                             ls.sem,
                             ls.budzhet,
                             ls.department,
                             ls.choice,
                             ls.count_students,
                             ls.count_groups,
                             ls.teacher_id,
                             ls.group_id,
                             ls.group_type,
                             ls.alias_name,
                             ls.count_hours,
                             ls.hours_type,
                             ls.is_own,
                             ls.active,
                             ls.current_faculty,
                             ls.add_inf,
                             ls.object_type,
                             ls.flow_id,
                             reslp.work_count,
                             res.group_id AS fg,
                             res.group_type AS gt,
                             flows.main,
                             load.profile_id,
                             load.course,
                             load.own_faculty,
                             ls.count_foreign,
                             ls.show
                      FROM loadstrokes AS ls
                      LEFT JOIN (flows
                                 LEFT JOIN
                                   (SELECT ls.id,
                                           ls.group_id,
                                           ls.group_type
                                    FROM loadstrokes AS ls) AS res ON res.id=flows.main) ON ls.flow_id=flows.id
                      LEFT JOIN
                        (SELECT *
                         FROM lessonplanstroke AS lp,
                              lessonplanworks AS lw
                         WHERE lw.lessonplanstroke_id=lp.id) AS reslp ON reslp.uuid=ls.uuid,
                                                                         LOAD
                      WHERE load.version=:ver
                        AND ls.load_id=load.id
                        AND ((reslp.work_type=101
                              AND ls.hours_type='lek')
                             OR (reslp.work_type=102
                                 AND ls.hours_type='lab')
                             OR ((reslp.work_type=103
                                  OR reslp.work_type=104)
                                 AND ls.hours_type='pract')
                             OR ls.hours_type like 'other%') ) AS strokes
                   LEFT JOIN groups_profile AS gp ON strokes.group_type='profile'
                   AND strokes.group_id=gp.id
                   LEFT JOIN subgroups AS gs ON strokes.group_type='subgroup'
                   AND strokes.group_id=gs.id
                   AND gs.active=TRUE
                   LEFT JOIN groups_choice AS gc ON strokes.group_type='choice'
                   AND strokes.group_id=gc.id) AS res2;
                  """, {'ver': v_in})

      for row in copy:
        if (row.bc == None and row.group_type == "profile"):
            if row.load_id not in del_loads:
                del_loads.append(row.load_id)

        if row.bc == None:
            if row.group_type == 'subgroup':
                  del_sub += {"uuid": row.uuid, "group_id": row.group_id, "hours_type": row.hours_type}
            elif not new_ver:
                  stmt = delete(ami.LoadStroke).\
                         where(ami.LoadStroke.id == row.id).\
                         execution_options(synchronize_session="fetch")
                  db.execute(stmt)
                  db.commit()
        else:          
            count_students = row.bc
            if (not row.own_faculty or row.object_type in [3, 31, 6] and row.active):
              count_students = row.count_students
            elif not row.budzhet:
              count_students = row.vc

            count_foreign = row.bf
            if (not row.own_faculty or row.object_type in [3, 31, 6] and row.active):
              count_foreign = row.count_foreign
            elif not row.budzhet:
              count_foreign = row.vf

            if (row.group_type == 'subgroup' and row.object_type == 2):
              count_students = 0
              count_foreign = 0  

            grc = 0
            if (row.group_type == "subgroup" or row.is_own or not row.own_faculty or 
                row.object_type in [3, 31, 6] and row.teacher_id != -1):
                grc = row.count_groups
            elif (row.budzhet):
              grc = 1 if (row.bc >= row.vc and row.bc > 0) else 0
            elif (not row.budzhet):
              grc = 1 if row.vc > row.bc else 0

            main = row.main

            count_hours = row.work_count*grc

            if ((row.count_groups == grc) or (row.fg and (row.fg != row.group_id or row.gt != row.group_type))
                or (not row.active and row.object_type == 2) or (row.group_type == 'subgroup')):
              count_hours = row.count_hours

            if (row.object_type in [3, 31, 6] and row.active):
              key = "{}_{}_{}".format(row.uuid, row.group_type, row.group_id)
              if key not in pract:
                pract[key] = []
              pract[key].append({"uuid": row.uuid, "group_id": row.group_id, "group_type": row.group_type,
                                 "bc": row.bc, "vc": row.vc, "bf": row.bf, "vf": row.vf,
                                 "count_students": count_students, "count_foreign": count_foreign,
                                 "budzhet": row.budzhet})

            if (not row.active and row.object_type == 2 and grc != row.count_groups):
              up_sub.append({"uuid": row.uuid, "group_id": row.group_id, "hours_type": row.hours_type,
                             "count": row.work_count*grc, "grc": grc, "object_type": row.object_type, "budzhet": row.budzhet})

            if (count_students != row.count_students or grc != row.count_groups or
                count_foreign != row.count_foreign):
                if row.fg:
                  if (row.main != row.id and row.count_groups==0 and grc==1 and
                     row.fg==row.group_id and row.gt==row.group_type):
                    if not new_ver:
                        stmt = update(ami.Flow).\
                               where(ami.Flow.id == row.flow_id).\
                               values(main = row.id).\
                               execution_options(synchronize_session="fetch")

                        res = db.execute(stmt)
                        db.commit()
                    main = row.id
                if not new_ver:
                  print(count_hours)
                  stmt = update(ami.LoadStroke).\
                         where(ami.LoadStroke.id == row.id).\
                         values(count_students=count_students, count_groups=grc, 
                                count_hours=count_hours, count_foreign=count_foreign).\
                         execution_options(synchronize_session="fetch")

                  res = db.execute(stmt)
                  db.commit()

            if (count_students != None or row.group_type != "subgroup" and new_ver):
              strokes.append({"stroke": row, "count_students": count_students, "count_groups": grc,
                              "count_hours": count_hours, "main": main, "count_foreign": count_foreign})

      loads_to_del = []
      
      for l in del_loads:
        loadsel = db.query(ami.Load).get({"id":l})
        grscount = db.query(func.count(ami.GroupProfile.id)).\
                      filter(ami.GroupProfile.year == loadsel.year).\
                      filter(ami.GroupProfile.course == loadsel.course).\
                      filter(ami.GroupProfile.profile_id == loadsel.profile_id).\
                      first()

        if grscount[0]==0:
            loads_to_del.append(l)              


      if new_ver:
        mains = {}
        copyload = db.query(ami.Load).filter(ami.Load.version==v_in).all()
        for row in copyload:
          if row.id not in loads_to_del:  
              load = ami.Load(profile_id=row.profile_id, course=row.course, year=row.year,
                              version=v_to, own_faculty=row.own_faculty)
              db.add(load)
              db.commit()
              db.refresh(load)

              nstrokes = [x for x in strokes if (x["stroke"].profile_id == row.profile_id and x["stroke"].course == row.course)]

              for stroke in nstrokes:
                st = ami.LoadStroke(uuid=stroke["stroke"].uuid,
                                    load_id=load.id,
                                    code=stroke["stroke"].code,
                                    name=stroke["stroke"].name,
                                    sem=stroke["stroke"].sem,
                                    budzhet=stroke["stroke"].budzhet,
                                    department=stroke["stroke"].department,
                                    choice=stroke["stroke"].choice,
                                    count_students=stroke["count_students"],
                                    count_groups=stroke["count_groups"],
                                    teacher_id=stroke["stroke"].teacher_id,
                                    group_id=stroke["stroke"].group_id,
                                    group_type=stroke["stroke"].group_type,
                                    alias_name=stroke["stroke"].alias_name,
                                    count_hours=stroke["count_hours"],
                                    hours_type=stroke["stroke"].hours_type,
                                    is_own=stroke["stroke"].is_own,
                                    active=stroke["stroke"].active,
                                    current_faculty=stroke["stroke"].current_faculty,
                                    add_inf=stroke["stroke"].add_inf,
                                    object_type=stroke["stroke"].object_type,
                                    flow_id=stroke["stroke"].flow_id,
                                    count_foreign=stroke["count_foreign"],
                                    show=stroke["stroke"].show)

                db.add(st)
                db.commit()
                db.refresh(st)

                if (stroke["main"] == stroke["stroke"].id):
                  mains[stroke["stroke"].flow_id] = st.id
        
        copyflows = db.query(ami.Flow).filter(ami.Flow.load_version == v_in).all()
        for row in copyflows:
          fl = ami.Flow(type=row.type, load_version=v_to,
                        name=row.name, department=row.department,
                        main=mains[row.id])
          db.add(fl)
          db.commit()
          db.refresh(fl)

          load_ids = db.query(ami.Load.id).filter(ami.Load.version==v_to).all()
          l_ids = [x[0] for x in load_ids]

          stmt = update(ami.LoadStroke).\
                 where(ami.LoadStroke.flow_id == row.id).\
                 where(ami.LoadStroke.load_id.in_(l_ids)).\
                 values(flow_id=fl.id).\
                 execution_options(synchronize_session="fetch")

          res = db.execute(stmt)
          db.commit()

        copycolumns = db.execute("""
                                  SELECT DISTINCT calc_id,
                                        value,
                                        UUID,
                                        group_id,
                                        group_type,
                                        budzhet,
                                        teacher_id
                                  FROM loadcolumns
                                  WHERE UUID in
                                      (SELECT ls.uuid
                                       FROM loadstrokes AS ls,
                                            LOAD
                                       WHERE load.id=ls.load_id
                                         AND load.version=:ver
                                         AND load.own_faculty=FALSE )
                                    AND load_version=:ver;
                                 """, {"ver": v_in})

        for row in copycolumns:
          lc = ami.LoadColumn(calc_id=row.calc_id,
                              value=row.value,
                              uuid=row.uuid,
                              group_id=row.group_id,
                              group_type=row.group_type,
                              budzhet=row.budzhet,
                              load_version=v_to,
                              teacher_id=row.teacher_id,
                              hand=row.hand)

          db.add(lc)
          db.commit()
          db.refresh(lc)

      else:
        stmt = delete(ami.LoadStroke).\
               where(ami.LoadStroke.load_id.in_(loads_to_del)).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

        stmt = delete(ami.Load).\
               where(ami.Load.id.in_(loads_to_del)).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()    

      for up in up_sub:
        gr_ids = db.query(ami.SubGroup.id).filter(ami.SubGroup.parent_id == up["group_id"]).all()
        gr_ids = [x[0] for x in gr_ids]

        ls_ids = db.query(ami.LoadStroke.id).\
                    join(ami.Load).\
                    filter(ami.Load.version == v_to).\
                    filter(ami.LoadStroke.uuid == up["uuid"]).\
                    filter(ami.LoadStroke.group_type == 'subgroup').\
                    filter(ami.LoadStroke.hours_type == up["hours_type"]).\
                    filter(ami.LoadStroke.group_id.in_(gr_ids)).\
                    filter(ami.LoadStroke.budzhet == up["budzhet"]).\
                    all()
        lss_ids = [x[0] for x in ls_ids]

        if up["object_type"] == 2:
          stmt = update(ami.LoadStroke).\
                 where(ami.LoadStroke.id.in_(lss_ids)).\
                 values(count_hours=up["count"]).\
                 execution_options(synchronize_session="fetch")

          res = db.execute(stmt)
          db.commit()
        else:
          stmt = update(ami.LoadStroke).\
                 where(ami.LoadStroke.id.in_(lss_ids)).\
                 values(count_hours=up["count"], count_groups=up["grc"]).\
                 execution_options(synchronize_session="fetch")

          res = db.execute(stmt)
          db.commit()

      for dels in del_sub:
        p_ids = db.query(ami.SubGroup.parent_id).filter(ami.SubGroup.id == dels["group_id"]).first()
        p_id = p_ids[0]

        if (p_id):
            acl.join_subgroups(dels["uuid"], v_to, p_id, dels["hours_type"])
        else:
            ls_ids = db.query(ami.LoadStroke.id).\
                        join(ami.Load).\
                        filter(ami.Load.version == v_to).\
                        filter(ami.LoadStroke.uuid == dels["uuid"]).\
                        filter(ami.LoadStroke.group_type == 'subgroup').\
                        filter(ami.LoadStroke.hours_type == dels["hours_type"]).\
                        filter(ami.LoadStroke.group_id == dels["group_id"]).\
                        all()
            lss_ids = [x[0] for x in ls_ids]

            stmt = delete(ami.LoadStroke).\
                   where(ami.LoadStroke.id.in_(lss_ids)).\
                   execution_options(synchronize_session="fetch")
            db.execute(stmt)
            db.commit()

      for key in pract:
        strspr = pract[key]

        strs_vn = [x for x in strspr if not x["budzhet"]]
        strs_b = [x for x in strspr if x["budzhet"]]

        count_students_vn = [x["count_students"] for x in strs_vn]
        count_students_b = [x["count_students"] for x in strs_b]

        sum_v = sum(count_students_vn)
        sum_b = sum(count_students_b)

        sums = [sum_b, sum_v]
        cont = [None, None]

        if (len(strs_b) > 0):
            cont[0] = strs_b[0]

        if (len(strs_vn) > 0):
            cont[1] = strs_vn[0]

        for idx, c in enumerate(cont):
            if c:
                p = c["bc"] if c["budzhet"] else c["vc"]
                strokes_changed = strs_b if c["budzhet"] else strs_vn

                if (sums[idx] != p):
                    if (len(strokes_changed) == 1):
                        ls_ids = db.query(ami.LoadStroke.id).\
                                    join(ami.Load).\
                                    filter(ami.Load.version == v_to).\
                                    filter(ami.LoadStroke.uuid == c["uuid"]).\
                                    filter(ami.LoadStroke.group_type == c["group_type"]).\
                                    filter(ami.LoadStroke.budzhet == c["budzhet"]).\
                                    filter(ami.LoadStroke.group_id == c["group_id"]).\
                                    all()
                        lss_ids = [x[0] for x in ls_ids]

                        stmt = update(ami.LoadStroke).\
                               where(ami.LoadStroke.id.in_(lss_ids)).\
                               values(count_students=p).\
                               execution_options(synchronize_session="fetch")

                        res = db.execute(stmt)
                        db.commit()

                    elif (len(strokes_changed) > 1):
                        ls_ids = db.query(ami.LoadStroke.id).\
                                    join(ami.Load).\
                                    filter(ami.Load.version == v_to).\
                                    filter(ami.LoadStroke.uuid == c["uuid"]).\
                                    filter(ami.LoadStroke.group_type == c["group_type"]).\
                                    filter(ami.LoadStroke.budzhet == c["budzhet"]).\
                                    filter(ami.LoadStroke.group_id == c["group_id"]).\
                                    filter(ami.LoadStroke.teacher_id == -1).\
                                    all()
                        lss_ids = [x[0] for x in ls_ids]

                        if (sums[idx] < p):
                            stmt = update(ami.LoadStroke).\
                                   where(ami.LoadStroke.id.in_(lss_ids)).\
                                   values(count_students=p - sums[idx]).\
                                   execution_options(synchronize_session="fetch")

                            res = db.execute(stmt)
                            db.commit()

                        else:
                            stmt = update(ami.LoadStroke).\
                                   where(ami.LoadStroke.id.in_(lss_ids)).\
                                   values(count_students=p).\
                                   execution_options(synchronize_session="fetch")

                            res = db.execute(stmt)
                            db.commit() 

                            ld_ids = db.query(ami.LoadStroke.id).\
                                        join(ami.Load).\
                                        filter(ami.Load.version == v_to).\
                                        filter(ami.LoadStroke.uuid == c["uuid"]).\
                                        filter(ami.LoadStroke.group_type == c["group_type"]).\
                                        filter(ami.LoadStroke.budzhet == c["budzhet"]).\
                                        filter(ami.LoadStroke.group_id == c["group_id"]).\
                                        filter(ami.LoadStroke.teacher_id != -1).\
                                        all()
                            lds_ids = [x[0] for x in ld_ids]

                            stmt = delete(ami.LoadStroke).\
                                   where(ami.LoadStroke.id.in_(lds_ids)).\
                                   execution_options(synchronize_session="fetch")
                            db.execute(stmt)
                            db.commit()

      no_added_plans_q = db.execute("""
                                    SELECT id,
                                           lessonplan.course,
                                           lessonplan.profile_id
                                    FROM lessonplan,

                                      (SELECT course,
                                              profile_id
                                       FROM lessonplan
                                       EXCEPT SELECT course,
                                                     profile_id
                                       FROM LOAD
                                       WHERE load.version=:ver) AS res
                                    WHERE lessonplan.course=res.course
                                      AND lessonplan.profile_id=res.profile_id
                                      AND lessonplan.year=:year
                                    """, {'ver': v_in, 'year': year})

      for row in no_added_plans_q:
        create_profile_load(db, row.profile_id, row.course, row.id, year, v_to, faculty_id)

      new_groups_q = db.execute("""
                                SELECT groups_profile.id,
                                       groups_profile.profile_id,
                                       groups_profile.course,
                                       groups_profile.year,
                                       budzhet_count,
                                       vnebudzhet_count,
                                       budzhet,
                                       lp.id AS lip,
                                       budzhet_foreign,
                                       vnebudzhet_foreign
                                FROM groups_profile,
                                     lessonplan AS lp
                                WHERE groups_profile.year=:year
                                  AND groups_profile.profile_id in
                                    (SELECT profile_id
                                     FROM profiles_full
                                     WHERE faculty_id=:faculty_id)
                                  AND groups_profile.id not in
                                    (SELECT DISTINCT group_id
                                     FROM loadstrokes AS ls,
                                          LOAD
                                     WHERE group_type='profile'
                                       AND load.version=:ver
                                       AND ls.load_id=load.id)
                                  AND groups_profile.profile_id=lp.profile_id
                                  AND groups_profile.year=lp.year
                                  AND groups_profile.course=lp.course;
                                """, {'ver': v_in, 'year': year, 'faculty_id': faculty_id})

      for row in new_groups_q:
        group = {"id": row.id, "profile_id": row.profile_id, "course": row.course,
                 "year": row.year, "lp_id": row.lip}
        create_group_load(db, group, v_to, v_in, faculty_id)    

    aclc.load_calc_update(db, v_to)
    return acl.get_load_versions(db, faculty_id, year)            


def create_profile_load(db, profile_id, course, lp_id, year, version, faculty_id):
    pl, load_id = add_load_profile(db, profile_id, course, lp_id, year, version, faculty_id)
    add_strokes(db, pl, load_id)


def create_group_load(db, group, version, version_old, faculty_id):
    pl, load_id = add_load_group(db, group, version, version_old, faculty_id)
    add_strokes(db, pl, load_id)    


def add_load_profile(db, profile_id, course, lp_id, year, version, faculty_id):
    deps = acd.get_departments(db, faculty_id)
    deps_numbers = [d.number for d in deps]

    cont = db.query(ami.GroupProfile).\
              filter(ami.GroupProfile.profile_id == profile_id).\
              filter(ami.GroupProfile.course == course).\
              filter(ami.GroupProfile.year == year).\
              all()

    planstrokes = []
    load_id = -1

    if len(cont) > 0:
        all_cont = db.query(func.sum(ami.GroupProfile.budzhet_count),
                            func.sum(ami.GroupProfile.vnebudzhet_count),
                            func.sum(ami.GroupProfile.budzhet_foreign),
                            func.sum(ami.GroupProfile.vnebudzhet_foreign)).\
                      filter(ami.GroupProfile.profile_id == profile_id).\
                      filter(ami.GroupProfile.course == course).\
                      filter(ami.GroupProfile.year == year).\
                      first()

        add_cont = db.execute("""
                              SELECT CASE
                                   WHEN
                                          (SELECT count(groups_choice.uuid)
                                           FROM groups_choice,
                                                lessonplanstroke
                                           WHERE groups_choice.uuid=lessonplanstroke.uuid
                                             AND lessonplanstroke.lessonplan_id=:lpid)=0 THEN
                                          (SELECT count(lessonplanstroke)
                                           FROM lessonplanstroke
                                           WHERE choice=TRUE
                                             AND lessonplanstroke.lessonplan_id=:lpid)
                                   ELSE 0
                                END;
                              """, {'lpid': lp_id})

        for row in add_cont:
          if (row[0] != 0):
            acp.add_groups_choice(db, lp_id)

        load = ami.Load(profile_id=profile_id, course=course, year=year,
                        version=version, own_faculty=True)
        db.add(load)
        db.commit()
        db.refresh(load)

        load_id = load.id

        planquery = db.execute("""
                                SELECT lessonplanstroke.uuid,
                                       code,
                                       name,
                                       sem,
                                       department,
                                       choice,
                                       object_type,
                                       SUM (CASE
                                                WHEN work_type=101 THEN work_count
                                                ELSE 0
                                            END) AS lecture_count,
                                           SUM (CASE
                                                    WHEN work_type=102 THEN work_count
                                                    ELSE 0
                                                END) AS lab_count,
                                               SUM (CASE
                                                        WHEN work_type=103
                                                             OR work_type=104 THEN work_count
                                                        ELSE 0
                                                    END) AS pract_count,
                                                   SUM (CASE
                                                            WHEN work_type=51 THEN work_count
                                                            ELSE 0
                                                        END) AS add_inf,
                                                       gc.id AS gid,
                                                       gc.budzhet_count,
                                                       gc.vnebudzhet_count,
                                                       gc.budzhet_foreign,
                                                       gc.vnebudzhet_foreign
                                FROM lessonplanstroke
                                LEFT JOIN groups_choice AS gc ON lessonplanstroke.uuid=gc.uuid,
                                                                 lessonplanworks
                                WHERE lessonplanworks.lessonplanstroke_id=lessonplanstroke.id
                                  AND lessonplan_id=:lpid
                                GROUP BY lessonplanstroke.uuid,
                                         sem,
                                         code,
                                         name,
                                         department,
                                         choice,
                                         object_type,
                                         gc.id,
                                         gc.budzhet_count,
                                         gc.vnebudzhet_count,
                                         gc.budzhet_foreign,
                                         gc.vnebudzhet_foreign;
                               """, {'lpid': lp_id})

        for row in planquery:
          own = True if (row.department in deps_numbers or row.object_type in [3, 6, 31]) else False

          if row.choice:
            if all_cont[0] > 0:
              gc = 1 if (row.budzhet_count >= row.vnebudzhet_count and row.budzhet_count > 0) else 0
              planstrokes.append({"stroke": row, "gcount": gc, "count": row.budzhet_count, "count_foreign": row.budzhet_foreign,
                                  "budzhet": True, "group_type": "choice", "own": own, "group_id": row.gid})
            if all_cont[1] > 0:
              gc = 1 if (row.budzhet_count < row.vnebudzhet_count) else 0
              planstrokes.append({"stroke": row, "gcount": gc, "count": row.vnebudzhet_count, "count_foreign": row.vnebudzhet_foreign,
                                  "budzhet": False, "group_type": "choice", "own": own, "group_id": row.gid})

          else:
            for group in cont:
              bg = 1 if (group.budzhet) else 0
              vbg = 0 if (group.budzhet) else 1

              if (group.budzhet_count > 0):
                planstrokes.append({"stroke": row, "gcount": bg, "count": group.budzhet_count, "count_foreign": group.budzhet_foreign,
                                    "budzhet": True, "group_type": "profile", "own": own, "group_id": group.id})
              if (group.vnebudzhet_count > 0):
                planstrokes.append({"stroke": row, "gcount": vbg, "count": group.vnebudzhet_count, "count_foreign": group.vnebudzhet_foreign,
                                    "budzhet": False, "group_type": "profile", "own": own, "group_id": group.id})

    return planstrokes, load_id


def add_load_group(db, group, version, version_old, faculty_id):
    deps = acd.get_departments(db, faculty_id)
    deps_numbers = [d.number for d in deps]

    planstrokes = []

    cont = db.query(ami.GroupProfile).\
              filter(ami.GroupProfile.profile_id == group["profile_id"]).\
              filter(ami.GroupProfile.course == group["course"]).\
              filter(ami.GroupProfile.year == group["year"]).\
              all()

    if group:
        all_cont = db.query(func.sum(ami.GroupProfile.budzhet_count),
                            func.sum(ami.GroupProfile.vnebudzhet_count),
                            func.sum(ami.GroupProfile.budzhet_foreign),
                            func.sum(ami.GroupProfile.vnebudzhet_foreign)).\
                      filter(ami.GroupProfile.profile_id == group["profile_id"]).\
                      filter(ami.GroupProfile.course == group["course"]).\
                      filter(ami.GroupProfile.year == group["year"]).\
                      first()

        load_query = db.query(ami.Load.id).\
                        filter(ami.Load.profile_id == group["profile_id"]).\
                        filter(ami.Load.course == group["course"]).\
                        filter(ami.Load.version == version_old).\
                        filter(ami.Load.own_faculty == True).\
                        first()
        load_id = load_query[0]                          

        planquery = db.execute("""
                                SELECT *
                                FROM
                                  (SELECT lessonplanstroke.uuid,
                                          lessonplanstroke.code,
                                          lessonplanstroke.name,
                                          lessonplanstroke.sem,
                                          lessonplanstroke.department,
                                          lessonplanstroke.choice,
                                          lessonplanstroke.object_type,
                                          SUM (CASE
                                                   WHEN work_type=101 THEN work_count
                                                   ELSE 0
                                               END) AS lecture_count,
                                              SUM (CASE
                                                       WHEN work_type=102 THEN work_count
                                                       ELSE 0
                                                   END) AS lab_count,
                                                  SUM (CASE
                                                           WHEN work_type=103
                                                                OR work_type=104 THEN work_count
                                                           ELSE 0
                                                       END) AS pract_count,
                                                      SUM (CASE
                                                               WHEN work_type=51 THEN work_count
                                                               ELSE 0
                                                           END) AS add_inf,
                                                          gc.id AS gid,
                                                          gc.budzhet_count,
                                                          gc.vnebudzhet_count,
                                                          gc.budzhet_foreign,
                                                          gc.vnebudzhet_foreign
                                   FROM lessonplanstroke
                                   LEFT JOIN groups_choice AS gc ON lessonplanstroke.uuid=gc.uuid,
                                                                    lessonplanworks
                                   WHERE lessonplanworks.lessonplanstroke_id=lessonplanstroke.id
                                     AND lessonplan_id=:lpid
                                   GROUP BY lessonplanstroke.uuid,
                                            lessonplanstroke.sem,
                                            lessonplanstroke.code,
                                            lessonplanstroke.name,
                                            lessonplanstroke.department,
                                            lessonplanstroke.choice,
                                            lessonplanstroke.object_type,
                                            gc.id,
                                            gc.budzhet_count,
                                            gc.vnebudzhet_count,
                                            gc.budzhet_foreign,
                                            gc.vnebudzhet_foreign) AS res2
                                LEFT JOIN
                                  (SELECT UUID AS luuid
                                   FROM loadstrokes AS ls,
                                        LOAD
                                   WHERE ls.load_id=load.id
                                     AND load.version=:ver
                                   GROUP BY luuid) AS res ON res2.uuid=res.luuid;
                               """, {'lpid': group["lp_id"], 'ver': version})

        for row in planquery:
          own = True if (row.department in deps_numbers or row.object_type in [3, 6, 31]) else False

          if row.choice:
            if all_cont[0] > 0 and row.uuid==None:
              gc = 1 if (row.budzhet_count >= row.vnebudzhet_count and row.budzhet_count > 0) else 0
              planstrokes.append({"stroke": row, "gcount": gc, "count": row.budzhet_count, "count_foreign": row.budzhet_foreign,
                                  "budzhet": True, "group_type": "choice", "own": own, "group_id": row.gid})
            if all_cont[1] > 0 and row.uuid==None:
              gc = 1 if (row.budzhet_count < row.vnebudzhet_count) else 0
              planstrokes.append({"stroke": row, "gcount": gc, "count": row.vnebudzhet_count, "count_foreign": row.vnebudzhet_foreign,
                                  "budzhet": False, "group_type": "choice", "own": own, "group_id": row.gid})

          else:
            for group in cont:
              bg = 1 if (group.budzhet) else 0
              vbg = 0 if (group.budzhet) else 1

              if (group.budzhet_count > 0):
                planstrokes.append({"stroke": row, "gcount": bg, "count": group.budzhet_count, "count_foreign": group.budzhet_foreign,
                                    "budzhet": True, "group_type": "profile", "own": own, "group_id": group.id})
              if (group.vnebudzhet_count > 0):
                planstrokes.append({"stroke": row, "gcount": vbg, "count": group.vnebudzhet_count, "count_foreign": group.vnebudzhet_foreign,
                                    "budzhet": False, "group_type": "profile", "own": own, "group_id": group.id})

    return planstrokes, load_id


def add_strokes(db, planstrokes, load_id):
  types = ["lek", "pract", "lab", "other"]

  for row in planstrokes:
   stcount = 0 
   for typest in types:
     count_hours = 0
     if typest == 'lek' and row["stroke"].lecture_count:
      count_hours = row["stroke"].lecture_count
     elif typest == 'pract' and row["stroke"].pract_count:
      count_hours = row["stroke"].pract_count
     elif typest == 'lab' and row["stroke"].lab_count:
      count_hours = row["stroke"].lab_count

     if count_hours > 0 or stcount==0 and typest=="other":  
        st = ami.LoadStroke(uuid=row["stroke"].uuid,
                           load_id=load_id,
                           code=row["stroke"].code,
                           name=row["stroke"].name,
                           sem=row["stroke"].sem,
                           budzhet=row["budzhet"],
                           department=row["stroke"].department,
                           choice=row["stroke"].choice,
                           count_students=row["count"],
                           count_groups=row["gcount"],
                           teacher_id=-1,
                           group_id=row["group_id"],
                           group_type=row["group_type"],
                           alias_name=row["stroke"].name,
                           count_hours=count_hours*row["gcount"],
                           hours_type=typest,
                           is_own=False,
                           active=True,
                           current_faculty=row["own"],
                           add_inf=row["stroke"].add_inf,
                           object_type=row["stroke"].object_type,
                           flow_id=-1,
                           count_foreign=row["count_foreign"],
                           show=True)

        db.add(st)
        db.commit()
        db.refresh(st)
        stcount += 1


def delete_version(db, version):
    ver = db.query(ami.LoadVersion).get({"id":version})
    all_ver = acl.get_load_version(db, version)
    
    faculty_id = ver.faculty_id
    year = ver.year

    if not (ver.current or all_ver.active_own or all_ver.active_other):
        ld_ids = db.query(ami.LoadStroke.id).\
                    join(ami.Load).\
                    filter(ami.Load.version == version).\
                    all()
        lds_ids = [x[0] for x in ld_ids]

        stmt = delete(ami.LoadStroke).\
               where(ami.LoadStroke.id.in_(lds_ids)).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

        stmt = delete(ami.Load).\
               where(ami.Load.version == version).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

        stmt = delete(ami.LoadColumn).\
               where(ami.LoadColumn.load_version == version).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

        stmt = delete(ami.LoadVersion).\
               where(ami.LoadVersion.id == version).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()

    return acl.get_load_versions(db, faculty_id, year)    




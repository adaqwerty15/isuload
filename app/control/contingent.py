import app.models.isuload as ami
import app.schemas.users as asu
import app.settings as settings

import app.schemas.contingent as asc

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func
from sqlalchemy.dialects import postgresql
from sqlalchemy import update
from sqlalchemy import delete
import copy


def get_profiles_tree(db: Session, faculty: int):
    res =  db.query(ami.Direction).filter(ami.Direction.faculty_id == faculty).\
                                   filter(ami.Direction.active==True).\
                                   order_by(ami.Direction.form_id, ami.Direction.shifr).all()
    forms = {}
    keys = {}
    for row in res:
        if row.form_id not in forms:
            forms[row.form_id] = {"key": "{}".format(row.form_id), "label": row.form.name, 
                                  "selectable": False, "children": []}

        direction = {"key": "{}-{}".format(row.form_id, row.id), 
                     "label": "{} {}".format(row.shifr, row.name),
                     "selectable": False, "children": []}
        for p in row.profiles:
            keys[p.id] = "{}-{}".format(row.form_id, row.id)
            direction["children"].append({"key":"{}-{}-{}".format(row.form_id, row.id, p.id),
                                          "label": p.name, "selectable": True, "id": p.id})

        forms[row.form_id]["children"].append(direction)
    
    result = []	
    for row in forms:
        result.append(forms[row])		
    return {"tree": result, "keys": keys}


def get_all_groups(db: Session, profile_id: int, year: int):

    itog = {1: {}, 2: {}, 3: {}, 4: {}, 5: {}}

    for c in itog:
        itog[c]["common"] = {"bc": 0, "vc": 0, "bf": 0, "vf": 0}
        itog[c]["groups"] = []
        itog[c]["blocks"] = {}

    allstat = db.query(ami.GroupProfile.course,
                       func.sum(ami.GroupProfile.budzhet_count).label("bc"),
                       func.sum(ami.GroupProfile.vnebudzhet_count).label("vc"),
                       func.sum(ami.GroupProfile.budzhet_foreign).label("bf"),
                       func.sum(ami.GroupProfile.vnebudzhet_foreign).label("vf"),
                       ).\
                 filter(ami.GroupProfile.profile_id == profile_id).\
                 filter(ami.GroupProfile.year == year).\
                 group_by(ami.GroupProfile.course).\
                 all()

    for row in allstat:
        itog[row.course]["common"] = {"bc": row.bc, "vc": row.vc, "bf": row.bf, "vf": row.vf}           

    all_groups = db.query(ami.GroupProfile).\
                    filter(ami.GroupProfile.profile_id == profile_id).\
                    filter(ami.GroupProfile.year == year).\
                    all()          
    
    for row in all_groups:
        row.blocks = {}

        for sub in row.subgroups:
          if sub.active:  
            if sub.block_id not in row.blocks:
                row.blocks[sub.block_id] = []
  
            row.blocks[sub.block_id].append(sub)

        itog[row.course]["groups"].append(row)

    choice = db.query(ami.GroupChoice, ami.LessonPlanStroke, ami.LessonPlan.course).\
                join(ami.LessonPlanStroke, ami.GroupChoice.uuid == ami.LessonPlanStroke.uuid).\
                join(ami.LessonPlan, ami.LessonPlan.id == ami.LessonPlanStroke.lessonplan_id).\
                filter(ami.LessonPlan.year == year).\
                filter(ami.LessonPlan.profile_id == profile_id).\
                order_by(ami.GroupChoice.block, ami.LessonPlanStroke.name)

    uuids = []            

    for row in choice:
        if (row.GroupChoice.block not in itog[row.course]["blocks"]):
            itog[row.course]["blocks"][row.GroupChoice.block] = []   
        if (row.LessonPlanStroke.uuid) not in uuids:
          uuids.append(row.LessonPlanStroke.uuid)
          row.GroupChoice.removed = False
        else:
          row.GroupChoice.removed = True   
        itog[row.course]["blocks"][row.GroupChoice.block].append(row)         
    
    return itog 


def get_common(db, year, profile_id, course): 
    itog = {"bc": 0, "vc": 0, "bf": 0, "vf": 0}

    allstat = db.query(func.sum(ami.GroupProfile.budzhet_count).label("bc"),
                       func.sum(ami.GroupProfile.vnebudzhet_count).label("vc"),
                       func.sum(ami.GroupProfile.budzhet_foreign).label("bf"),
                       func.sum(ami.GroupProfile.vnebudzhet_foreign).label("vf"),
                       ).\
                 filter(ami.GroupProfile.profile_id == profile_id).\
                 filter(ami.GroupProfile.year == year).\
                 filter(ami.GroupProfile.course == course).\
                 all()

    for row in allstat:
        if row.bc:
          itog = {"bc": row.bc, "vc": row.vc, "bf": row.bf, "vf": row.vf}

    return itog


def add_profile_group(db: Session, profile_id: int, year: int, name: str, bc: int, vc: int,
                      bf: int, vf: int, course: int):
    budzhet = bc >= vc

    group = ami.GroupProfile(name=name, profile_id=profile_id, year=year,
                             budzhet_count=bc, vnebudzhet_count=vc, budzhet_foreign=bf,
                             vnebudzhet_foreign=vf, budzhet=budzhet, course=course)
    db.add(group)
    db.commit()
    db.refresh(group)   

    group.subgroups = []
    group.blocks = {} 

    itog = get_common(db, year, profile_id, course)                

    return {"group": group, "common": itog}


def delete_profile_group(db: Session, group_id: int):
    group = db.query(ami.GroupProfile).get({"id":group_id})
    year = group.year 
    course = group.course
    profile_id = group.profile_id

    stmt = delete(ami.SubGroup).\
           where(ami.SubGroup.parent_id == group_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)

    stmt = delete(ami.GroupProfile).\
           where(ami.GroupProfile.id == group_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)

    db.commit()

    itog = get_common(db, year, profile_id, course) 

    return {"group_id": group_id, "common": itog, "course": course}


def edit_profile_group_name(db: Session, new_name: str, group_id: int):
    group = db.query(ami.GroupProfile).get({"id":group_id})
    group.name = new_name
    db.commit()
    db.refresh(group)

    return group


def edit_subgroup_name(db: Session, new_name: str, subgroup_id: int):
    group = db.query(ami.SubGroup).get({"id":subgroup_id})
    group.name = new_name
    db.commit()
    db.refresh(group)

    group_parent = db.query(ami.GroupProfile).get({"id": group.parent_id})

    return {"group": group_parent, "subgroup": group}


def edit_profile_group_contingent(db: Session, group_id: int, bc: int, vc: int,
                                  bf: int, vf: int):
    budzhet = bc >= vc

    group = db.query(ami.GroupProfile).get({"id": group_id})
    group.budzhet_count = bc 
    group.vnebudzhet_count = vc 
    group.budzhet_foreign = bf
    group.vnebudzhet_foreign = vf
    group.budzhet = budzhet

    db.commit()
    db.refresh(group)

    itog = get_common(db, group.year, group.profile_id, group.course)

    return {"group": group, "common": itog}


def edit_subroup_contingent(db: Session, subgroup_id: int, bc: int, vc: int,
                             bf: int, vf: int):
    group = db.query(ami.SubGroup).get({"id": subgroup_id})
    group.budzhet_count = bc 
    group.vnebudzhet_count = vc 
    group.budzhet_foreign = bf
    group.vnebudzhet_foreign = vf

    db.commit()
    db.refresh(group)

    group_parent = db.query(ami.GroupProfile).get({"id": group.parent_id})

    return {"group": group_parent, "subgroup": group}


def disable_subgroup_block(db: Session, block_id: int):
    stmt = update(ami.SubGroup).\
           where(ami.SubGroup.block_id == block_id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return block_id


def add_subgroups_block(db: Session, group_id: int, subgroups):

    max_c = db.query(func.max(ami.SubGroup.block_id)).first()
    max_c = max_c[0] + 1

    subgroups_res = {max_c: []}

    for gr in subgroups:
        group = ami.SubGroup(name=gr.name, parent_id=group_id, block_id=max_c,
                             budzhet_count=gr.bc, vnebudzhet_count=gr.vc,
                             budzhet_foreign=gr.bf, vnebudzhet_foreign=gr.vf,
                             active=True)
        db.add(group)
        db.commit()
        db.refresh(group)
        subgroups_res[max_c].append(group)

    for row in subgroups_res[max_c]:
        row.id

    group_parent = db.query(ami.GroupProfile).get({"id": group_id})

    return {'group': group_parent, 'subgroups': subgroups_res}


def add_choice_group(db: Session, uuid: str, block: str):
    gc = ami.GroupChoice(uuid=uuid, budzhet_count=0,
                         vnebudzhet_count=0, budzhet_foreign=0,
                         vnebudzhet_foreign=0, block=block)

    db.add(gc)
    db.commit()
    db.refresh(gc)

    lp = db.query(ami.LessonPlanStroke).filter(ami.LessonPlanStroke.uuid==uuid).first()

    course = (int(lp.sem) + 1) // 2

    gc.removed = True

    group = {"GroupChoice": gc, "LessonPlanStroke": lp, "course": course}

    return {"group": group, "block": block}


def delete_choice_group(db: Session, group_id: int):
    stmt = delete(ami.GroupChoice).\
           where(ami.GroupChoice.id == group_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)

    db.commit()

    return group_id


def edit_groupchoice_contingent(db: Session, group_id: int, bc: int, vc: int,
                                bf: int, vf: int):
    group = db.query(ami.GroupChoice).get({"id": group_id})
    group.budzhet_count = bc 
    group.vnebudzhet_count = vc 
    group.budzhet_foreign = bf
    group.vnebudzhet_foreign = vf

    db.commit()
    db.refresh(group)

    return group


def generate_groups_auto(db: Session, year: int, faculty: int):

    plans = db.query(ami.LessonPlan).\
               join(ami.Profile).\
               join(ami.Direction).\
               filter(ami.Direction.faculty_id == faculty).\
               filter(ami.LessonPlan.year == year).\
               all()

    group_template_faculty = db.query(ami.FacultyNameVersion.group_template).\
                                join(ami.Faculty).\
                                filter(ami.Faculty.id == faculty).\
                                first()

    group_template_faculty = group_template_faculty[0] if group_template_faculty[0] else "0"                          
               
    for plan in plans:
        groups = db.query(func.count(ami.GroupProfile.id)).\
                    filter(ami.GroupProfile.profile_id == plan.profile_id).\
                    filter(ami.GroupProfile.year == plan.year).\
                    filter(ami.GroupProfile.course == plan.course).\
                    first()

        if groups[0] == 0:
            if (plan.bg == None):
                plan.bg = 0

            if (plan.vg == None):
                plan.vg = 0
                
            if (plan.bc == None):
                plan.bc = 0
                
            if (plan.vc == None):
                plan.vc = 0

            if (plan.bg > 0 or plan.vg > 0):
                gt = db.query(ami.Direction.group_template, ami.ConstrDirectory.name).\
                        join(ami.Profile).\
                        join(ami.ConstrDirectory, ami.ConstrDirectory.value == ami.Direction.qual_id).\
                        filter(ami.Profile.id == plan.profile_id).\
                        filter(ami.ConstrDirectory.type == "qualification").\
                        first()

                gtem = gt[0] if gt[0] else "0"
                qtem = gt[1][0]  

                i = 1   

                if (plan.bg > 0):
                    cs = plan.bc // plan.bg
                    for g in range(0, plan.bg):
                        count = cs if g>0 else cs+(plan.bc - cs*plan.bg)
                        vc = plan.vc if (plan.vg == 0 and g==0) else 0
                        name = "0{}{}{}{}-Д{}".format(group_template_faculty, plan.course, gtem, i, qtem)
                        group = ami.GroupProfile(name=name, profile_id=plan.profile_id, year=year,
                                                 budzhet_count=count, vnebudzhet_count=vc, budzhet_foreign=0,
                                                 vnebudzhet_foreign=0, budzhet=True, course=plan.course)
                        db.add(group)
                        db.commit()
                        db.refresh(group)
                        i += 1

                if (plan.vg > 0):
                    cs = plan.vc // plan.vg
                    for g in range(0, plan.vg):
                        count = cs if g>0 else cs+(plan.vc - cs*plan.vg)
                        bc = plan.bc if (plan.bg == 0 and g==0) else 0
                        name = "0{}{}{}{}-Д{}".format(group_template_faculty, plan.course, gtem, i, qtem)                      
                        group = ami.GroupProfile(name=name, profile_id=plan.profile_id, year=year,
                                                 budzhet_count=bc, vnebudzhet_count=count, budzhet_foreign=0,
                                                 vnebudzhet_foreign=0, budzhet=False, course=plan.course)
                        db.add(group)
                        db.commit()
                        db.refresh(group)
                        i += 1        


    return ""                     



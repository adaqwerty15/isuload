import app.models.isuload as ami
import app.models.consts as amc
import app.schemas.users as asu
import app.settings as settings

import app.schemas.load as asl
import app.control.dirs as acd
import app.control.loadcalc as aclc
import copy, uuid
import xlsxwriter

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File, Response
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func
from sqlalchemy.dialects import postgresql
from sqlalchemy import update
from sqlalchemy import delete, or_, and_
from sqlalchemy.orm import aliased
from io import BytesIO


def get_load_versions(db: Session, faculty: int, year: int):
    sub = db.query(ami.LoadVersion.id, 
                   ami.LoadVersion.year,
                   ami.LoadVersion.name,
                   ami.LoadVersion.current,
                   ami.VersionHistory.active_own,
                   ami.VersionHistory.active_other).\
             outerjoin(ami.VersionHistory, ami.LoadVersion.id == ami.VersionHistory.version).\
             filter(ami.LoadVersion.faculty_id == faculty).\
             filter(ami.LoadVersion.year == year).\
             subquery()

    res = db.query(sub.c.name, sub.c.id, sub.c.year, sub.c.current,
                   func.bool_or(sub.c.active_own).label("active_own"),
                   func.bool_or(sub.c.active_other).label("active_other")).\
             group_by(sub.c.name, sub.c.id, sub.c.year, sub.c.current).\
             order_by(sub.c.id).\
             all()         

    return res


def get_load_version(db: Session, version: int):
    sub = db.query(ami.LoadVersion.id, 
                   ami.LoadVersion.year,
                   ami.LoadVersion.name,
                   ami.LoadVersion.current,
                   ami.VersionHistory.active_own,
                   ami.VersionHistory.active_other).\
             outerjoin(ami.VersionHistory, ami.LoadVersion.id == ami.VersionHistory.version).\
             filter(ami.LoadVersion.id == version).\
             subquery()

    res = db.query(sub.c.name, sub.c.id, sub.c.year, sub.c.current,
                   func.bool_or(sub.c.active_own).label("active_own"),
                   func.bool_or(sub.c.active_other).label("active_other")).\
             group_by(sub.c.name, sub.c.id, sub.c.year, sub.c.current).\
             order_by(sub.c.id).\
             first()         

    return res    


def get_table_columns(db: Session, faculty: int, version_id: int, active_view: bool = True):
    version = db.query(ami.LoadVersion).get({"id":version_id})

    c2 = aliased(ami.ColumnLoad)

    colspan = 0
    
    cols = db.query(ami.ColumnLoad.id,
                    ami.ColumnLoad.name,
                    ami.ColumnLoad.alias,
                    c2.id.label("child_id"),
                    c2.name.label("child_name"),
                    c2.alias.label("child_alias")).\
              join(c2, or_(ami.ColumnLoad.id == c2.parent_id, 
                       and_(ami.ColumnLoad.id == c2.id, ami.ColumnLoad.parent_id == None))).\
              filter(ami.ColumnLoad.year == version.year).\
              filter(ami.ColumnLoad.faculty_id == faculty).\
              filter(ami.ColumnLoad.active == True).\
              filter(or_(ami.ColumnLoad.active_view == True, 
                         ami.ColumnLoad.active_view == active_view)).\
              filter(or_(c2.active_view == True,
                         c2.active_view == active_view)).\
              filter(c2.active == True).\
              order_by(ami.ColumnLoad.id, c2.id).\
              all()

    pc = {}
    ch = []
    
    for row in cols:
        if row.id not in pc:
            pc[row.id] = {"id": row.id, "name": row.name, "alias": row.alias, "children": []}
            colspan += 1

        if row.id != row.child_id:
            child = {"id": row.child_id, "name": row.child_name, "alias": row.child_alias}
            pc[row.id]["children"].append(child)
            if (len(pc[row.id]["children"]) > 1):
                colspan += 1
            ch.append(child)                 

    return {"columns": pc, "children": ch, "colspan": (colspan+14)}


def get_table_load(db: Session, version: int, onlyotherstudents: int, ownteachers: int, dep: int = None):
    ver = db.query(ami.LoadVersion).get({"id":version})
    ownt = True if (ownteachers == 1) else False
    otherst = True if (onlyotherstudents == 1) else False

    columnvaluesres = db.execute("""
                                 SELECT UUID,
                                        c_id,
                                        department,
                                        val,
                                        coalesce(formula_text, '') AS formula,
                                        budzhet,
                                        parent_id,
                                        coalesce(hand, FALSE) AS hand
                                 FROM
                                   (SELECT res.uuid,
                                           c_id,
                                           department,
                                           coalesce(sum(value), 0.0000) AS val,
                                           min(cc_id) AS cc_id,
                                           res.budzhet,
                                           res.parent_id,
                                           bool_or(hand) as hand
                                    FROM
                                      (SELECT DISTINCT ls.group_id,
                                                       ls.group_type,
                                                       ls.uuid,
                                                       c.id AS c_id,
                                                       coalesce(c.parent_id, c.id) AS parent_id,
                                                       ls.department,
                                                       ls.budzhet
                                       FROM loadstrokes AS ls,
                                            columns AS c,
                                            LOAD
                                       WHERE load.id=ls.load_id
                                         AND load.version=:ver
                                         AND ls.active=TRUE
                                         AND ls.current_faculty=:faculty
                                         AND c.active=TRUE
                                         AND c.active_view=TRUE
                                         AND c.year=:year
                                         AND (c.id not in
                                                (SELECT parent_id
                                                 FROM columns
                                                 WHERE parent_id IS NOT NULL)) ) AS res
                                    LEFT JOIN
                                      (SELECT group_id,
                                              group_type,
                                              budzhet,
                                              UUID,
                                              value,
                                              column_id,
                                              cc.id AS cc_id,
                                              lc.hand
                                       FROM loadcolumns AS lc,
                                            columns_calc AS cc
                                       WHERE lc.calc_id=cc.id
                                         AND lc.load_version=:ver) AS res2 ON res2.column_id=res.c_id
                                    AND res.uuid=res2.uuid
                                    AND res.group_id=res2.group_id
                                    AND res.group_type=res2.group_type
                                    AND res.budzhet=res2.budzhet
                                    GROUP BY res.uuid,
                                             c_id,
                                             department,
                                             res.budzhet,
                                             res.parent_id
                                             ) AS res3
                                 LEFT JOIN columns_calc AS cc ON cc.id=cc_id
                                 ORDER BY UUID,
                                          budzhet,
                                          parent_id,
                                          c_id;
                                 """, {'ver': version, 'faculty': ownt, 'year': ver.year})

    column_values_dict = {}

    for row in columnvaluesres:
        if row["uuid"] not in column_values_dict:
            column_values_dict[row["uuid"]] = []
        column_values_dict[row["uuid"]].append({"c_id": row["c_id"], "department": row["department"],
                                                "val": row["val"].normalize(), "formula": row["formula"],
                                                "budzhet": row["budzhet"], "parent_id": row["parent_id"],
                                                "hand": row["hand"]})

    profiles = db.query(ami.Profile).\
                  join(ami.Load).\
                  join(ami.Direction).\
                  filter(ami.Load.version == version).\
                  order_by(ami.Load.own_faculty.desc(), ami.Direction.form_id, ami.Direction.shifr)

    if (not ownt):
        profiles =  db.query(ami.Profile).\
                     join(ami.Load).\
                     join(ami.Direction).\
                     filter(ami.Load.version == version).\
                     filter(ami.Load.own_faculty == True).\
                     order_by(ami.Load.own_faculty.desc(), ami.Direction.form_id, ami.Direction.shifr)
    elif otherst:
        profiles =  db.query(ami.Profile).\
                  join(ami.Load).\
                  join(ami.Direction).\
                  filter(ami.Load.version == version).\
                  filter(ami.Load.own_faculty == False).\
                  order_by(ami.Load.own_faculty.desc(), ami.Direction.form_id, ami.Direction.shifr)

    profiles = profiles.all()                                           

    load = {} 
    directions = {}              

    for row in profiles:
        direction = row.direction
        if (direction.id not in directions):
            directions[direction.id] = {"id": direction.id,
                                        "name": direction.name,
                                        "shifr": direction.shifr,
                                        "faculty_id": direction.faculty_id,
                                        "form_id": direction.form_id,
                                        "profiles": {},
                                        "show_profiles": True,
                                        "nb": 0,
                                        "nvb": 0}

        if row.id not in directions[direction.id]["profiles"]:
            lsb = get_load_strokes(db, True, row.id, version, ownt, column_values_dict, dep)
            if len(lsb) > 0:
                directions[direction.id]["nb"] += 1
            lsvb = get_load_strokes(db, False, row.id, version, ownt, column_values_dict, dep)
            if len(lsvb) > 0:
                directions[direction.id]["nvb"] += 1
            directions[direction.id]["profiles"][row.id] = {"name": row.name,
                                                            "id": row.id,
                                                            "loadstrokes_budzhet": lsb,
                                                            "loadstrokes_vnebudzhet": lsvb,
                                                            "show_budzhet": True,
                                                            "show_vnebudzhet": True,
                                                            "show_strokes": True}

        faculty = row.direction.faculty
        if (faculty.id not in load):
            faculty_inf = acd.get_faculty_by_id(db, faculty.id, ver.year)
            load[faculty.id] = {"id": faculty.id,
                                "name": faculty_inf.name,
                                "forms": {},
                                "show_forms": True}
            
        form = row.direction.form
        if (form.value not in load[faculty.id]["forms"]):
            load[faculty.id]["forms"][form.value] = {"id": form.value,
                                                     "name": form.name,
                                                     "directions": [],
                                                     "show_dirs": True}


    for d in directions:
        dn = directions[d]
        load[dn["faculty_id"]]["forms"][dn["form_id"]]["directions"].append(dn)

    return load


def get_load_strokes(db, budzhet, profile_id, version_id, ownteachers, colsvalues, dep = None):
    depclause = "AND ls.department = {}".format(dep) if dep else ""
    ls = db.execute("""
                  SELECT course,
                         UUID,
                         code,
                         name,
                         sem,
                         department,
                         choice,
                         sum(count_students) as cs,
                         sum(count_groups) as cg,
                         sum(lecture_fact) as lf,
                         sum(practice_fact) as pf,
                         sum(lab_fact) as labf,
                         is_own,
                         active,
                         add_inf,
                         current_faculty,
                         lec_count,
                         pract_count,
                         object_type,
                         sum(CASE
                                 WHEN group_type='subgroup' THEN 1
                                 ELSE 0
                             END) AS is_subgroup,
                         alias_name,
                         own_faculty,
                         SHOW
                  FROM
                    (SELECT course,
                            UUID,
                            code,
                            name,
                            sem,
                            department,
                            choice,
                            count_students,
                            count_groups,
                            sum(lec) AS lecture_fact,
                            sum(pract) AS practice_fact,
                            sum(lab) AS lab_fact,
                            is_own,
                            active,
                            (CASE
                                 WHEN object_type in (3,
                                                      6,
                                                      31) THEN max(add_inf)
                                 ELSE sum(add_inf)
                             END) AS add_inf,
                            current_faculty,
                            lec_count,
                            pract_count,
                            object_type,
                            group_type,
                            group_id,
                            alias_name,
                            own_faculty,
                            SHOW
                     FROM
                       (SELECT course,
                               ls.uuid,
                               ls.code,
                               ls.name,
                               ls.sem,
                               ls.department,
                               ls.choice,
                               ls.count_students,
                               ls.count_groups,
                               (CASE
                                    WHEN ls.hours_type='lek' THEN ls.count_hours
                                    ELSE 0
                                END) AS lec,
                               (CASE
                                    WHEN ls.hours_type='pract' THEN ls.count_hours
                                    ELSE 0
                                END) AS pract,
                               (CASE
                                    WHEN ls.hours_type='lab' THEN ls.count_hours
                                    ELSE 0
                                END) AS lab,
                               ls.is_own,
                               (CASE
                                    WHEN ls.object_type=2
                                         AND ls.choice=FALSE THEN TRUE
                                    ELSE ls.active
                                END) AS active,
                               ls.add_inf,
                               ls.current_faculty,
                               SUM (CASE
                                        WHEN work_type=101 THEN work_count
                                        ELSE 0
                                    END) AS lec_count,
                                   SUM (CASE
                                            WHEN (work_type=103
                                                  OR work_type=104) THEN work_count
                                            ELSE 0
                                        END) AS pract_count,
                                       ls.object_type,
                                       ls.group_type,
                                       ls.group_id,
                                       ls.alias_name,
                                       load.own_faculty,
                                       ls.show
                        FROM LOAD,
                             loadstrokes AS ls
                        LEFT JOIN
                          (SELECT *
                           FROM lessonplanstroke AS lp,
                                lessonplanworks AS lw
                           WHERE lw.lessonplanstroke_id=lp.id) AS reslp ON reslp.uuid=ls.uuid
                        WHERE load.profile_id=:profile_id
                          AND ls.load_id=load.id
                          AND budzhet=:budzhet
                          AND ls.current_faculty=:ownt
                          AND load.version=:version
                          AND ls.active=TRUE
                        """
                        +
                        depclause
                        +
                        """  
                        GROUP BY ls.id,
                                 course,
                                 ls.uuid,
                                 ls.code,
                                 ls.name,
                                 ls.sem,
                                 ls.department,
                                 ls.choice,
                                 ls.is_own,
                                 active,
                                 ls.current_faculty,
                                 ls.object_type,
                                 ls.count_students,
                                 ls.count_groups,
                                 ls.group_type,
                                 ls.alias_name,
                                 ls.group_id,
                                 load.own_faculty,
                                 ls.show) AS res_table
                     GROUP BY course,
                              UUID,
                              code,
                              name,
                              sem,
                              department,
                              choice,
                              count_students,
                              count_groups,
                              is_own,
                              active,
                              current_faculty,
                              object_type,
                              lec_count,
                              pract_count,
                              alias_name,
                              group_type,
                              group_id,
                              own_faculty,
                              SHOW) AS res2
                  GROUP BY course,
                           UUID,
                           code,
                           name,
                           sem,
                           department,
                           choice,
                           is_own,
                           active,
                           add_inf,
                           current_faculty,
                           lec_count,
                           pract_count,
                           object_type,
                           alias_name,
                           own_faculty,
                           SHOW
                  ORDER BY course,
                           object_type,
                           code,
                           sem,
                           UUID;                      
                  """, {"profile_id": profile_id, "budzhet": budzhet,
                        "ownt": ownteachers, "version": version_id})
    
    loadstrokes = []                

    for row in ls: 
        is_group = row.is_subgroup != 0 

        vals = colsvalues[row["uuid"]]
        valsfilter = [s for s in vals if s["department"]==row.department and s["budzhet"]==budzhet]

        sum_stroke = 0
        vc = []
        for val in valsfilter:
            sum_stroke += val["val"]
            vc.append({"c_id": val["c_id"], "formula": val["formula"],
                         "val": val["val"], "hand": val["hand"]})

        sum_stroke += row.lf + row.pf + row.labf
    

        stroke = {"uuid": row.uuid, "code": row.code, "name": row.name,
                  "alias": row.alias_name, "course": row.course,
                  "sem": row.sem, "choice": row.choice, 
                  "contingent": row.cs, "groups": row.cg,
                  "lecplan": row.lec_count, "lecfact": row.lf.normalize(),
                  "practplan": row.pract_count, "practfact": row.pf.normalize(),
                  "labfact": row.labf.normalize(), "add_inf": row.add_inf,
                  "is_own": row.is_own, "show": row.show,
                  "ownstudents": row.own_faculty, "ownteachers": row.current_faculty,
                  "summ": sum_stroke, "department": row.department, "is_group": is_group,
                  "budzhet": budzhet, "object_type": row.object_type,
                  "columns": vc}
        loadstrokes.append(stroke)
    
    return loadstrokes  
    # return ""


def get_one_stroke(db, version, uuid):
    ver = db.query(ami.LoadVersion).get({"id":version})

    columnvaluesres = db.execute("""
                                 SELECT UUID,
                                        c_id,
                                        department,
                                        val,
                                        coalesce(formula_text, '') AS formula,
                                        budzhet,
                                        parent_id,
                                        coalesce(hand, FALSE) AS hand
                                 FROM
                                   (SELECT res.uuid,
                                           c_id,
                                           department,
                                           coalesce(sum(value), 0.0000) AS val,
                                           min(cc_id) AS cc_id,
                                           res.budzhet,
                                           res.parent_id,
                                           bool_or(hand) as hand
                                    FROM
                                      (SELECT DISTINCT ls.group_id,
                                                       ls.group_type,
                                                       ls.uuid,
                                                       c.id AS c_id,
                                                       coalesce(c.parent_id, c.id) AS parent_id,
                                                       ls.department,
                                                       ls.budzhet
                                       FROM loadstrokes AS ls,
                                            columns AS c,
                                            LOAD
                                       WHERE load.id=ls.load_id
                                         AND load.version=:ver
                                         AND ls.active=TRUE
                                         AND ls.uuid = :uuid
                                         AND c.active=TRUE
                                         AND c.active_view=TRUE
                                         AND c.year=:year
                                         AND (c.id not in
                                                (SELECT parent_id
                                                 FROM columns
                                                 WHERE parent_id IS NOT NULL)) ) AS res
                                    LEFT JOIN
                                      (SELECT group_id,
                                              group_type,
                                              budzhet,
                                              UUID,
                                              value,
                                              column_id,
                                              cc.id AS cc_id,
                                              lc.hand
                                       FROM loadcolumns AS lc,
                                            columns_calc AS cc
                                       WHERE lc.calc_id=cc.id
                                         AND lc.load_version=:ver) AS res2 ON res2.column_id=res.c_id
                                    AND res.uuid=res2.uuid
                                    AND res.group_id=res2.group_id
                                    AND res.group_type=res2.group_type
                                    AND res.budzhet=res2.budzhet
                                    GROUP BY res.uuid,
                                             c_id,
                                             department,
                                             res.budzhet,
                                             res.parent_id
                                             ) AS res3
                                 LEFT JOIN columns_calc AS cc ON cc.id=cc_id
                                 ORDER BY UUID,
                                          budzhet,
                                          parent_id,
                                          c_id;
                                 """, {'ver': version, 'uuid': uuid, 'year': ver.year})

    column_values = []

    for row in columnvaluesres:
        column_values.append({"c_id": row["c_id"], "department": row["department"],
                              "val": row["val"].normalize(), "formula": row["formula"],
                              "budzhet": row["budzhet"], "parent_id": row["parent_id"],
                              "hand": row["hand"]})

    ls = db.execute("""
                  SELECT course,
                         UUID,
                         code,
                         name,
                         sem,
                         department,
                         choice,
                         sum(count_students) as cs,
                         sum(count_groups) as cg,
                         sum(lecture_fact) as lf,
                         sum(practice_fact) as pf,
                         sum(lab_fact) as labf,
                         is_own,
                         active,
                         add_inf,
                         current_faculty,
                         lec_count,
                         pract_count,
                         object_type,
                         sum(CASE
                                 WHEN group_type='subgroup' THEN 1
                                 ELSE 0
                             END) AS is_subgroup,
                         alias_name,
                         own_faculty,
                         SHOW,
                         budzhet
                  FROM
                    (SELECT course,
                            UUID,
                            code,
                            name,
                            sem,
                            department,
                            choice,
                            count_students,
                            count_groups,
                            sum(lec) AS lecture_fact,
                            sum(pract) AS practice_fact,
                            sum(lab) AS lab_fact,
                            is_own,
                            active,
                            (CASE
                                 WHEN object_type in (3,
                                                      6,
                                                      31) THEN max(add_inf)
                                 ELSE sum(add_inf)
                             END) AS add_inf,
                            current_faculty,
                            lec_count,
                            pract_count,
                            object_type,
                            group_type,
                            group_id,
                            alias_name,
                            own_faculty,
                            SHOW,
                            budzhet
                     FROM
                       (SELECT course,
                               ls.uuid,
                               ls.code,
                               ls.name,
                               ls.sem,
                               ls.department,
                               ls.choice,
                               ls.count_students,
                               ls.count_groups,
                               (CASE
                                    WHEN ls.hours_type='lek' THEN ls.count_hours
                                    ELSE 0
                                END) AS lec,
                               (CASE
                                    WHEN ls.hours_type='pract' THEN ls.count_hours
                                    ELSE 0
                                END) AS pract,
                               (CASE
                                    WHEN ls.hours_type='lab' THEN ls.count_hours
                                    ELSE 0
                                END) AS lab,
                               ls.is_own,
                               (CASE
                                    WHEN ls.object_type=2
                                         AND ls.choice=FALSE THEN TRUE
                                    ELSE ls.active
                                END) AS active,
                               ls.add_inf,
                               ls.current_faculty,
                               SUM (CASE
                                        WHEN work_type=101 THEN work_count
                                        ELSE 0
                                    END) AS lec_count,
                                   SUM (CASE
                                            WHEN (work_type=103
                                                  OR work_type=104) THEN work_count
                                            ELSE 0
                                        END) AS pract_count,
                                       ls.object_type,
                                       ls.group_type,
                                       ls.group_id,
                                       ls.alias_name,
                                       load.own_faculty,
                                       ls.show,
                                       ls.budzhet
                        FROM LOAD,
                             loadstrokes AS ls
                        LEFT JOIN
                          (SELECT *
                           FROM lessonplanstroke AS lp,
                                lessonplanworks AS lw
                           WHERE lw.lessonplanstroke_id=lp.id) AS reslp ON reslp.uuid=ls.uuid
                        WHERE 
                          ls.load_id=load.id
                          AND load.version=:version
                          AND ls.uuid=:uuid
                          AND ls.active=TRUE
                        GROUP BY ls.id,
                                 course,
                                 ls.uuid,
                                 ls.code,
                                 ls.name,
                                 ls.sem,
                                 ls.department,
                                 ls.choice,
                                 ls.is_own,
                                 active,
                                 ls.current_faculty,
                                 ls.object_type,
                                 ls.count_students,
                                 ls.count_groups,
                                 ls.group_type,
                                 ls.alias_name,
                                 ls.group_id,
                                 load.own_faculty,
                                 ls.show,
                                 ls.budzhet) AS res_table
                     GROUP BY course,
                              UUID,
                              code,
                              name,
                              sem,
                              department,
                              choice,
                              count_students,
                              count_groups,
                              is_own,
                              active,
                              current_faculty,
                              object_type,
                              lec_count,
                              pract_count,
                              alias_name,
                              group_type,
                              group_id,
                              own_faculty,
                              SHOW,
                              budzhet) AS res2
                  GROUP BY course,
                           UUID,
                           code,
                           name,
                           sem,
                           department,
                           choice,
                           is_own,
                           active,
                           add_inf,
                           current_faculty,
                           lec_count,
                           pract_count,
                           object_type,
                           alias_name,
                           own_faculty,
                           SHOW,
                           budzhet
                  ORDER BY course,
                           object_type,
                           code,
                           sem,
                           UUID;                      
                  """, {"uuid": uuid, "version": version})
    
    loadstrokes = {"budzhet": [], "vnebudzhet": []}
    for row in ls:
        is_group = row.is_subgroup != 0 

        vals = column_values
        valsfilter = [s for s in vals if s["department"]==row.department and s["budzhet"]==row.budzhet]

        sum_stroke = 0
        vc = []
        for val in valsfilter:
            sum_stroke += val["val"]
            vc.append({"c_id": val["c_id"], "formula": val["formula"],
                         "val": val["val"], "hand": val["hand"]})

        sum_stroke += row.lf + row.pf + row.labf
    

        stroke = {"uuid": row.uuid, "code": row.code, "name": row.name,
                  "alias": row.alias_name, "course": row.course,
                  "sem": row.sem, "choice": row.choice, 
                  "contingent": row.cs, "groups": row.cg,
                  "lecplan": row.lec_count, "lecfact": row.lf.normalize(),
                  "practplan": row.pract_count, "practfact": row.pf.normalize(),
                  "labfact": row.labf.normalize(), "add_inf": row.add_inf,
                  "is_own": row.is_own, "show": row.show,
                  "ownstudents": row.own_faculty, "ownteachers": row.current_faculty,
                  "summ": sum_stroke, "department": row.department, "is_group": is_group,
                  "budzhet": row.budzhet, "object_type": row.object_type,
                  "columns": vc}

        type_b = "budzhet" if row.budzhet else "vnebudzhet"          
        loadstrokes[type_b].append(stroke)
    
    return loadstrokes 


def get_table_load_str_active(db, version, uuid, value):

    ids = []

    subq = db.query(ami.LoadStroke.id).\
              join(ami.Load).\
              filter(ami.Load.version == version).\
              filter(ami.LoadStroke.uuid == uuid).\
              all()                 

    for row in subq:
        ids.append(row[0])                 

    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(ids)).\
           values(show=value).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()          

    return subq


def change_stroke_department(db: Session, version: int, uuid: str, department: int):
    ver = db.query(ami.LoadVersion).get({"id":version})
    deps = acd.get_departments(db, ver.faculty_id)
    deps_numbers = [d.number for d in deps]

    flows = db.query(ami.LoadStroke.flow_id).\
               join(ami.Load).\
               filter(ami.Load.version == version).\
               filter(ami.LoadStroke.uuid == uuid).\
               filter(ami.LoadStroke.flow_id != -1).\
               count()

    if flows > 0:
        return {"res": "warn", "message": "Дисциплина состоит в потоке"}

    ids = []

    subq = db.query(ami.LoadStroke.id).\
              join(ami.Load).\
              filter(ami.Load.version == version).\
              filter(ami.LoadStroke.uuid == uuid).\
              all()                 

    for row in subq:
        ids.append(row[0])                 

    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(ids)).\
         values(department=department, current_faculty=(department in deps_numbers)).\
         execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()                        

    return {"res": "done"}


def appoint_subgroups(db, uuid, version, block_id, hours_type, departments):
    block = db.query(ami.SubGroup).filter(ami.SubGroup.block_id == block_id).all()

    deps = {}
    for row in departments:
        deps[row.sid] = row.department

    ls = db.query(ami.LoadStroke).\
            join(ami.Load).\
            filter(ami.Load.version == version).\
            filter(ami.LoadStroke.uuid == uuid).\
            filter(ami.LoadStroke.hours_type == hours_type).\
            filter(ami.LoadStroke.group_type == 'profile').\
            filter(ami.LoadStroke.group_id == block[0].parent_id).\
            all()

    subs = []
    sub_ids = []        

    for stroke in ls:
        count_hours = stroke.count_hours
        if (stroke.object_type == 2):
           stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id == stroke.id).\
           values(count_hours=0).\
           execution_options(synchronize_session="fetch")

           res = db.execute(stmt)
        else:
           stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id == stroke.id).\
           values(active=False).\
           execution_options(synchronize_session="fetch")

           res = db.execute(stmt)
        
        for group in block:
            dep = stroke.department if stroke.object_type == 2 else deps[group.id]
            gr_count = stroke.count_groups
            count = group.budzhet_count if stroke.budzhet else group.vnebudzhet_count
            count_f = group.budzhet_foreign if stroke.budzhet else group.vnebudzhet_foreign
            if (stroke.object_type == 2):
                count = 0
                count_f = 0
                gr_count = 0
  
            st = ami.LoadStroke(uuid=stroke.uuid,
                                load_id=stroke.load_id,
                                code=stroke.code,
                                name=stroke.name,
                                sem=stroke.sem,
                                budzhet=stroke.budzhet,
                                department=dep,
                                choice=stroke.choice,
                                count_students=count,
                                count_groups=gr_count,
                                teacher_id=stroke.teacher_id,
                                group_id=group.id,
                                group_type='subgroup',
                                alias_name=stroke.alias_name,
                                count_hours=count_hours,
                                hours_type=stroke.hours_type,
                                is_own=stroke.is_own,
                                active=True,
                                current_faculty=stroke.current_faculty,
                                add_inf=stroke.add_inf,
                                object_type=stroke.object_type,
                                flow_id=stroke.flow_id,
                                count_foreign=count_f,
                                show=stroke.show)

            db.add(st)
            db.commit()
            db.refresh(st)

            if (stroke.object_type != 2 and group.id not in sub_ids):
                subs.append({"block_id": block_id, "name": group.name, "group_id": group.id,
                             "parent_id": block[0].parent_id, "department": dep})
                sub_ids.append(group.id)

    if (len(ls) > 0):        
        aclc.load_calc_for_one(db, uuid, version)

        if (stroke.object_type == 2):                
            return {"sub": {"hours_type": hours_type, "parent_id": block[0].parent_id, 
                    "block_id": block_id, "hours": amc.TYPES_OF_LESSON[hours_type]},
                    "strokes": get_one_stroke(db, version, uuid)}
        return {"sub": subs, "strokes": get_one_stroke(db, version, uuid)}            

    return {"sub": "", "strokes": []}                    
                        


def join_subgroups(db, uuid, version, group_profile_id, hours_type):
    subgroups = db.query(ami.SubGroup.id).filter(ami.SubGroup.parent_id == group_profile_id).all()
    sub_ids = [x.id for x in subgroups]

    ls = db.query(ami.LoadStroke.id, ami.LoadStroke.object_type, ami.LoadStroke.count_hours).\
            join(ami.Load).\
            filter(ami.Load.version == version).\
            filter(ami.LoadStroke.uuid == uuid).\
            filter(ami.LoadStroke.hours_type == hours_type).\
            filter(ami.LoadStroke.group_type == 'subgroup').\
            filter(ami.LoadStroke.group_id.in_(sub_ids)).\
            all()

    ls_profile = db.query(ami.LoadStroke.id).\
                    join(ami.Load).\
                    filter(ami.Load.version == version).\
                    filter(ami.LoadStroke.uuid == uuid).\
                    filter(ami.LoadStroke.hours_type == hours_type).\
                    filter(ami.LoadStroke.group_type == 'profile').\
                    filter(ami.LoadStroke.group_id==group_profile_id).\
                    all()

    ls_p_ids = [x[0] for x in ls_profile]                        

    if len(ls) > 0:
        count = ls[0][2]
        object_type = ls[0][1]
        ids = [x[0] for x in ls]


        if (object_type == 2):
           stmt = update(ami.LoadStroke).\
                  where(ami.LoadStroke.id.in_(ls_p_ids)).\
                  where(ami.LoadStroke.count_groups > 0).\
                  values(count_hours=count).\
                  execution_options(synchronize_session="fetch")

           res = db.execute(stmt)
           db.commit()

        stmt = update(ami.LoadStroke).\
               where(ami.LoadStroke.id.in_(ls_p_ids)).\
               values(active=True).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()  

        stmt = delete(ami.LoadStroke).\
               where(ami.LoadStroke.id.in_(ids)).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)     

    aclc.load_calc_for_one(db, uuid, version)

    return get_one_stroke(db, version, uuid)


def get_subgroups_info(db, uuid, version):
    ls_profile = db.query(ami.LoadStroke.group_id,
                          ami.LoadStroke.object_type).\
                    join(ami.Load).\
                    filter(ami.Load.version == version).\
                    filter(ami.LoadStroke.uuid == uuid).\
                    filter(ami.LoadStroke.group_type == 'profile').\
                    distinct().\
                    all()

    ids = [x[0] for x in ls_profile]  
    
    groups_profile = db.query(ami.GroupProfile).\
                        filter(ami.GroupProfile.id.in_(ids)).\
                        all()

    groups = {}                    

    for row in groups_profile:
        row.blocks = {}
        row.blocks_name = {}
        row.strokesubs = []
        for sub in row.subgroups:
          if sub.active:  
            if sub.block_id not in row.blocks:
                row.blocks[sub.block_id] = []
                row.blocks_name[sub.block_id] = ''
            
            row.blocks_name[sub.block_id] = "{} / {}".format(row.blocks_name[sub.block_id], sub.name)
            row.blocks[sub.block_id].append(sub)
        
        groups[row.id] = row        

    if len(ls_profile) > 0:
        object_type = ls_profile[0].object_type 

        if (object_type == 2):
            ls = db.query(ami.LoadStroke.hours_type, 
                          ami.SubGroup.parent_id,
                          ami.SubGroup.block_id).\
                    join(ami.Load).\
                    join(ami.SubGroup, ami.SubGroup.id == ami.LoadStroke.group_id).\
                    filter(ami.Load.version == version).\
                    filter(ami.LoadStroke.uuid == uuid).\
                    filter(ami.LoadStroke.group_type == 'subgroup').\
                    distinct().\
                    all()

            for row in ls: 
                sub = {"hours_type": row.hours_type, "parent_id": row.parent_id,
                       "block_id": row.block_id, "hours": amc.TYPES_OF_LESSON[row.hours_type]}
                groups[row.parent_id].strokesubs.append(sub)       

        else:                                          
            ls = db.query(ami.LoadStroke.group_id,
                          ami.LoadStroke.department,
                          ami.SubGroup.block_id,
                          ami.SubGroup.parent_id,
                          ami.SubGroup.name).\
                    join(ami.Load).\
                    join(ami.SubGroup, ami.SubGroup.id == ami.LoadStroke.group_id).\
                    filter(ami.Load.version == version).\
                    filter(ami.LoadStroke.uuid == uuid).\
                    filter(ami.LoadStroke.group_type == 'subgroup').\
                    distinct().\
                    all()

            for row in ls:
                # row["hours"] = amc.TYPES_OF_LESSON[row.hours_type]
                groups[row.parent_id].strokesubs.append(row)        

    return groups


def change_dep_of_subgroup(db, uuid, version, subgroup_id, newdepartment):

    ls = db.query(ami.LoadStroke.id).\
            join(ami.Load).\
            filter(ami.Load.version == version).\
            filter(ami.LoadStroke.uuid == uuid).\
            filter(ami.LoadStroke.group_type == 'subgroup').\
            filter(ami.LoadStroke.group_id == subgroup_id).\
            all()

    ids = [x[0] for x in ls]
    
    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(ids)).\
           values(department=newdepartment).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    if (res.rowcount > 0):
        return {"strokes": get_one_stroke(db, version, uuid)}

    return {"strokes": ""}


def get_all_flows(db: Session, version: int, own: bool):

    ver = db.query(ami.LoadVersion).get({"id":version})
    deps = db.query(ami.Department.number).filter(ami.Department.faculty == ver.faculty_id).all()
    deps_arr = [x[0] for x in deps]

    flows = db.query(ami.Flow).\
               filter(ami.Flow.load_version == version).\
               filter(ami.Flow.department.in_(deps_arr)).\
               all()
               
    if not own:
       flows = db.query(ami.Flow).\
               filter(ami.Flow.load_version == version).\
               filter(ami.Flow.department.not_in(deps_arr)).\
               all()             

    for row in flows:
        row.show_groups = False
        row.hours = amc.TYPES_OF_LESSON[row.type]

        groups = []
        row.loadstrokes_res = []

        for ls in row.loadstrokes:
            ls.is_main = True if row.main == ls.id else False


        row.loadstrokes = sorted(row.loadstrokes, key=lambda x: x.is_main, reverse=True)    

        for ls in row.loadstrokes:
            if (ls.group_id not in groups):
                groups.append(ls.group_id)
                
                if (ls.group_type == 'profile'):
                    ls.group_name = ls.group_profile.name
                elif (ls.group_type == 'subgroup'):
                    ls.group_name = ls.subgroup.name
                else:
                    ls.group_name = "{} ({} сем.)".format(ls.name, ls.sem)
                row.loadstrokes_res.append(ls) 

        row.loadstrokes = []               

    return flows


def get_stroke_flows(db: Session, version: int, uuid: str):
    flows = db.query(ami.LoadStroke,
                     ami.Flow.main, ami.Flow.type).\
               join(ami.Load).\
               join(ami.Flow).\
               filter(ami.LoadStroke.flow_id >= 0).\
               filter(ami.LoadStroke.uuid == uuid).\
               filter(ami.Load.version == version).\
               all()

    groups = []
    loadstrokes_res = []

    for ls in flows:
        ls.LoadStroke.is_main = True if ls.main == ls.LoadStroke.id else False

    result_dict = sorted(flows, key=lambda x: x.LoadStroke.is_main, reverse=True)

    for ls in result_dict:  
        key = "{}_{}".format(ls.LoadStroke.group_id, ls.LoadStroke.hours_type)  
        if (key not in groups):
                groups.append(key)
                if (ls.LoadStroke.group_type == 'profile'):
                    ls.LoadStroke.group_name = ls.LoadStroke.group_profile.name
                elif (ls.LoadStroke.group_type == 'subgroup'):
                    ls.LoadStroke.group_name = ls.LoadStroke.subgroup.name
                else:
                    ls.LoadStroke.group_name = "{} ({} сем.)".format(ls.LoadStroke.name, ls.LoadStroke.sem)
                ls.LoadStroke.hours = amc.TYPES_OF_LESSON[ls.type]
                loadstrokes_res.append(ls)

    return {"loadstrokes": loadstrokes_res, "groups": get_stroke_groups(db, version, uuid)}  


def add_flow(db: Session, version: int, hours_type: str, name: str, dep: int):
    flow = ami.Flow(type=hours_type, name=name,
                    load_version=version, department=dep)
    db.add(flow)
    db.commit()
    db.refresh(flow)

    flow.loadstrokes_res = []
    flow.show_groups = False
    flow.hours = amc.TYPES_OF_LESSON[hours_type]
    return flow


def get_stroke_groups(db: Session, version: int, uuid: str):
    groups = db.query(ami.LoadStroke).\
                join(ami.Load).\
                filter(ami.LoadStroke.uuid == uuid).\
                filter(ami.Load.version == version).\
                all()

    groups_full = []
    groups_ids = []

    for ls in groups:
        if (ls.group_id not in groups_ids):
                groups_ids.append(ls.group_id)              
                if (ls.group_type == 'profile'):
                    ls.group_name = ls.group_profile.name
                elif (ls.group_type == 'subgroup'):
                    ls.group_name = ls.subgroup.name
                else:
                    ls.group_name = "{} ({} сем.)".format(ls.name, ls.sem)
                groups_full.append({"group_id": ls.group_id,
                                    "group_type": ls.group_type,
                                    "group_name": ls.group_name})

    return groups_full


def add_flow_participant(db: Session, uuid: str, flow_id: int,
                         group_type: str, group_id: int):

    flow = db.query(ami.Flow).get({"id":flow_id})

    flows = db.query(ami.LoadStroke).\
               join(ami.Load).\
               filter(ami.LoadStroke.uuid == uuid).\
               filter(ami.Load.version == flow.load_version).\
               filter(ami.LoadStroke.hours_type == flow.type).\
               filter(ami.LoadStroke.group_id == group_id).\
               filter(ami.LoadStroke.group_type == group_type).\
               filter(ami.LoadStroke.flow_id < 0).\
               order_by(ami.LoadStroke.count_hours.desc()).\
               all()        

    if len(flows) > 0:
        if flow.main == None:
            stmt = update(ami.Flow).\
                   where(ami.Flow.id==flow.id).\
                   values(main=flows[0].id).\
                   execution_options(synchronize_session="fetch")

            res = db.execute(stmt)
            db.commit()
            db.refresh(flow)

            for row in flows:
                stmt = update(ami.LoadStroke).\
                       where(ami.LoadStroke.id == row.id).\
                       values(flow_id=flow.id).\
                       execution_options(synchronize_session="fetch")
                res = db.execute(stmt)
                db.commit()
        else:
            for row in flows:
                stmt = update(ami.LoadStroke).\
                       where(ami.LoadStroke.id == row.id).\
                       values(flow_id=flow.id, count_hours=0).\
                       execution_options(synchronize_session="fetch")
                res = db.execute(stmt)
                db.commit()


        aclc.load_calc_for_one(db, uuid, flow.load_version)
        return {"strokes": get_one_stroke(db, flow.load_version, uuid)}        
                        
    return {"strokes": ""}


def remove_flow_participant(db: Session, uuid: str, flow_id: int,
                            group_type: str, group_id: int):

    flow = db.query(ami.Flow).get({"id":flow_id})

    flows = db.query(ami.LoadStroke.id).\
               join(ami.Load).\
               filter(ami.LoadStroke.uuid == uuid).\
               filter(ami.Load.version == flow.load_version).\
               filter(ami.LoadStroke.hours_type == flow.type).\
               filter(ami.LoadStroke.group_id == group_id).\
               filter(ami.LoadStroke.group_type == group_type).\
               filter(ami.LoadStroke.flow_id == flow.id).\
               all()

    strokes = db.query(ami.LessonPlanStroke).\
                 filter(ami.LessonPlanStroke.uuid == uuid).\
                 all()

    hours = 0
    ids = [x[0] for x in flows]             

    for row in strokes:
        lpw = row.lessonplanworks
        for lp in lpw:
            if ((flow.type == 'lek') and lp.work_type == 101) or \
               ((flow.type == 'pract') and (lp.work_type == 103 or lp.work_type == 104)):
                hours = lp.work_count
            elif flow.type == 'lab' and lp.work_type == 102:
                hours += lp.work_count


    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(ids)).\
           values(flow_id=-1).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(ids)).\
           where(ami.LoadStroke.count_groups > 0).\
           values(count_hours=hours).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()                                          

    aclc.load_calc_for_one(db, uuid, flow.load_version)
    return {"strokes": get_one_stroke(db, flow.load_version, uuid)}


def change_main_in_flow(db: Session, uuid: str, flow_id: int,
                        group_type: str, group_id: int):

    flow = db.query(ami.Flow).get({"id": flow_id})
    stroke = db.query(ami.LoadStroke).get({"id": flow.main})

    strokes_old = db.query(ami.LoadStroke.id, ami.Load.profile_id).\
                     join(ami.Load).\
                     filter(ami.LoadStroke.uuid == stroke.uuid).\
                     filter(ami.Load.version == flow.load_version).\
                     filter(ami.LoadStroke.hours_type == flow.type).\
                     filter(ami.LoadStroke.group_id == stroke.group_id).\
                     filter(ami.LoadStroke.group_type == stroke.group_type).\
                     filter(ami.LoadStroke.flow_id == flow.id).\
                     all()

    old_profile = strokes_old[0].profile_id                 

    for row in strokes_old:
        stmt = update(ami.LoadStroke).\
               where(ami.LoadStroke.id == row.id).\
               values(count_hours=0).\
               execution_options(synchronize_session="fetch")
        res = db.execute(stmt)
        db.commit()


    strokes_new = db.query(ami.LoadStroke.id, ami.Load.profile_id).\
                     join(ami.Load).\
                     filter(ami.LoadStroke.uuid == uuid).\
                     filter(ami.Load.version == flow.load_version).\
                     filter(ami.LoadStroke.hours_type == flow.type).\
                     filter(ami.LoadStroke.group_id == group_id).\
                     filter(ami.LoadStroke.group_type == group_type).\
                     filter(ami.LoadStroke.flow_id == flow.id).\
                     order_by(ami.LoadStroke.count_groups.desc()).\
                     all()

    strokes = db.query(ami.LessonPlanStroke).\
                 filter(ami.LessonPlanStroke.uuid == uuid).\
                 all()                 

    hours = 0
    ids = [x[0] for x in strokes_new]
    new_profile = strokes_new[0].profile_id           

    for row in strokes:
        lpw = row.lessonplanworks
        for lp in lpw:
            if ((flow.type == 'lek') and lp.work_type == 101) or \
               ((flow.type == 'pract') and (lp.work_type == 103 or lp.work_type == 104)):
                hours = lp.work_count
            elif flow.type == 'lab' and lp.work_type == 102:
                hours += lp.work_count

    stmt = update(ami.LoadStroke).\
           where(ami.LoadStroke.id.in_(ids)).\
           where(ami.LoadStroke.count_groups > 0).\
           values(count_hours=hours).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    stmt = update(ami.Flow).\
           where(ami.Flow.id==flow.id).\
           values(main=strokes_new[0].id).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()                  

    aclc.load_calc_for_one(db, stroke.uuid, flow.load_version)
    aclc.load_calc_for_one(db, uuid, flow.load_version)

    res = get_one_stroke(db, flow.load_version, uuid)
    res_old = get_one_stroke(db, flow.load_version, stroke.uuid)

    return {"old": {"strokes": res_old, "profile": old_profile},
            "new": {"strokes": res, "profile": new_profile},
            "old_uuid": stroke.uuid}


def delete_flow(db, flow_id):
    flow = db.query(ami.Flow).get({"id":flow_id})

    flows = db.query(ami.LoadStroke.uuid, ami.LoadStroke.id,
                     ami.LoadStroke.count_groups,
                     ami.Load.profile_id).\
               join(ami.Load).\
               filter(ami.Load.version == flow.load_version).\
               filter(ami.LoadStroke.hours_type == flow.type).\
               filter(ami.LoadStroke.flow_id == flow.id).\
               distinct().\
               all()

    uuids = [x[0] for x in flows]           

    strokes = db.query(ami.LessonPlanStroke).\
                 filter(ami.LessonPlanStroke.uuid.in_(uuids)).\
                 all()

    strokes_dir = {}

    for row in strokes:
        hours = 0
        lpw = row.lessonplanworks
        for lp in lpw:
            if ((flow.type == 'lek') and lp.work_type == 101) or \
               ((flow.type == 'pract') and (lp.work_type == 103 or lp.work_type == 104)):
                hours = lp.work_count
            elif flow.type == 'lab' and lp.work_type == 102:
                hours += lp.work_count
        row.hours = hours        
        strokes_dir[row.uuid] = row

    for row in flows:
        stmt = update(ami.LoadStroke).\
               where(ami.LoadStroke.id == row.id).\
               values(count_hours=strokes_dir[row.uuid].hours*row.count_groups,
                      flow_id=-1).\
               execution_options(synchronize_session="fetch")
        res = db.execute(stmt)
        db.commit()
        strokes_dir[row.uuid].profile_id = row.profile_id

    strokes_res = []    

    for row in strokes:
        aclc.load_calc_for_one(db, row.uuid, flow.load_version)
        res = get_one_stroke(db, flow.load_version, row.uuid)
        strokes_res.append({"strokes": res, "uuid": row.uuid,
                            "profile_id": strokes_dir[row.uuid].profile_id})

    stmt = delete(ami.Flow).\
           where(ami.Flow.id == flow.id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    return strokes_res


def add_own_stroke(db: Session, version: int, profile_id: int, 
                   course: int, sem: int,
                   type_id: int, add_inf: int, department: int):
    
    type_st = db.query(ami.ConstrDirectory).get({"id":type_id})
    ver = db.query(ami.LoadVersion).get({"id":version})

    groups = db.query(ami.GroupProfile).\
                filter(ami.GroupProfile.profile_id == profile_id).\
                filter(ami.GroupProfile.course == course).\
                filter(ami.GroupProfile.year == ver.year).\
                all()

    load = db.query(ami.Load).\
              filter(ami.Load.profile_id == profile_id).\
              filter(ami.Load.version == version).\
              filter(ami.Load.course == course).\
              first()

    str_uuid = str(uuid.uuid4())
    types_add = ["other"]

    if (add_inf):
        for i in range(1, add_inf):
            types_add.append("other{}".format(i))


    if load:          
        for idx, row in enumerate(groups):
            # hours_type_name = 'others' if idx==0 else 'others{}'.format(idx)

            if row.budzhet_count > 0:
                for gt in types_add:
                    st = ami.LoadStroke(uuid=str_uuid,
                                        load_id=load.id,
                                        code="",
                                        name=type_st.name,
                                        sem=(course - 1)*2 + sem,
                                        budzhet=True,
                                        department=department,
                                        choice=False,
                                        count_students=row.budzhet_count,
                                        count_groups=0,
                                        teacher_id=-1,
                                        group_id=row.id,
                                        group_type='profile',
                                        alias_name=type_st.name,
                                        count_hours=0,
                                        hours_type=gt,
                                        is_own=True,
                                        active=True,
                                        current_faculty=True,
                                        add_inf=(1 if add_inf else 0),
                                        object_type=type_st.value,
                                        flow_id=-1,
                                        count_foreign=row.budzhet_foreign,
                                        show=True)

                    db.add(st)
                    db.commit()
                    db.refresh(st)

            if row.vnebudzhet_count > 0:
                for gt in types_add:
                    st = ami.LoadStroke(uuid=str_uuid,
                                        load_id=load.id,
                                        code="",
                                        name=type_st.name,
                                        sem=(course - 1)*2 + sem,
                                        budzhet=False,
                                        department=department,
                                        choice=False,
                                        count_students=row.vnebudzhet_count,
                                        count_groups=0,
                                        teacher_id=-1,
                                        group_id=row.id,
                                        group_type='profile',
                                        alias_name=type_st.name,
                                        count_hours=0,
                                        hours_type=gt,
                                        is_own=True,
                                        active=True,
                                        current_faculty=True,
                                        add_inf=(1 if add_inf else 0),
                                        object_type=type_st.value,
                                        flow_id=-1,
                                        count_foreign=row.vnebudzhet_foreign,
                                        show=True)

                    db.add(st)
                    db.commit()
                    db.refresh(st)

    aclc.load_calc_for_one(db, str_uuid, version)             

    return {"strokes": get_one_stroke(db, version, str_uuid)}


def delete_own_stroke(db, uuid, version):
    loads = db.query(ami.Load.id).filter(ami.Load.version == version).all()
    load_ids = [x[0] for x in loads]

    stmt = delete(ami.LoadStroke).\
           where(ami.LoadStroke.uuid == uuid).\
           where(ami.LoadStroke.load_id.in_(load_ids)).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    return "ok"


def change_column_value(db, uuid, budzhet, version, column_id, value, dep):

    calcs = db.query(ami.ColumnCalc.id).filter(ami.ColumnCalc.column_id == column_id).all()
    calcs_ids = [x[0] for x in calcs]

    count = 0

    subq = db.query(ami.LoadStroke.group_id, ami.LoadStroke.group_type).\
              join(ami.Load).\
              filter(ami.Load.version == version).\
              filter(ami.LoadStroke.uuid == uuid).\
              filter(ami.LoadStroke.budzhet == budzhet).\
              filter(ami.LoadStroke.department == dep).\
              distinct().\
              all()

    for row in subq:          
        stmt = update(ami.LoadColumn).\
               where(ami.LoadColumn.uuid == uuid).\
               where(ami.LoadColumn.calc_id.in_(calcs_ids)).\
               where(ami.LoadColumn.budzhet == budzhet).\
               where(ami.LoadColumn.load_version == version).\
               where(ami.LoadColumn.group_type == row.group_type).\
               where(ami.LoadColumn.group_id == row.group_id).\
               values(value=0, hand=True).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()
        count += res.rowcount

    if (count == 0):
        if len(calcs_ids)==0:
            calc = ami.ColumnCalc(column_id=column_id,
                                  formula="0",
                                  formula_text="0",
                                  active=False)
            db.add(calc)
            db.commit()
            db.refresh(calc)

        min_cc = db.query(func.min(ami.ColumnCalc.id)).filter(ami.ColumnCalc.column_id == column_id).first()
        min_cc = min_cc[0]

        for idx, row in enumerate(subq):
            val = value if idx==0 else 0
            lc = ami.LoadColumn(calc_id=min_cc, value=val, uuid=uuid,
                                group_id=row.group_id, group_type=row.group_type,
                                budzhet=budzhet, load_version=version,
                                teacher_id=-1, hand=True) 
            db.add(lc)
            db.commit()

    else:
        lcs = db.query(func.min(ami.LoadColumn.id)).\
                 filter(ami.LoadColumn.uuid == uuid).\
                 filter(ami.LoadColumn.calc_id.in_(calcs_ids)).\
                 filter(ami.LoadColumn.budzhet == budzhet).\
                 filter(ami.LoadColumn.load_version == version).\
                 filter(ami.LoadColumn.group_id == subq[0].group_id).\
                 filter(ami.LoadColumn.group_type == subq[0].group_type).\
                 first()

        ls_id = lcs[0]
        
        stmt = update(ami.LoadColumn).\
               where(ami.LoadColumn.id == ls_id).\
               values(value=value).\
               execution_options(synchronize_session="fetch")

        res = db.execute(stmt)
        db.commit()             

    return {"strokes": get_one_stroke(db, version, uuid)} 


def return_to_auto_calc(db, uuid, budzhet, version, column_id, dep):
    calcs = db.query(ami.ColumnCalc.id).filter(ami.ColumnCalc.column_id == column_id).all()
    calcs_ids = [x[0] for x in calcs]

    subq = db.query(ami.LoadStroke.group_id, ami.LoadStroke.group_type).\
              join(ami.Load).\
              filter(ami.Load.version == version).\
              filter(ami.LoadStroke.uuid == uuid).\
              filter(ami.LoadStroke.budzhet == budzhet).\
              filter(ami.LoadStroke.department == dep).\
              distinct().\
              all()

    for row in subq:          
        lcs = db.query(ami.LoadColumn.id).\
                 filter(ami.LoadColumn.uuid == uuid).\
                 filter(ami.LoadColumn.calc_id.in_(calcs_ids)).\
                 filter(ami.LoadColumn.budzhet == budzhet).\
                 filter(ami.LoadColumn.load_version == version).\
                 filter(ami.LoadColumn.hand == True).\
                 filter(ami.LoadColumn.group_type == row.group_type).\
                 filter(ami.LoadColumn.group_id == row.group_id).\
                 all()

        lcs_ids = [x[0] for x in lcs]         

        stmt = delete(ami.LoadColumn).\
               where(ami.LoadColumn.id.in_(lcs_ids)).\
               execution_options(synchronize_session="fetch")
        db.execute(stmt)
        db.commit()         

    aclc.load_calc_for_one(db, uuid, version)             

    return {"strokes": get_one_stroke(db, version, uuid)}


def get_all_faculty_report(db, faculty, version, onlyotherstudents, ownteachers):
    ver = db.query(ami.LoadVersion).get({"id":version})
    faculty_inf = acd.get_faculty_by_id(db, faculty, ver.year)

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})

    cols = get_table_columns(db, faculty, version, False)
    load = get_table_load(db, version, onlyotherstudents, ownteachers)

    worksheet = workbook.add_worksheet("Бюджет")
    add_data_to_worksheet(workbook, worksheet, cols, load, faculty_inf, ver, True) 

    worksheet = workbook.add_worksheet("Внебюджет")
    add_data_to_worksheet(workbook, worksheet, cols, load, faculty_inf, ver, False)               

    workbook.close()
    output.seek(0)

    headers = {'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
               'Content-Disposition': 'attachment; filename=reportone.xlsx'}
    return Response(content=output.getvalue(), status_code=200, headers=headers)


def add_data_to_worksheet(workbook, worksheet, cols, load, faculty_inf, ver, budzhet):
    merge_format = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial'})
    cell_format_c = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial', 'border': 1})
    cell_format = workbook.add_format({'align': 'left', 'font_size': 12, 'font_name': 'Arial', 'border': 1, 'text_wrap': True})
    merge_format_rotate = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'rotation': 90,
                                               'border': 1, 'text_wrap': True, 'valign': 'vcenter'})
    merge_format_rotate_b = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'rotation': 90,
                                               'border': 1, 'text_wrap': True})
    merge_format_header = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial',
                                               'border': 1, 'text_wrap': True, 'valign': 'vcenter'})
    merge_format_bold = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial', 'bold': True})
    merge_format_boldr = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'bold': True,
                                              'border': 1})
    merge_format_right = workbook.add_format({'align': 'right', 'bold': True, 'font_size': 12, 'font_name': 'Arial'})
    merge_format_fname = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 20, 'font_name': 'Arial',
                                              'border': 1, 'bg_color': "#00CCFF"})
    merge_format_form = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 20, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#FFFF00"})
    merge_format_dir = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#00FF00"})
    merge_format_profile = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#00FFFF"})
    cell_format_itog = workbook.add_format({'align': 'left', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#FFCC99", 'text_wrap': True})
    
    worksheet.set_zoom(75)

    worksheet.set_column('A:A', 10)
    worksheet.set_column('B:B', 20)
    worksheet.set_column(2, 34, 5)

    worksheet.merge_range('AC1:AK1', 'Таблица №1', merge_format_right)
    worksheet.merge_range('B2:G2', 'МИНОБРНАУКИ РОССИИ', merge_format)
    worksheet.merge_range('B3:G3', 'Федеральное государственное бюджетное', merge_format)
    worksheet.merge_range('B4:G4', 'образовательное учреждение', merge_format)
    worksheet.merge_range('B5:G5', 'высшего образования', merge_format)
    worksheet.merge_range('B6:G6', '"ИРКУТСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ"', merge_format)
    worksheet.merge_range('B7:G7', '{}'.format(faculty_inf.name), merge_format)
    worksheet.merge_range('AC3:AK3', 'УТВЕРЖДАЮ', merge_format)
    worksheet.merge_range('AC4:AK5', 'Проректор по учебной работе', merge_format)
    worksheet.merge_range('AC6:AK6', '____________________А. И. Вокин', merge_format)
    worksheet.merge_range('AC8:AK8', '"______"___________________ 20     г.', merge_format)
    worksheet.merge_range('I9:AA9', 'СВОДНЫЙ РАСЧЕТ ЧАСОВ РАБОТЫ ПО "{}"'.format(faculty_inf.name.upper()), merge_format_bold)
    worksheet.merge_range('I10:AA10', 'на {}-{} учебный год'.format(ver.year, ver.year+1), merge_format_bold)
    worksheet.write("B12", "БЮДЖЕТ" if budzhet else "ВНЕБЮДЖЕТ", merge_format)

    worksheet.set_row(14, 150)
    worksheet.merge_range('A14:A15', 'Код дисциплины по учебному плану', merge_format_rotate)
    worksheet.merge_range('B14:B15', 'Наименование дисциплины', merge_format_header)
    worksheet.merge_range('C14:C15', 'Курс / семестр', merge_format_rotate_b)
    worksheet.merge_range('D14:D15', 'Контингент студентов', merge_format_rotate_b)
    worksheet.merge_range('E14:E15', 'Количество учебных групп', merge_format_rotate_b)
    worksheet.merge_range('F14:G14', 'Лекции', merge_format_boldr)
    worksheet.write("F15", "По плану", merge_format_rotate_b)
    worksheet.write("G15", "Всего", merge_format_rotate_b)
    worksheet.merge_range('H14:I14', 'Практ., семинар. занятия', merge_format_boldr)
    worksheet.write("H15", "По плану", merge_format_rotate_b)
    worksheet.write("I15", "Всего", merge_format_rotate_b)
    worksheet.merge_range('J14:J15', 'Лаборат. занятия', merge_format_rotate_b)

    c = 10
    cisd = {}

    for row in cols["columns"]:
        col = cols["columns"][row]
        if (len(col["children"]) == 0):
            worksheet.merge_range(13, c, 14, c, col["name"], merge_format_rotate_b)
            cisd[col["id"]] = c
            c += 1
        else:
            worksheet.merge_range(13, c, 13, c+len(col["children"])-1, col["name"] , merge_format_boldr)
            for ch in col["children"]:
               worksheet.write(14, c, ch["name"], merge_format_rotate_b)
               cisd[ch["id"]] = c
               c += 1

    worksheet.merge_range(13, c, 14, c, "ВСЕГО", merge_format_rotate_b)
    c += 1               
    worksheet.merge_range(13, c, 14, c, "Кафедры, обеспечивающие нагрузку", merge_format_rotate_b)

    for i in range(1, cols["colspan"]-1):
        worksheet.write(15, i-1, i, cell_format_c)

    len_name = cols["colspan"]-5

    active_row = 22 

    fill_empty = ["" for x in range(0, cols["colspan"]-2)]
    sums_forms = {}
    sums_faculties = {}

    for key in load:
        f = load[key]
        sums_faculties[key] = [0 for x in range(0, cols["colspan"]-3)]
        worksheet.set_row(active_row, 22)
        worksheet.write(active_row, 0, "", cell_format_c)
        worksheet.write(active_row, 1, "", cell_format_c)
        worksheet.merge_range(active_row, 2, active_row, len_name, f["name"].upper(), merge_format_fname)
        worksheet.write(active_row, len_name+1, "", cell_format_c)       
        worksheet.write(active_row, len_name+2, "", cell_format_c)
        active_row += 1

        for kf in f["forms"]:
            form = f["forms"][kf]
            sums_forms[kf] = [0 for x in range(0, cols["colspan"]-3)]
            worksheet.set_row(active_row, 22)
            worksheet.write(active_row, 0, "", cell_format_c)
            worksheet.write(active_row, 1, "", cell_format_c)
            worksheet.merge_range(active_row, 2, active_row, len_name, form["name"].upper(), merge_format_form)
            worksheet.write(active_row, len_name+1, "", cell_format_c)       
            worksheet.write(active_row, len_name+2, "", cell_format_c)
            active_row += 1

            for dirc in form["directions"]:
                k = "nb" if budzhet else "nvb"

                if dirc[k] > 0:
                    worksheet.write(active_row, 0, "", cell_format_c)
                    worksheet.write(active_row, 1, "", cell_format_c)
                    worksheet.merge_range(active_row, 2, active_row, len_name, 'Направление {} "{}"'.format(dirc["shifr"], dirc["name"]), merge_format_dir)
                    worksheet.write(active_row, len_name+1, "", cell_format_c)       
                    worksheet.write(active_row, len_name+2, "", cell_format_c)
                    active_row += 1

                for j, kp in enumerate(dirc["profiles"]):

                    profile = dirc["profiles"][kp]
                    strokes = profile["loadstrokes_budzhet"] if budzhet else profile["loadstrokes_vnebudzhet"] 

                    if len(strokes)>0:
                        worksheet.write(active_row, 0, "", cell_format_c)
                        worksheet.write(active_row, 1, "", cell_format_c)
                        worksheet.merge_range(active_row, 2, active_row, len_name, 'профиль "{}"'.format(profile["name"]), merge_format_profile)
                        worksheet.write(active_row, len_name+1, "", cell_format_c)       
                        worksheet.write(active_row, len_name+2, "", cell_format_c)
                        active_row += 1     

                        for stroke in strokes:
                            if stroke["show"]:
                                worksheet.write_row(active_row, 0, fill_empty, cell_format_c)
                                worksheet.write(active_row, 0, stroke["code"], cell_format_c)
                                worksheet.write(active_row, 1, stroke["name"], cell_format)
                                worksheet.write(active_row, 2, "{}/{}".format(stroke["course"], stroke["sem"]), cell_format_c)
                                if stroke["contingent"] != 0:
                                    worksheet.write(active_row, 3, stroke["contingent"], cell_format_c)
                                if stroke["groups"] != 0:    
                                    worksheet.write(active_row, 4, stroke["groups"], cell_format_c)
                                if stroke["lecplan"] != 0:
                                    worksheet.write(active_row, 5, stroke["lecplan"], cell_format_c)
                                    sums_faculties[key][5] += stroke["lecplan"] or 0
                                    sums_forms[kf][5] += stroke["lecplan"] or 0
                                if stroke["lecfact"] != 0:
                                    worksheet.write(active_row, 6, stroke["lecfact"], cell_format_c)
                                    sums_faculties[key][6] += stroke["lecfact"] or 0
                                    sums_forms[kf][6] += stroke["lecfact"] or 0
                                if stroke["practplan"] != 0:    
                                    worksheet.write(active_row, 7, stroke["practplan"], cell_format_c)
                                    sums_faculties[key][7] += stroke["practplan"] or 0
                                    sums_forms[kf][7] += stroke["practplan"] or 0
                                if stroke["practfact"] != 0:    
                                    worksheet.write(active_row, 8, stroke["practfact"], cell_format_c)
                                    sums_faculties[key][8] += stroke["practfact"] or 0
                                    sums_forms[kf][8] += stroke["practfact"] or 0
                                if stroke["labfact"] != 0:    
                                    worksheet.write(active_row, 9, stroke["labfact"], cell_format_c)
                                    sums_faculties[key][9] += stroke["labfact"] or 0
                                    sums_forms[kf][9] += stroke["labfact"] or 0

                                for cval in stroke["columns"]:
                                    if cval["val"] != 0:
                                        worksheet.write(active_row, cisd[cval["c_id"]], cval["val"], cell_format_c)
                                        sums_faculties[key][cisd[cval["c_id"]]] += cval["val"]
                                        sums_forms[kf][cisd[cval["c_id"]]] += cval["val"]

                                worksheet.write(active_row, len_name+1, stroke["summ"], cell_format_c)
                                sums_faculties[key][len_name+1] += stroke["summ"]    
                                sums_forms[kf][len_name+1] += stroke["summ"]    
                                worksheet.write(active_row, len_name+2, stroke["department"], cell_format_c)    
                                active_row += 1

        worksheet.set_row(active_row, 60)
        worksheet.write_row(active_row, 0, fill_empty, cell_format_itog)
        worksheet.write(active_row, 1, 'Итого по "{}"'.format(f["name"]), cell_format_itog)
        for idx, v in enumerate(sums_faculties[key]):
            if v != 0:
                worksheet.write(active_row, idx, v, cell_format_itog)
        active_row += 1

    worksheet.write_row(16, 0, fill_empty, cell_format)
    worksheet.write(16, 1, "Очная", cell_format)
    worksheet.write_row(17, 0, fill_empty, cell_format)
    worksheet.write(17, 1, "Очно-заочная", cell_format)
    worksheet.write_row(18, 0, fill_empty, cell_format)
    worksheet.write(18, 1, "Заочная", cell_format)
    worksheet.write_row(19, 0, fill_empty, cell_format)
    worksheet.write(19, 1, "На других факультетах", cell_format)
    worksheet.write_row(20, 0, fill_empty, cell_format)
    worksheet.write(20, 1, "ИТОГО:", cell_format)
    
    r = None
    sums_all = [0 for x in range(0, cols["colspan"]-3)]
    for key in sums_forms:
        if key==1:
            r = 16
        elif key==2:
            r = 17
        elif key==3:
            r = 18

        sums_all = [x + y for x, y in zip(sums_all, sums_forms[key])]          

        for idx, v in enumerate(sums_forms[key]):
            if v != 0:
                worksheet.write(r, idx, v, cell_format)

    for idx, v in enumerate(sums_all):
            if v != 0:
                worksheet.write(20, idx, v, cell_format)


def get_dep_faculty_report(db, faculty, dep, version, onlyotherstudents, ownteachers):
    ver = db.query(ami.LoadVersion).get({"id":version})
    faculty_inf = acd.get_faculty_by_id(db, faculty, ver.year)

    output = BytesIO()
    workbook = xlsxwriter.Workbook(output, {'in_memory': True})

    cols = get_table_columns(db, faculty, version, False)
    load = get_table_load(db, version, onlyotherstudents, ownteachers, dep)
    department = acd.get_department_by_number(db, dep)

    teachers = get_stroke_teachers(db, dep, version)

    worksheet = workbook.add_worksheet("Бюджет")
    add_data_to_worksheet_dep(workbook, worksheet, cols, load, faculty_inf, ver, True, department, teachers) 

    worksheet = workbook.add_worksheet("Внебюджет")
    add_data_to_worksheet_dep(workbook, worksheet, cols, load, faculty_inf, ver, False, department, teachers)               

    workbook.close()
    output.seek(0)

    headers = {'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
               'Content-Disposition': 'attachment; filename=reporttwo.xlsx'}
    return Response(content=output.getvalue(), status_code=200, headers=headers)


def get_stroke_teachers(db, dep, version):
    teachers_dict = {}
    ls_t = db.query(ami.LoadStroke.uuid, 
                    ami.LoadStroke.budzhet, 
                    ami.LoadStroke.teacher_id,
                    ami.Teacher.name,
                    ami.Teacher.surname,
                    ami.Teacher.fathername).\
              join(ami.Load).\
              join(ami.Teacher, ami.Teacher.id==ami.LoadStroke.teacher_id).\
              filter(ami.Load.version == version).\
              filter(ami.LoadStroke.active == True).\
              filter(ami.LoadStroke.department == dep).\
              filter(ami.LoadStroke.teacher_id != -1).\
              distinct().\
              all() 

    for row in ls_t:
        key = "{}_{}".format(row.uuid, row.budzhet)
        if key not in teachers_dict:
            teachers_dict[key] = {"ids": [], "names": []}
        teachers_dict[key]["names"].append("{} {}.{}.".format(row.surname, row.name[0], row.fathername[0]))
        teachers_dict[key]["ids"].append(row.teacher_id)

    ls_tc = db.query(ami.LoadColumn.uuid, 
                     ami.LoadColumn.budzhet, 
                     ami.LoadColumn.teacher_id,
                     ami.Teacher.name,
                     ami.Teacher.surname,
                     ami.Teacher.fathername).\
              join(ami.Teacher, ami.Teacher.id==ami.LoadColumn.teacher_id).\
              filter(ami.LoadColumn.load_version == version).\
              filter(ami.LoadColumn.teacher_id != -1).\
              distinct().\
              all()

    for row in ls_tc:
        key = "{}_{}".format(row.uuid, row.budzhet)
        if key not in teachers_dict:
            teachers_dict[key] = {"ids": [], "names": []}
        
        if row.teacher_id not in teachers_dict[key]["ids"]:    
            teachers_dict[key]["names"].append("{} {}.{}.".format(row.surname, row.name[0], row.fathername[0]))
            teachers_dict[key]["ids"].append(row.teacher_id)

    return teachers_dict         


def add_data_to_worksheet_dep(workbook, worksheet, cols, load, faculty_inf, ver, budzhet, department, teachers):
    merge_format = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial'})
    cell_format_c = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial', 'border': 1})
    cell_format = workbook.add_format({'align': 'left', 'font_size': 12, 'font_name': 'Arial', 'border': 1, 'text_wrap': True})
    merge_format_rotate = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'rotation': 90,
                                               'border': 1, 'text_wrap': True, 'valign': 'vcenter'})
    merge_format_rotate_b = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'rotation': 90,
                                               'border': 1, 'text_wrap': True})
    merge_format_header = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial',
                                               'border': 1, 'text_wrap': True, 'valign': 'vcenter'})
    merge_format_bold = workbook.add_format({'align': 'center', 'font_size': 12, 'font_name': 'Arial', 'bold': True})
    merge_format_boldr = workbook.add_format({'align': 'center', 'font_size': 10, 'font_name': 'Arial', 'bold': True,
                                              'border': 1})
    merge_format_right = workbook.add_format({'align': 'right', 'bold': True, 'font_size': 12, 'font_name': 'Arial'})
    merge_format_fname = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 20, 'font_name': 'Arial',
                                              'border': 1, 'bg_color': "#00CCFF"})
    merge_format_form = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 20, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#FFFF00"})
    merge_format_dir = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#00FF00"})
    merge_format_profile = workbook.add_format({'align': 'center', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#00FFFF"})
    cell_format_itog = workbook.add_format({'align': 'left', 'bold': True, 'font_size': 12, 'font_name': 'Arial',
                                             'border': 1, 'bg_color': "#FFCC99", 'text_wrap': True})
    
    worksheet.set_zoom(75)

    worksheet.set_column('A:A', 10)
    worksheet.set_column('B:B', 20)
    worksheet.set_column(2, 34, 5)

    worksheet.merge_range('AC1:AK1', 'Таблица №2', merge_format_right)
    worksheet.merge_range('B2:G2', 'МИНОБРНАУКИ РОССИИ', merge_format)
    worksheet.merge_range('B3:G3', 'Федеральное государственное бюджетное', merge_format)
    worksheet.merge_range('B4:G4', 'образовательное учреждение', merge_format)
    worksheet.merge_range('B5:G5', 'высшего образования', merge_format)
    worksheet.merge_range('B6:G6', '"ИРКУТСКИЙ ГОСУДАРСТВЕННЫЙ УНИВЕРСИТЕТ"', merge_format)
    worksheet.merge_range('B7:G7', '{}'.format(faculty_inf.name), merge_format)
    worksheet.merge_range('AC3:AK3', 'УТВЕРЖДАЮ', merge_format)
    worksheet.merge_range('AC4:AK5', 'Директор ИМИТ', merge_format)
    worksheet.merge_range('AC6:AK6', '____________________М. В. Фалалеев', merge_format)
    worksheet.merge_range('AC8:AK8', '"______"___________________ 20     г.', merge_format)
    worksheet.merge_range('I9:AA9', 'СВОДНЫЙ РАСЧЕТ ЧАСОВ РАБОТЫ КАФЕДРЫ {}'.format(department.name.upper()), merge_format_bold)
    worksheet.merge_range('I10:AA10', 'на {}-{} учебный год'.format(ver.year, ver.year+1), merge_format_bold)
    worksheet.write("B12", "БЮДЖЕТ" if budzhet else "ВНЕБЮДЖЕТ", merge_format)

    worksheet.set_row(14, 150)
    worksheet.merge_range('A14:A15', 'Код дисциплины по учебному плану', merge_format_rotate)
    worksheet.merge_range('B14:B15', 'Наименование дисциплины', merge_format_header)
    worksheet.merge_range('C14:C15', 'Курс / семестр', merge_format_rotate_b)
    worksheet.merge_range('D14:D15', 'Контингент студентов', merge_format_rotate_b)
    worksheet.merge_range('E14:E15', 'Количество учебных групп', merge_format_rotate_b)
    worksheet.merge_range('F14:G14', 'Лекции', merge_format_boldr)
    worksheet.write("F15", "По плану", merge_format_rotate_b)
    worksheet.write("G15", "Всего", merge_format_rotate_b)
    worksheet.merge_range('H14:I14', 'Практ., семинар. занятия', merge_format_boldr)
    worksheet.write("H15", "По плану", merge_format_rotate_b)
    worksheet.write("I15", "Всего", merge_format_rotate_b)
    worksheet.merge_range('J14:J15', 'Лаборат. занятия', merge_format_rotate_b)

    c = 10
    cisd = {}

    for row in cols["columns"]:
        col = cols["columns"][row]
        if (len(col["children"]) == 0):
            worksheet.merge_range(13, c, 14, c, col["name"], merge_format_rotate_b)
            cisd[col["id"]] = c
            c += 1
        else:
            worksheet.merge_range(13, c, 13, c+len(col["children"])-1, col["name"] , merge_format_boldr)
            for ch in col["children"]:
               worksheet.write(14, c, ch["name"], merge_format_rotate_b)
               cisd[ch["id"]] = c
               c += 1

    worksheet.merge_range(13, c, 14, c, "ВСЕГО", merge_format_rotate_b)
    c += 1               
    worksheet.merge_range(13, c, 14, c, "Преподаватели, обеспечивающие нагрузку", merge_format_rotate_b)

    for i in range(1, cols["colspan"]-1):
        worksheet.write(15, i-1, i, cell_format_c)

    len_name = cols["colspan"]-5

    active_row = 22 

    fill_empty = ["" for x in range(0, cols["colspan"]-2)]
    sums_forms = {}
    sums_faculties = {}

    for key in load:
        f = load[key]
        sums_faculties[key] = [0 for x in range(0, cols["colspan"]-3)]
        worksheet.set_row(active_row, 22)
        worksheet.write(active_row, 0, "", cell_format_c)
        worksheet.write(active_row, 1, "", cell_format_c)
        worksheet.merge_range(active_row, 2, active_row, len_name, f["name"].upper(), merge_format_fname)
        worksheet.write(active_row, len_name+1, "", cell_format_c)       
        worksheet.write(active_row, len_name+2, "", cell_format_c)
        active_row += 1

        for kf in f["forms"]:
            form = f["forms"][kf]
            sums_forms[kf] = [0 for x in range(0, cols["colspan"]-3)]
            worksheet.set_row(active_row, 22)
            worksheet.write(active_row, 0, "", cell_format_c)
            worksheet.write(active_row, 1, "", cell_format_c)
            worksheet.merge_range(active_row, 2, active_row, len_name, form["name"].upper(), merge_format_form)
            worksheet.write(active_row, len_name+1, "", cell_format_c)       
            worksheet.write(active_row, len_name+2, "", cell_format_c)
            active_row += 1

            for dirc in form["directions"]:
                k = "nb" if budzhet else "nvb"

                if dirc[k] > 0:
                    worksheet.write(active_row, 0, "", cell_format_c)
                    worksheet.write(active_row, 1, "", cell_format_c)
                    worksheet.merge_range(active_row, 2, active_row, len_name, 'Направление {} "{}"'.format(dirc["shifr"], dirc["name"]), merge_format_dir)
                    worksheet.write(active_row, len_name+1, "", cell_format_c)       
                    worksheet.write(active_row, len_name+2, "", cell_format_c)
                    active_row += 1

                for j, kp in enumerate(dirc["profiles"]):

                    profile = dirc["profiles"][kp]
                    strokes = profile["loadstrokes_budzhet"] if budzhet else profile["loadstrokes_vnebudzhet"] 

                    if len(strokes)>0:
                        worksheet.write(active_row, 0, "", cell_format_c)
                        worksheet.write(active_row, 1, "", cell_format_c)
                        worksheet.merge_range(active_row, 2, active_row, len_name, 'профиль "{}"'.format(profile["name"]), merge_format_profile)
                        worksheet.write(active_row, len_name+1, "", cell_format_c)       
                        worksheet.write(active_row, len_name+2, "", cell_format_c)
                        active_row += 1     

                        for stroke in strokes:
                            if stroke["show"]:
                                worksheet.write_row(active_row, 0, fill_empty, cell_format_c)
                                worksheet.write(active_row, 0, stroke["code"], cell_format_c)
                                worksheet.write(active_row, 1, stroke["name"], cell_format)
                                worksheet.write(active_row, 2, "{}/{}".format(stroke["course"], stroke["sem"]), cell_format_c)
                                if stroke["contingent"] != 0:
                                    worksheet.write(active_row, 3, stroke["contingent"], cell_format_c)
                                if stroke["groups"] != 0:    
                                    worksheet.write(active_row, 4, stroke["groups"], cell_format_c)
                                if stroke["lecplan"] != 0:
                                    worksheet.write(active_row, 5, stroke["lecplan"], cell_format_c)
                                    sums_faculties[key][5] += stroke["lecplan"] or 0
                                    sums_forms[kf][5] += stroke["lecplan"] or 0
                                if stroke["lecfact"] != 0:
                                    worksheet.write(active_row, 6, stroke["lecfact"], cell_format_c)
                                    sums_faculties[key][6] += stroke["lecfact"] or 0
                                    sums_forms[kf][6] += stroke["lecfact"] or 0
                                if stroke["practplan"] != 0:    
                                    worksheet.write(active_row, 7, stroke["practplan"], cell_format_c)
                                    sums_faculties[key][7] += stroke["practplan"] or 0
                                    sums_forms[kf][7] += stroke["practplan"] or 0
                                if stroke["practfact"] != 0:    
                                    worksheet.write(active_row, 8, stroke["practfact"], cell_format_c)
                                    sums_faculties[key][8] += stroke["practfact"] or 0
                                    sums_forms[kf][8] += stroke["practfact"] or 0
                                if stroke["labfact"] != 0:    
                                    worksheet.write(active_row, 9, stroke["labfact"], cell_format_c)
                                    sums_faculties[key][9] += stroke["labfact"] or 0
                                    sums_forms[kf][9] += stroke["labfact"] or 0

                                for cval in stroke["columns"]:
                                    if cval["val"] != 0:
                                        worksheet.write(active_row, cisd[cval["c_id"]], cval["val"], cell_format_c)
                                        sums_faculties[key][cisd[cval["c_id"]]] += cval["val"]
                                        sums_forms[kf][cisd[cval["c_id"]]] += cval["val"]

                                worksheet.write(active_row, len_name+1, stroke["summ"], cell_format_c)
                                sums_faculties[key][len_name+1] += stroke["summ"]    
                                sums_forms[kf][len_name+1] += stroke["summ"]
                                keyt = "{}_{}".format(stroke["uuid"], budzhet)
                                if keyt in teachers:
                                    worksheet.write(active_row, len_name+2, ", ".join(teachers[keyt]["names"]), cell_format)    
                                active_row += 1

        worksheet.set_row(active_row, 60)
        worksheet.write_row(active_row, 0, fill_empty, cell_format_itog)
        worksheet.write(active_row, 1, 'Итого по "{}"'.format(f["name"]), cell_format_itog)
        for idx, v in enumerate(sums_faculties[key]):
            if v != 0:
                worksheet.write(active_row, idx, v, cell_format_itog)
        active_row += 1

    worksheet.write_row(16, 0, fill_empty, cell_format)
    worksheet.write(16, 1, "Очная", cell_format)
    worksheet.write_row(17, 0, fill_empty, cell_format)
    worksheet.write(17, 1, "Очно-заочная", cell_format)
    worksheet.write_row(18, 0, fill_empty, cell_format)
    worksheet.write(18, 1, "Заочная", cell_format)
    worksheet.write_row(19, 0, fill_empty, cell_format)
    worksheet.write(19, 1, "На других факультетах", cell_format)
    worksheet.write_row(20, 0, fill_empty, cell_format)
    worksheet.write(20, 1, "ИТОГО:", cell_format)
    
    r = None
    sums_all = [0 for x in range(0, cols["colspan"]-3)]
    for key in sums_forms:
        if key==1:
            r = 16
        elif key==2:
            r = 17
        elif key==3:
            r = 18

        sums_all = [x + y for x, y in zip(sums_all, sums_forms[key])]          

        for idx, v in enumerate(sums_forms[key]):
            if v != 0:
                worksheet.write(r, idx, v, cell_format)

    for idx, v in enumerate(sums_all):
            if v != 0:
                worksheet.write(20, idx, v, cell_format)                























    









import app.models.isuload as ami
import app.schemas.users as asu
import app.settings as settings

import app.schemas.plans as asp
import xml.etree.ElementTree as ET
import uuid

from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException, status, File
from typing import Optional
from datetime import timedelta, datetime
from sqlalchemy import func, delete, update
from sqlalchemy.dialects import postgresql


def get_plans_by_year(db: Session, year: int, faculty: int):
    res = db.query(ami.LessonPlan).join(ami.Profile).\
                                   join(ami.Direction).\
                                   join(ami.Faculty).\
                                   filter(ami.LessonPlan.year == year).\
                                   filter(ami.Faculty.id == faculty).\
                                   order_by(ami.Direction.form_id, ami.Direction.qual_id,
                                            ami.Direction.shifr, ami.Profile.name,
                                            ami.LessonPlan.course).\
                                   all()

    plans = []

    for row in res:
        plans.append({"id": row.id, 
                      "form": row.profile.direction.form.name, 
                      "course": row.course, 
                      "qual": row.profile.direction.qual.name, 
                      "dir": row.profile.direction.name, 
                      "profile": row.profile.name,
                      "shifr": row.profile.direction.shifr,
                      "bc": row.bc,
                      "vc": row.vc,
                      "vg": row.vg,
                      "bg": row.bg })   

    return plans


def add_plan_contingent(db: Session, plan_id: int, bc: int, 
                        vc: int, bg: int, vg: int):
    stmt = update(ami.LessonPlan).\
           where(ami.LessonPlan.id==plan_id).\
           values(bc=bc, vc=vc, bg=bg, vg=vg).\
           execution_options(synchronize_session="fetch")

    res = db.execute(stmt)
    db.commit()

    return "ok" 


def get_old_plan_strokes(db: Session, plan: File, year: int, faculty: int):
    
    file_name = plan.filename
    itog = {"filename": file_name}

    content = plan.file.read()
    root = ET.fromstring(content)

    shifr = root[0][0].get('ПоследнийШифр')
    forma_name = root[0].get('ФормаОбучения').lower()

    form = db.query(ami.ConstrDirectory.value).\
              filter(func.lower(ami.ConstrDirectory.name) == forma_name).\
              filter(ami.ConstrDirectory.type == 'forma').\
              scalar()

    profile_name = root.findall(".//Специальность[@Ном='2']")[0].get('Название')
    profile_name = profile_name.replace('"', '').replace("'", '').replace("профиль", "").strip()
    
    profile = db.query(ami.Profile).join(ami.Direction).\
                 filter(ami.Direction.form_id == form).\
                 filter(ami.Direction.shifr == shifr).\
                 filter(func.lower(ami.Profile.name) == profile_name.lower()).\
                 first()


    if not profile:
        itog["message"] = "Профиль не найден в системе, обновите справочник"
        return itog

    indk = file_name.rfind('k')
    indk2 = file_name.rfind('к')

    course = 0

    if indk > 0:
        course = file_name[indk-1]
    elif indk2 > 0:
        course = file_name[indk2-1]
    else:
        course = str(year-int(root[0][0].get("ГодНачалаПодготовки")) + 1)    

    if (course == 0):   
        itog["message"] = "Невозможно определить курс, отредактируйте имя файла"
        return itog

    pl = db.query(ami.LessonPlan).filter_by(course=course).filter_by(year=year).\
                                  filter_by(profile_id=profile.id).first()

    if pl:
        itog["message"] = "Учебный план уже был загружен в систему"
        return itog

    plan = ami.LessonPlan(course=course, profile_id=profile.id, year=year)
    db.add(plan)
    db.commit()
    db.refresh(plan)

    kr = root.findall(".//ГрафикУчПроцесса/Курс[@Ном='{}']/Семестр".format(course))
    for row in kr:
        sem = row.get('Ном')
        hours = row.get("СтрНедТО")
        table = ami.TimeTable(lessonplan_id=plan.id, sem=sem, hours=hours)
        db.add(table)
        db.commit()

    types = db.query(ami.TypesOfWorkDirectory.code).\
               filter(ami.TypesOfWorkDirectory.active==True).\
               filter(ami.TypesOfWorkDirectory.active_calc==True).\
               all()

    types_list = [row[0] for row in types] 
    types_list.append(51)           

    st = root.findall(".//СтрокиПлана/Строка")
    strokes = []
                                          
    for dis in st:
        code = dis.get('НовИдДисциплины')
        name = dis.get('Дис')
        dep = dis.get('Кафедра')
        choice = 'ДВ' in code if code else False
        object_type = 2

        sems = dis.findall('Сем')
        for dish in sems:
            sem = dish.get('Ном')
            course_new = (int(sem) + 1) // 2
            count = 0
            types_of_work = []

            if (course==str(course_new)):
                if (1 in types_list):
                    c = dish.get('Экз')
                    if c:
                        types_of_work.append((1, 1))

                if (2 in types_list):
                    c = dish.get('Зач')
                    if c:
                        types_of_work.append((2, 1))

                if (3 in types_list):
                    c = dish.get('ЗачО')
                    if c:
                        types_of_work.append((3, 1))

                if (5 in types_list):
                    c = dish.get('КР')
                    if c:
                        types_of_work.append((5, 1))

                vz = dish.findall('VZ')
                
                for v in vz:
                    id_vs = int(v.get('ID'))
                    if id_vs in types_list:
                        types_of_work.append((id_vs, v.get('H')))

                strokes.append({"uuid": str(uuid.uuid4()), "code": code, "sem": sem,
                                "dep": dep, "choice": choice, "object_type": object_type,
                                "types_of_work": types_of_work, "name": name})

    plan_pract = root.findall(".//СпецВидыРабот/УчебПрактики/ПрочаяПрактика")
    
    for row in plan_pract:
        sem = row.get('Сем')
        course_new = 0
        if (sem):
            course_new = (int(sem) + 1) // 2
        if (course == str(course_new)):
            code = ""
            name = row.get('Вид')
            object_type = 31
            dep = dis.get('Кафедра')
            count = dis.get('Нед')

            strokes.append({"uuid": str(uuid.uuid4()), "code": code, "sem": sem,
                            "dep": dep, "choice": False, "object_type": object_type,
                            "types_of_work":  [(51, count)], "name": name})

    plan_pract = root.findall(".//СпецВидыРабот/ПрочиеПрактики/ПрочаяПрактика")
    name_prev = ""
    for row in plan_pract:
        sem = row.get('Сем')
        course_new = (int(sem) + 1) // 2
        if (course == str(course_new)):
            code = ""
            name = row.get('Вид')
            object_type = 3
            dep = dis.get('Кафедра')
            count = dis.get('Нед')

            if (count or name_prev != name):
                if count:
                    strokes.append({"uuid": str(uuid.uuid4()), "code": code, "sem": sem,
                                    "dep": dep, "choice": False, "object_type": object_type,
                                    "types_of_work": [(51, count)], "name": name})
                else:
                   plan_hov = root.findall(".//СпецВидыРаботНов/ПрочиеПрактики/ПрочаяПрактика[@Наименование='{}']/Семестр".\
                                            format(name))[0].get('ПланНед')

                   strokes.append({"uuid": str(uuid.uuid4()), "code": code, "sem": sem,
                                   "dep": dep, "choice": False, "object_type": object_type,
                                   "types_of_work": [(51, plan_hov)], "name": name})                              

            name_prev = name

    plan_dis = root.findall(".//ДиссерПодготовка/ПрочаяПрактика")

    for row in plan_dis:
        semestr = row[0]
        sem = semestr.get('Ном')
        course_new = (int(sem) + 1) // 2
        if (course == str(course_new)):
            code = ""
            name = row.get('Наименование')
            object_type = 6
            dep = 0
            count = semestr.get('ПланНед')

            strokes.append({"uuid": str(uuid.uuid4()), "code": code, "sem": sem,
                            "dep": dep, "choice": False, "object_type": object_type,
                            "types_of_work": [(51, count)], "name": name})


    add_plan_strokes(db, strokes, plan.id)
    add_groups_choice(db, plan.id)

    itog["message"] = "План успешно загружен для {} (профиль {}) {} курс".\
                      format(shifr, profile.name, course)

    return itog         



def get_new_plan_strokes(db: Session, plan: File, year: int, faculty: int):
    
    file_name = plan.filename
    itog = {"filename": file_name}

    ns = {'all': 'http://tempuri.org/dsMMISDB.xsd'}

    content = plan.file.read()
    root = ET.fromstring(content)

    shifr = root.findall(".//all:ООП", ns)[0].get("Шифр").strip()
    forma = root.get("КодФормыОбучения")
    profile_name = root.findall(".//all:ООП/all:ООП", ns)[0].get("Название").strip()

    profile = db.query(ami.Profile).join(ami.Direction).\
                 filter(ami.Direction.form_id == forma).\
                 filter(ami.Direction.shifr == shifr).\
                 filter(func.lower(ami.Profile.name) == profile_name.lower()).\
                 first()

    if not profile:
        itog["message"] = "Профиль не найден в системе, обновите справочник"
        return itog


    indk = file_name.rfind('k')
    indk2 = file_name.rfind('к')

    course = 0

    if indk > 0:
        course = file_name[indk-1]
    elif indk2 > 0:
        course = file_name[indk2-1]
    else:
        course = str(year-int(root.findall(".//all:Планы", ns)[0].get("ГодНачалаПодготовки")) + 1)

    if (course == 0):   
        itog["message"] = "Невозможно определить курс, отредактируйте имя файла"
        return itog
    

    pl = db.query(ami.LessonPlan).filter_by(course=course).filter_by(year=year).\
                                  filter_by(profile_id=profile.id).first()

    if pl:
        itog["message"] = "Учебный план уже был загружен в систему"
        return itog
                                        
    plan = ami.LessonPlan(course=course, profile_id=profile.id, year=year)
    db.add(plan)
    db.commit()
    db.refresh(plan)

    kr = root.findall(".//all:КратностьЧасов[@Семестр='{}']".format(int(course)*2), ns)[0]
    kr2 = root.findall(".//all:КратностьЧасов[@Семестр='{}']".format(int(course)*2-1), ns)[0]
    
    for row in list((kr, kr2)):
        sem = (int(row.get("Семестр")) - 1) % 2 + 1
        hours = row.get("Кратность")
        table = ami.TimeTable(lessonplan_id=plan.id, sem=sem, hours=hours)
        db.add(table)
        db.commit()

    types = db.query(ami.TypesOfWorkDirectory.code).\
               filter(ami.TypesOfWorkDirectory.active==True).\
               filter(ami.TypesOfWorkDirectory.active_calc==True).\
               all()

    types_list = [row[0] for row in types] 
    types_list.append(51)           
    types_list.append(1000)           

    st = root.findall(".//all:ПланыСтроки", ns)

    strokes = []

    for dis in st:
        plnch = root.findall(".//all:ПланыНовыеЧасы[@КодОбъекта='{}']".format(dis.get('Код')), ns)
        code = dis.get('ДисциплинаКод')
        name = dis.get('Дисциплина')
        dep = dis.get('КодКафедры')
        choice = dis.get('УровеньВложения')=="2"
        object_type = dis.get('ТипОбъекта') if dis.get('ВидПрактики')!="6" else 31
            
        if (dep):
            types_of_work = {}
            for dish in plnch:
                count = 0
                code_type = int(dish.get('КодВидаРаботы'))
                course_new = dish.get('Курс')
                if (course_new == course and code_type in types_list):
                    if code_type == 51:
                        count = dish.get('Недель')
                    else:
                        count = dish.get('Количество')

                    sem = int(dish.get('Семестр')) + (int(course)-1)*2
                    if (sem not in types_of_work):
                        types_of_work[sem] = [] 
                    types_of_work[sem].append((code_type, count))           
                    
            for row in types_of_work:
                strokes.append({"uuid": str(uuid.uuid4()), "code": code, "sem": row,
                                "dep": dep, "choice": choice, "object_type": object_type,
                                "types_of_work": types_of_work[row], "name": name})


    add_plan_strokes(db, strokes, plan.id)
    add_groups_choice(db, plan.id)

    itog["message"] = "План успешно загружен для {} (профиль {}) {} курс".\
                      format(shifr, profile.name, course)

    return itog            
                                   


def add_plan(db: Session, plan: File, year: int, faculty: int):
    name = plan.filename
    if ('plx' in name):
        return get_new_plan_strokes(db, plan, year, faculty)
    else:
        return get_old_plan_strokes(db, plan, year, faculty) 


def add_plan_strokes(db, strokes, plan_id):
    for row in strokes:
        st = ami.LessonPlanStroke(uuid=row["uuid"],
                                  lessonplan_id=plan_id,
                                  code=row["code"],
                                  sem=row["sem"],
                                  name=row["name"],
                                  department=row["dep"],
                                  choice=row["choice"],
                                  object_type=row["object_type"])

        db.add(st)
        db.commit()
        db.refresh(st)
        
        for t in row["types_of_work"]:
            if (t[0] != 1000):
                ins = db.execute("""
                                    INSERT INTO lessonplanworks(work_type, work_count, lessonplanstroke_id)
                                    VALUES (:wt, :wc, :lpid)
                                    ON CONFLICT (work_type, lessonplanstroke_id) DO NOTHING
                                """, {"wt": t[0], "wc": t[1], "lpid": st.id})
                db.commit()
                # type_of_work = ami.LessonPlanWork(work_type=t[0],
                #                                   work_count=t[1],
                #                                   lessonplanstroke_id=st.id)
                # db.add(type_of_work)
                # db.commit()


def add_groups_choice(db, plan_id):
    strokes = db.query(ami.LessonPlanStroke).\
                 filter(ami.LessonPlanStroke.lessonplan_id == plan_id).\
                 filter(ami.LessonPlanStroke.choice == True).\
                 order_by(ami.LessonPlanStroke.sem, ami.LessonPlanStroke.code).\
                 all()

    st = ""
    n = 0
    for idx, row in enumerate(strokes):
        same = row.code[::-1]
        same = same[same.index('.'):]
        if (st != same):
            n += 1
        
        gc = ami.GroupChoice(uuid=row.uuid, budzhet_count=0,
                             vnebudzhet_count=0, budzhet_foreign=0,
                             vnebudzhet_foreign=0, block="{}_{}".format(plan_id, n))
        db.add(gc)
        db.commit()
        db.refresh(gc)
        st = same


def delete_lessonplan(db, plan_id):
    plan = db.query(ami.LessonPlan).get({"id":plan_id})

    sel = db.query(ami.LessonPlanStroke.uuid).filter(ami.LessonPlanStroke.lessonplan_id == plan_id).all()
    uuids = [x[0] for x in sel]

    print(uuids)

    stmt = delete(ami.LoadColumn).\
           where(ami.LoadColumn.uuid.in_(uuids)).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    stmt = delete(ami.LoadStroke).\
           where(ami.LoadStroke.uuid.in_(uuids)).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    stmt = delete(ami.GroupChoice).\
           where(ami.GroupChoice.uuid.in_(uuids)).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    stmt = delete(ami.Load).\
           where(ami.Load.profile_id == plan.profile_id).\
           where(ami.Load.year == plan.year).\
           where(ami.Load.course == plan.course).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    stmt = delete(ami.LessonPlanStroke).\
           where(ami.LessonPlanStroke.lessonplan_id == plan_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    stmt = delete(ami.LessonPlan).\
           where(ami.LessonPlan.id == plan_id).\
           execution_options(synchronize_session="fetch")
    db.execute(stmt)
    db.commit()

    return plan_id






    
             